﻿Imports System
Imports ArchestrA.GRAccess
Imports ArchestrA.Visualization.GraphicAccess
Imports System.Xml.Linq
Imports System.Linq
Imports System.Collections.Generic
Imports System.IO
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports System.Reflection
Imports System.ComponentModel
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Runtime.CompilerServices


Public Class Archestra2Xml

    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '  Add these to Archestra2XML at TOP so it can work with easy multithreading
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
#Region "Easy Multithreading"
    'This is the class where you do the actual work so that the WinForm isn't cluttered
    Public thread As ThreadCallBack
    Public CallThisSub As String

    'TODO - Go back and implemnt this properly, but hacked for now to save time
    Public Sub New()    'ByRef MethodToCall As String)
        'CallThisSub = MethodToCall

        Call ModifyFileLocationsIfRunOnVM()
    End Sub

    'This is the PRIMARY sub that will start the work.  Add other subs as needed, but call all from here
    Public Sub DoWork()
        Interaction.CallByName(Me, CallThisSub, CallType.Method)
    End Sub

#End Region


    Public grAccess As GRAccessApp
    Public gals As IGalaxies
    Public galaxy As IGalaxy
    Public grResult As ICommandResult
    Public aaGraphicAccess As GraphicAccess = New GraphicAccess()
    Public GalaxyName As String = ""
    Public GalaxyLoggedIn As Boolean = False
    'Instantiate the XML Helper
    Public aaXML As New aaXMLhelper
    Dim xmlDoc As New XDocument
    'General Setup typically coming from external source
    Public aaGraphicToolset As String = ""
    Public aaGraphicName As String = ""
    Public xmlfilename As String = ""
    Public aaTemplate As String = ""
    Public ExistingGraphicsList As New List(Of String)
    'CMS Have to GetParent a few times since the getcurrentdirectory starts out in either release or debug bin
    Public xmlFolderExport As String = "C:\Files\Temp\Export"
    Public xmlFolderImport As String = "C:\Files\Temp\Import"
    Public xmlFolderTemplates As String = "C:\Files\Temp\Templates"
    Public StatusOutputFolder As String = "C:\Files\Temp"
    Public ExportCsvAsT4sidebar As Boolean = True
    Public T4_AsNew As Boolean = False
    Public MaxSections As Integer = 1
    'Report back to the UI early before completion
    Public StatusResults As String = ""
    'Define our new tbList with the headers.
    Private tbList As New List(Of String)


    Private Sub ModifyFileLocationsIfRunOnVM()
        If System.Environment.MachineName = "WIN-IT8P9RCRC00" Then
            xmlFolderExport = "Z:\Files\Temp\Export"
            xmlFolderImport = "Z:\Files\Temp\Import"
            xmlFolderTemplates = "Z:\Files\Temp\Templates"
            StatusOutputFolder = "Z:\Files\Temp"
        End If
    End Sub

    Public Sub InitializeXmlDoc()
        Try
            'Create a GRAccess to login
            'grAccess = New GRAccessAppClass()
        Catch ex As Exception
            'Do nothing - This is here so we can test on machines without GRAccess
        End Try
        'Set up the xml filename
        If aaGraphicName.EndsWith(".xml") Then aaGraphicName.Replace(".xml", "")

        xmlfilename = Path.Combine(xmlFolderExport, aaGraphicName + ".xml")

        'Read the entire xml file
        xmlDoc = XDocument.Load(xmlfilename)
    End Sub

    Public OverwriteFunction As Overwrite = Overwrite.Initialized

    ''' <summary>
    ''' Imports a single xml graphic into archestra
    ''' </summary>
    ''' <param name="xFileName"></param>
    ''' <returns></returns>
    Public Function aaImport(ByVal xFileName As String, ByVal galaxy As String) As String
        Dim StatusResult As String = ""
        Try
            aaGraphicName = xFileName
            'ExistingGraphicsList is created before calling this function
            If ExistingGraphicsList.Contains(Path.GetFileNameWithoutExtension(xFileName)) Then
                If OverwriteFunction = Overwrite.Initialized Then
                    'FIRST, ask if the user wants to overwrite all.  If not, then ask individually
                    If MessageBox.Show("Some graphics exist.  Do you want to OVERWRITE ALL?", "OVERWRITE ALL EXISTING GRAPHICS", MessageBoxButtons.YesNo) = DialogResult.Yes Then OverwriteFunction = Overwrite.All
                End If
                If OverwriteFunction = Overwrite.All Then
                    StatusResult = Import_XmlToAaGraphic(xFileName)
                Else
                    'SECOND, give the user a chance to overwrite each individually
                    Dim result2 = MessageBox.Show("Do you want to overwrite the " & Path.GetFileNameWithoutExtension(xFileName) & " graphic?", "Graphic Exists", MessageBoxButtons.YesNo)
                    If result2 = DialogResult.Yes Then
                        StatusResult = Import_XmlToAaGraphic(xFileName)
                    Else
                        StatusResult = xFileName & " was not imported."
                    End If
                End If
            Else
                'Graphic doesn't exist, so its OK create it without asking
                StatusResult = Import_XmlToAaGraphic(xFileName)
            End If

            Return StatusResult
        Catch ex As Exception
            StatusResult = ExResults(ex) & vbCrLf & "Verify that " & xFileName & " is not open or checked out."
            Return ExResults(ex)
        End Try
    End Function
    Public Enum Overwrite
        Initialized = 0
        No = 1
        Yes = 2
        All = 3
    End Enum

    Public Function DeleteAllFilesInDirectory(ByVal dir As String) As String
        'Get every file in a directory and get animations from each
        Dim StatusResult As String = "Checking for files in export directory"
        Dim ExportedGraphics As String() = IO.Directory.GetFiles(dir, "*.xml")
        'Check for graphics in xml export directory
        If ExportedGraphics.Count > 0 Then
            'If they exist, ask about deleting them
            Dim deleteResult = MessageBox.Show("Previously exported XML files exist. Delete all existing XML files in " & dir & " before continuing?", "Previous Graphics Exist", MessageBoxButtons.YesNo)
            'If yes then loop through and delete all the existing graphics
            If deleteResult = DialogResult.Yes Then
                For Each graphic In ExportedGraphics
                    File.Delete(graphic)
                Next
            End If
        End If

        Return StatusResult
    End Function

    Public Function DeleteOneFileInDirectory(ByVal dir As String, ByVal filename As String) As String
        'Get every file in a directory and get animations from each
        Dim StatusResult As String = "Checking for files in export directory"
        If IO.File.Exists(IO.Path.Combine(dir, filename)) Then
            'If file exists, ask about deleting it
            Dim deleteResult = MessageBox.Show(filename & " exists. Delete file before continuing?", "Previous Graphics Exist", MessageBoxButtons.YesNo)
            'If yes then loop through and delete all the existing graphics
            If deleteResult = DialogResult.Yes Then File.Delete(IO.Path.Combine(dir, filename))
        End If

        Return StatusResult
    End Function

    ''' <summary>
    ''' Imports all xml graphics in folder into archestra.
    ''' </summary>
    ''' <returns></returns>
    Public Function aaImport_Directory(ByVal galaxy As String) As String
        Dim StatusResult As String = ""
        'Get all existing graphics to verify overwrites
        If ExistingGraphicsList.Count = 0 Then ExistingGraphicsList = WWSP_Get_aaGraphicNames(galaxy)
        Try
            OverwriteFunction = Overwrite.Initialized
            'Get every file in a directory and get animations from each
            Dim files As String() = IO.Directory.GetFiles(xmlFolderImport, "*.xml")
            For Each file In files
                aaGraphicName = Path.GetFileNameWithoutExtension(file)
                StatusResult = StatusResult & vbCrLf & aaImport(aaGraphicName, galaxy)
            Next
            Return StatusResult
        Catch ex As Exception
            StatusResult = ExResults(ex)
            Return StatusResult
        End Try
    End Function



    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   IDE aaGraphic to/from XML
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

    <STAThread>
    Public Function GalaxyLogin(ByVal GalaxyName As String, ByVal Username As String, ByVal password As String, Optional HostName As String = "localhost") As String
        Try
            'Use GRAccess to login to galaxy
            grAccess = New GRAccessAppClass()
            gals = grAccess.QueryGalaxies(HostName)
            If gals Is Nothing OrElse grAccess.CommandResult.Successful = False Then Return grAccess.CommandResult.CustomMessage & grAccess.CommandResult.Text

            galaxy = gals(GalaxyName)
            galaxy.Login(Username, password)
            grResult = galaxy.CommandResult
            If Not grResult.Successful Then Return "Login to galaxy Failed :" & grResult.Text & vbCrLf & grResult.CustomMessage

            Return "Login succesful to " & GalaxyName & "."
        Catch ex As Exception
            ' Return ExResults(ex)
            Return ExResults(ex)
        End Try

    End Function
    Public Sub Export_aaGraphicToXml()
        'TODO - Eventually, update all functions to use the multithreading like this one does
        'TODO - Also remove the old BGW
        Try
            Dim Files = "HESDFSD"
            '***************************** ArchestrA.Visualization.GraphicAccess **************************
            'Set up the xml filename
            xmlfilename = Path.Combine(xmlFolderExport, aaGraphicName & ".xml")  'Path.GetTempFileName()

            'Export the graphic by name to an xml file
            aaGraphicAccess.ExportGraphicToXml(galaxy, aaGraphicName, xmlfilename)
            grResult = aaGraphicAccess.CommandResult
            If Not grResult.Successful Then
                thread.UpdateUI("Error exporting " & aaGraphicName & vbCrLf & grResult.CustomMessage, -1)
                Return
            End If
            'Logout of the galaxy using GRAccess
            'galaxy.Logout()
            thread.UpdateUI(xmlfilename & " converted and saved.", -1)
        Catch ex As Exception
            MessageBox.Show(ex.ToString & vbCrLf & "Verify that " & aaGraphicName & " is not open or checked out.")
            thread.UpdateUI(ExResults(ex), -1)
        End Try
        thread.UpdateUI("Single Export Complete", -1)
    End Sub

    Public Sub aaExport_Toolset()
        Dim StatusReturn As String = ""
        Try
            'Get the symbols from the toolset
            Dim aagraphics = WWSP_GetMenuTree_FromIDE(aaGraphicToolset, GalaxyName, ReturnToolsetAndSymbol:=False)
            thread.UpdateUI(String.Join(vbCrLf, aagraphics), -1)
            'Loop through all symbols
            For Each aaG In aagraphics
                aaGraphicName = aaG
                Export_aaGraphicToXml()
            Next
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            thread.UpdateUI(ExResults(ex), -1)
        End Try
        thread.UpdateUI("Export All Complete", -1)
    End Sub


    Public Function Import_XmlToAaGraphic(ByVal aGraphicName As String) As String
        Try
            'Set up the xml filename
            xmlfilename = Path.Combine(xmlFolderImport, aGraphicName & ".xml")
            'xmlfilename = xmlfoldername & aaGraphicName & ".xml"  'Path.GetTempFileName()
            'Import the graphic by name to the GraphicToolbox
            If File.Exists(xmlfilename) Then
                aaGraphicAccess.ImportGraphicFromXml(galaxy, aGraphicName, xmlfilename, bOverWrite:=True)
                grResult = aaGraphicAccess.CommandResult
                If Not grResult.Successful Then Return "Error importing " & aGraphicName & vbCrLf & grResult.CustomMessage

                'Return
                Return "Imported " & aGraphicName & " to the IDE."
            Else
                '
                Return aGraphicName & " is not a valid format."
            End If
        Catch ex As Exception
            Return ExResults(ex)
        End Try
    End Function

    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   Old Intouch Graphics manipulation
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Public FoundTextList As New List(Of String) 'Used to troubleshoot
    Public Function GetAnimationsFromXml(ByVal filename As String) As String 'Also saves as csv
        StatusResults = aaGraphicName
        Dim xmlDocToParse As New XDocument

        Try
            'TODO - Add a text search and replace that removes &#x1B; since some have had them and it breaks any xml file.  Even XML apps won't open it.

            xmlDocToParse = XDocument.Load(filename)
            'Get the Last Used ElementID so that when you add new elements you can assign them a number
            Dim LastUsedId As Integer = xmlDocToParse.Root.Attribute("LastUsedId")

            'Get the base aaSymbol important items as primary elements
            Dim base_GraphicElements As XElement = xmlDocToParse.Root.Element("GraphicElements")
            Dim base_CustomProperties As XElement = xmlDocToParse.Root.Element("CustomProperties")
            Dim base_NamedScripts As XElement = xmlDocToParse.Root.Element("NamedScripts")
            Dim base_PredefinedScripts As XElement = xmlDocToParse.Root.Element("PredefinedScripts")



            'Set the atrribute you are using for lookups
            Dim xAttrName = "Name"
            Dim xPrefix = "#"

            'Get all archestra groups that have names that start with "#"
            'Dim xGroupObjs = base_GraphicElements.Elements.Where(Function(x) x.Attribute(xAttrName).Value.StartsWith(xPrefix)).ToList
            Dim xGroupObjs = base_GraphicElements.Elements.ToList

            'Check for nulls before finding animations
            If IsNothing(xGroupObjs) Then Return "No groups were found with the prefix " & xPrefix
            For Each item In xGroupObjs
                'Get all the animation expressions for this element as list of string in the format "aaObject|aaAnimation"
                Dim aaAnimationValues As List(Of String) = aaXML.GetAnimationsFromOneItem(aaGraphicName, item, xAttrName, xPrefix)
                'Results
                If aaAnimationValues.Count = 0 Then
                    'REMOVED DUE TO CLUTTER   StatusResults = StatusResults & vbCrLf & "No animations were found in the group " & item.Name.ToString
                Else
                    StatusResults = StatusResults & vbCrLf & String.Join(vbCrLf, aaAnimationValues)
                End If
            Next

            'Update the 'Description' field with unique text to make search and replace easier.
            Dim ObjCount = 0
            For Each item In aaXML.aaObjects
                ObjCount = ObjCount + 1
                'Assign ELementID based on the object type (Import will not work unless this is correct)
                Select Case item.ObjectType
                    'The default Description_ElementID_Postfix is 'Description', so only update here if it is different
                    Case ObjectTypes.Cntrl_ScadaHOA.ToString, ObjectTypes.Cntrl_Switch2pos.ToString, ObjectTypes.Cntrl_Switch3pos.ToString
                        item.Description_ElementID_Postfix = ".Title"
                    Case ObjectTypes.Discrete_Alarm.ToString
                        item.Description_ElementID_Postfix = ".#Discrete#.DiscreteAlarm"
                    Case ObjectTypes.Discrete_Status.ToString
                        item.Description_ElementID_Postfix = ".#Discrete#.DiscreteStatus"
                    Case ObjectTypes.SetpointAlarm_DI.ToString
                        Dim stopme1 = 1
                    Case Else
                        item.Description_ElementID_Postfix = ".Description"
                End Select
                'Update the generic 'Description' field automatically from the longest text found inside a #Object
                Dim Longest = ""
                For Each txtValue In item.TextElements
                    If Not FoundTextList.Contains(txtValue) Then FoundTextList.Add(txtValue)
                    'Ignore any values less than 6 letters to avoid misc text from being entered.  I.e., if the text isn't longer than 5 letters, then it's probably not a description
                    If txtValue.Contains("#.#") Or txtValue.Contains("##") Or txtValue.Contains("n/a") Then
                        'Skip, this is not a description
                    Else
                        If txtValue.Length > 3 And txtValue.Length > Longest.Length Then
                            If Longest = "" Then
                                Longest = txtValue
                            Else
                                Longest = Longest & " " & txtValue
                            End If
                        End If
                    End If
                Next
                If Longest.Length > 2 Then
                    item.Description = Longest
                Else
                    item.Description = item.ObjectName & "_" & item.ObjectTypeImported ' & " - " & i.ToString & 
                End If
            Next

            'Find all animation types and values
            Dim AllAnimationsList As New List(Of String)
            AllAnimationsList.Add(vbCrLf & "Animation Types Obtained:")
            For Each item In aaXML.aaObjects
                For Each anim In item.Animations
                    If Not AllAnimationsList.Contains("⮡  " & anim.AnimType) Then AllAnimationsList.Add("⮡  " & anim.AnimType)
                Next
            Next
            AllAnimationsList.Add(vbCrLf & "Animation Values Obtained:")
            For Each item In aaXML.aaObjects
                For Each anim In item.Animations
                    If Not AllAnimationsList.Contains("⮡  " & anim.Value) Then AllAnimationsList.Add("⮡  " & anim.Value)
                Next
            Next

            AllAnimationsList.Add(vbCrLf & "Properties Report:")
            For Each item In aaXML.aaObjects
                If item.GraphicFilename = Path.GetFileName(aaGraphicName) Then
                    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                    '  THIS IS THE START OF MANIPULATING THE ANIMATION VALUE 
                    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                    For Each anim In item.Animations
                        'Remove double spaces
                        anim.Value = anim.Value.Replace("  ", " ")
                        'Skip any ANIMATION TYPES that will never have tags and flag in the error report
                        If aaXML.AnimTypeWithoutTag(anim.AnimType) Then
                            anim.Tags.ErrorCheck = "<Ignore AnimType>"
                            Continue For 'Exit early since there is no tag to be found
                        End If
                        'If this is just a number or #.#, then ignore since its not a tag
                        Dim TryNumeric As Double
                        Dim IsNumberOnly = Double.TryParse(anim.Value.Trim, TryNumeric)
                        If IsNumberOnly Then
                            anim.Tags.ErrorCheck = "<#>"
                            Continue For
                        End If
                        If anim.Value.Trim.Replace(".", "").Replace("#", "") = "" Then
                            anim.Tags.ErrorCheck = "<#>"
                            Continue For
                        End If
                        'More oddball removals
                        If anim.Value.Contains("X.X") Or anim.Value.Contains("0.#") Or anim.Value.ToLowerInvariant.Contains(" hp") Or anim.Value.Trim.EndsWith(".") Then
                            anim.Tags.ErrorCheck = "<#>"
                            Continue For
                        End If
                        If anim.Value.ToLower.StartsWith("if ") Then
                            anim.Tags.ErrorCheck = "<IF-THEN>"
                            Continue For
                        End If
                        'Special treatment for Action Scripts because the tags can't be predicted well
                        If anim.AnimType.StartsWith("ActionScript") Then
                            anim.Tags.ErrorCheck = "<ActionScript>"
                            'Ignore these
                            If anim.Value.ToLowerInvariant.StartsWith("show") Or anim.Value.ToLowerInvariant.Contains("tagviewer") Or anim.Value.ToLowerInvariant.Contains("graphicinfo") Or anim.Value.ToLowerInvariant.Contains("startapp ") Then
                                anim.Tags.ErrorCheck = "<Script - Ignore Tag>"
                                Continue For
                            Else
                                'Accomodate the line breaks
                                anim.Value = anim.Value.Trim(";") 'Trim these off the end
                                anim.Value = anim.Value.Replace(";", " AND ") 'If any are left in the middle, replace with AND so they will split
                                anim.Value = anim.Value.Replace("  ", " ")
                            End If
                        End If
                        'Detect InTouch Properties and make them unique so they can be addressed later
                        'Replaces the period with <ITPROP> before the property so the period doesn't confuse the converter
                        '.Alarm, .Ack, .UnAck, .EngUnits, .QualityStatus, etc
                        aaXML.ContainsInTouchProperty(anim.Value)

                        'Extract Tagnames by making assumptions
                        Dim result = ""
                        Dim AnimVal As String = ""
                        Dim Sections As New List(Of String)
                        Try
                            'Remove "InTouch:"
                            If anim.Value.ToLowerInvariant.Contains("intouch:") Then
                                anim.Tags.FoundInTouch = "InTouch:"
                                AnimVal = anim.Value.Trim.Replace("InTouch:", "").Replace("Intouch:", "").Replace("intouch:", "")
                            Else
                                AnimVal = anim.Value.Trim
                            End If

                            'UNIQUE to SEJPA - Replace & Gsec, & $Msec
                            If AnimVal.Contains("& Gsec") Then AnimVal = AnimVal.Replace("& Gsec", "").Replace("  ", " ")
                            If AnimVal.Contains("& $Msec") Then AnimVal = AnimVal.Replace("& $Msec", "").Replace("  ", " ")

                            'Split AND into multiple tags
                            If AnimVal.ToLowerInvariant.Contains(" and ") Then
                                AnimVal = AnimVal.Replace("and", "AND")
                                anim.Tags.FoundAND = "AND"
                                Dim NewName = AnimVal.Replace(" AND ", "|")
                                Sections.AddRange(NewName.Split("|").ToList)
                            End If
                            'AND wasn't found, so just create a single section
                            If Sections.Count = 0 Then Sections.Add(AnimVal)
                            'Look in each section for OR
                            Dim NewSections As New List(Of String)
                            Dim index As Integer = 0
                            Dim SectionToRemove As Integer = 0
                            For Each section As String In Sections
                                'Replace double spaces in case the AND removal caused it
                                section = section.Replace("  ", " ")
                                'Split OR into multiple tags
                                If section.ToLowerInvariant.Contains(" or ") Then
                                    section = section.Replace("or", "OR")
                                    anim.Tags.FoundOR = "OR"
                                    SectionToRemove = index
                                    Dim NewName = section.Replace(" OR ", "^")
                                    NewSections.AddRange(NewName.Split("^").ToList)
                                End If
                                index = index + 1
                            Next
                            If anim.Tags.FoundOR = "OR" Then
                                Sections.RemoveAt(SectionToRemove)
                                Sections.AddRange(NewSections)
                            End If

                            If Sections.Count > 1 Then
                                'Add to the MoreTags list
                                anim.Tags.Tag1.TagFound = Sections(0).Trim
                                For i = 1 To Sections.Count - 1
                                    Dim a As New aaObject.aaAnimTag
                                    a.TagFound = Sections(i).Trim
                                    anim.Tags.MoreTags.Add(a)
                                Next
                            Else
                                'Just stick with Tag1
                                anim.Tags.Tag1.TagFound = Sections(0).Trim
                                'Add a blank tag to MoreTags list so the csv won't break
                                anim.Tags.MoreTags.Add(New aaObject.aaAnimTag)
                            End If
                            If MaxSections < Sections.Count Then MaxSections = Sections.Count

                        Catch ex As Exception
                            MessageBox.Show(ex.ToString)
                            anim.Tags.Tag1.TagFound = AnimVal
                            anim.Tags.ErrorCheck = ExResults(ex)
                        End Try

                        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                        '  Extract the tags
                        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                        For i = 1 To Sections.Count
                            Dim Index As Integer = i - 2
                            Dim ThisTag As New aaObject.aaAnimTag
                            If AnimVal.Contains("NOT") Then
                                Dim stophy = 1
                            End If
                            If i = 1 Then
                                ThisTag = anim.Tags.Tag1
                            Else
                                ThisTag = anim.Tags.MoreTags(Index)
                            End If

                            Try
                                'Load Tagname1 if no special conditions above were met
                                If ThisTag.TagFound = "" And i = 1 Then
                                    ThisTag.TagFound = AnimVal.Trim
                                End If

                                'Detect NOT
                                If ThisTag.TagFound.ToLowerInvariant.Contains("not ") Then
                                    ThisTag.TagFound = ThisTag.TagFound.Replace("not", "").Replace("NOT", "").Trim
                                    ThisTag.FoundNOT = "NOT"
                                End If

                                'Split out the operators ==, <>, <=, >=, <, >, *, +, /, -, (, )
                                ThisTag.TagFound = ThisTag.TagFound.Replace("(", "").Replace(")", "")
                                'These arguments are ByRef
                                Dim OperationFound As Boolean = False
                                If aaXML.RemoveOperators("==", ThisTag.TagFound, ThisTag.FoundOperator) Then OperationFound = True
                                If aaXML.RemoveOperators("=", ThisTag.TagFound, ThisTag.FoundOperator) Then OperationFound = True
                                If aaXML.RemoveOperators("<>", ThisTag.TagFound, ThisTag.FoundOperator) Then OperationFound = True
                                If aaXML.RemoveOperators("<=", ThisTag.TagFound, ThisTag.FoundOperator) Then OperationFound = True
                                If aaXML.RemoveOperators(">=", ThisTag.TagFound, ThisTag.FoundOperator) Then OperationFound = True
                                If aaXML.RemoveOperators("<", ThisTag.TagFound, ThisTag.FoundOperator) Then OperationFound = True
                                If aaXML.RemoveOperators(">", ThisTag.TagFound, ThisTag.FoundOperator) Then OperationFound = True
                                If aaXML.RemoveOperators("*", ThisTag.TagFound, ThisTag.FoundOperator) Then OperationFound = True
                                If aaXML.RemoveOperators("+", ThisTag.TagFound, ThisTag.FoundOperator) Then OperationFound = True
                                If aaXML.RemoveOperators("/", ThisTag.TagFound, ThisTag.FoundOperator) Then OperationFound = True
                                If aaXML.RemoveOperators("-", ThisTag.TagFound, ThisTag.FoundOperator) Then OperationFound = True
                                If OperationFound Then Dim stopme = 1

                                'Detect <ITPROP> that was added earlier to detect Intouch DotFields and address it
                                If ThisTag.TagFound.Contains(aaXML.ITPROP) Then
                                    ThisTag.TagName = ThisTag.TagFound.Replace(aaXML.ITPROP, "~").Split("~")(0)
                                    ThisTag.TagFound = ThisTag.TagFound.Replace(aaXML.ITPROP, ".")
                                Else
                                    ThisTag.TagName = ThisTag.TagFound
                                End If
                                'Now remove it from anim.val for display in the csv
                                If anim.Value.Contains(aaXML.ITPROP) Then
                                    anim.Value = anim.Value.Replace(aaXML.ITPROP, ".")
                                End If

                            Catch ex As Exception
                                anim.Tags.ErrorCheck = ex.Message
                                result = ExResults(ex)
                            End Try

                            Try
                                'Split into 3 parts to find the tag properties
                                Dim n As String() = ThisTag.TagFound.Split("_")
                                Select Case n.Count
                                    Case 1
                                        'Do not save the tagname if a static value ThisTag.TagFound = n(0)
                                        If AnimVal.Contains("$AccessLevel") Then
                                            anim.Tags.ErrorCheck = "<$AccessLevel>"
                                            'ThisTag.TagFound = anim.Value.Split("")(0).Trim 'ALREADY DONE ABOVE
                                            'ThisTag.FoundOperator = anim.Value.Split("")(1).Trim
                                        Else
                                            If n(0).Contains("$") Then
                                                anim.Tags.ErrorNotes = "<$System Tag>"
                                            Else
                                                anim.Tags.ErrorNotes = "<Tag - only 1 part>"
                                            End If
                                        End If
                                        'result = "🞭🞭 " & anim.Tags.ErrorCheck & " " & anim.Value & " (" & anim.AnimType & ")"
                                    Case 2
                                        '   Don't modify ThisTag.TagFound so it will display the entire tag it found
                                        anim.Tags.ErrorNotes = "<Tag - only 2 parts>"
                                        'result = "🞭🞭 " & anim.Tags.ErrorCheck & " " & anim.Value & " (" & anim.AnimType & ")"
                                    Case 3
                                        'This is the way they should come in (unless there is a .Alarm, .Ack, etc)
                                        '   Don't modify ThisTag.TagFound so it will display the entire tag it found
                                        ThisTag.p1_ObjType = n(0)
                                        ThisTag.p2_ObjName = n(1)
                                        ThisTag.p3_Property = n(2)
                                        'Flag the tagname first section not matching the import
                                        Select Case "#" & ThisTag.p1_ObjType
                                            Case item.ObjectTypeImported
                                                'No error
                                            Case "#" & item.ObjectType
                                                anim.Tags.ErrorCheck = "Tag ObjType - Verify"
                                                anim.Tags.ErrorNotes = ThisTag.p1_ObjType & " | " & item.ObjectTypeImported
                                            Case Else
                                                anim.Tags.ErrorCheck = "Tag ObjType <> #GroupName"
                                                anim.Tags.ErrorNotes = ThisTag.p1_ObjType & " <> " & item.ObjectTypeImported
                                        End Select
                                    Case 4
                                        '   Try to combine the 2nd and 3rd parts to guess where the misplaced _ is
                                        ThisTag.p1_ObjType = n(0)
                                        ThisTag.p2_ObjName = n(1) & "_" & n(2)
                                        ThisTag.p3_Property = n(3)
                                        anim.Tags.ErrorNotes = "<Tag - has 4 parts>"
                                    Case Else
                                        '   Don't modify ThisTag.TagFound so it will display the entire tag it found
                                        anim.Tags.ErrorNotes = "<Tag - has " & n.Count & " parts>"
                                End Select

                            Catch ex As Exception
                                anim.Tags.ErrorCheck = ex.Message
                                result = ExResults(ex)
                            End Try
                        Next

                        'Go back and override error message with these more important ones
                        'If this animation was found outside a # object, then still report it, but just notify the problems.
                        If item.ObjectType = "<# Not Found>" Then
                            anim.Tags.ErrorCheck = "ERROR: Not in #Group"
                            anim.Tags.ErrorNotes = ""
                            item.ObjectName = "<N/A>"
                        ElseIf item.ObjectType = "<NOT_FOUND>" Then
                            anim.Tags.ErrorCheck = "ERROR: ObjName NOT FOUND"
                            anim.Tags.ErrorNotes = ""
                        End If
                        If item.SourceName_Overflow.StartsWith("<ERROR:") Then
                            anim.Tags.ErrorCheck = item.SourceName_Overflow.Replace("<", "").Replace(">", "")
                            anim.Tags.ErrorNotes = ""
                        End If

                        If result.Trim = "" Then Continue For 'Skip blanks
                        If Not AllAnimationsList.Contains("⮡  " & result) Then AllAnimationsList.Add("⮡  " & result)
                    Next
                Else
                    'Do nothing
                End If
            Next

            StatusResults = StatusResults & vbCrLf & String.Join(vbCrLf, AllAnimationsList)

            'Return
            If StatusResults = aaGraphicName Then StatusResults = StatusResults & vbCrLf & "No animations were found in " & aaGraphicName & "."
            Return StatusResults
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            StatusResults = ExResults(ex)
            Return StatusResults
        End Try
    End Function

    Public Function WriteFTViewToCsv() As String
        StatusResults = ""
        Try
            'Write csv to visualize animations
            Dim TextElements As New List(Of String)
            Dim CsvList As New List(Of String)
            Dim TextFilename As String = Path.Combine(StatusOutputFolder, "TextElements.csv")
            Dim CsvFilename As String = Path.Combine(StatusOutputFolder, "GroupAnimations.csv")
            Dim ExcelFilename As String = Path.Combine(StatusOutputFolder, "aaGroupAnimations.xlsx")
            Dim MaxText = aaXML.aaObjects.Where(Function(x) Not IsNothing(x.TextElements)).Max(Function(x) x.TextElements.Count)
            CsvList.Add("GfxName|ObjName|x|y|animParent|animType|animValue")
            TextElements.Add("GfxName|ObjName|x|y|FontSize|Value")

            For Each O In aaXML.aaObjects
                If O.Animations.Count > 0 Then
                    For Each anim In O.Animations
                        Dim MoreTagsCombined As String = ""
                        If anim.Tags.MoreTags.Count = 0 Then
                            anim.Tags.MoreTags.Add(New aaObject.aaAnimTag)
                            MoreTagsCombined = ",,,,"
                        Else
                            Dim tagnames = anim.Tags.MoreTags.Select(Function(x) x.TagName).ToList
                            MoreTagsCombined = String.Join(",", tagnames)
                        End If
                        'If no errors, add the #'s back to ObjectType display in the csv
                        Dim csvObjType As String = O.ObjectType
                        If Not O.ObjectType.StartsWith("<") Then csvObjType = "#" & O.ObjectType & "#"
                        'Form the csv row
                        CsvList.Add(O.GraphicFilename & "|" & O.ObjectName & "|" & O.Xpos & "|" & O.Ypos & "|" & anim.Parent & "|" & anim.AnimType & "|" & anim.Value)
                        'TODO - Need to accomodate the rest of MoreTags list
                    Next
                Else
                    'Do nothing
                End If

                Dim sb As StringBuilder = New StringBuilder(O.GraphicFilename & "|" & O.ObjectName & "|" & O.Xpos & "|" & O.Ypos & "|" & O.FontSize)
                'If O.TextElements.Count > 10 Then MessageBox.Show(String.Join(vbCrLf, O.TextElements))
                If O.TextElements.Count > 0 Then
                    For Each txt In O.TextElements
                        Select Case True
                            Case txt.Contains(vbCrLf), txt.Contains(vbCr), txt.Contains(vbLf)
                                Dim strg = Replace(txt, vbCrLf, "~")
                                strg = Replace(strg, vbCr, "~")
                                strg = Replace(strg, vbLf, "~")
                                sb.Append("|" & strg)
                            Case Else
                                sb.Append("|" & txt)
                        End Select
                    Next
                    TextElements.Add(sb.ToString)
                    sb.Clear()
                Else
                    'Do nothing
                End If
            Next
            IO.File.WriteAllLines(CsvFilename, CsvList)
            StatusResults = "All animations imported"
            If False Then Process.Start(ExcelFilename) 'TODO - Add an option to autoopen

            IO.File.WriteAllLines(TextFilename, TextElements)


            'Make a csv with all object types by screen.  
            CsvList = New List(Of String)
            CsvList.Add("GraphicFilename|ObjectName|Group|Xpos|Ypos|Height|Width")
            For Each item In aaXML.aaObjects
                CsvList.Add(item.GraphicFilename & "|" & item.ObjectName & "|" & item.FTViewGroupPath & "|" & item.Xpos & "|" & item.Height & "|" & item.Width)
            Next
            CsvFilename = Path.Combine(StatusOutputFolder, "ObjectsPerImportedGraphic.csv")
            IO.File.WriteAllLines(CsvFilename, CsvList)

            Return StatusResults
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        StatusResults = ExResults(ex)
        Return StatusResults
        End Try

    End Function

    Public Function WriteAnimationsToCsv() As String
        StatusResults = ""
        Try
            'Write csv to visualize animations
            Dim TextElements As New List(Of String)
            Dim CsvList As New List(Of String)
            Dim TextFilename As String = Path.Combine(StatusOutputFolder, "TextElements.csv")
            Dim CsvFilename As String = Path.Combine(StatusOutputFolder, "GroupAnimations.csv")
            Dim ExcelFilename As String = Path.Combine(StatusOutputFolder, "aaGroupAnimations.xlsx")
            CsvList.Add("aaGraphic,#ObjTypeImported,#ObjTypeUsed,#ObjName,#Overflow,animParent,animType,animValue,ErrorCheck,ErrorNotes,1NOT,1Tag,1=,AND,OR,2NOT,2Tag,2=,1ObjType,1ObjName,1ObjProperty,2ObjType,2ObjName,2ObjProperty,InTouch:,Tag1,Tag2,Tag3,Tag4,Tag5,Tag6")

            For Each O In aaXML.aaObjects
                If O.Animations.Count > 0 Then
                    For Each anim In O.Animations
                        Dim MoreTagsCombined As String = ""
                        If anim.Tags.MoreTags.Count = 0 Then
                            anim.Tags.MoreTags.Add(New aaObject.aaAnimTag)
                            MoreTagsCombined = ",,,,"
                        Else
                            Dim tagnames = anim.Tags.MoreTags.Select(Function(x) x.TagName).ToList
                            MoreTagsCombined = String.Join(",", tagnames)
                        End If
                        'If no errors, add the #'s back to ObjectType display in the csv
                        Dim csvObjType As String = O.ObjectType
                        If Not O.ObjectType.StartsWith("<") Then csvObjType = "#" & O.ObjectType & "#"
                        'Form the csv row
                        CsvList.Add(O.GraphicFilename & "," & O.ObjectTypeImported & "," & csvObjType & "," & O.ObjectName & "," & O.SourceName_Overflow & "," &
                                    anim.Parent & "," & anim.AnimType & "," & anim.Value & "," & anim.Tags.ErrorCheck & "," & anim.Tags.ErrorNotes & "," &
                                    anim.Tags.Tag1.FoundNOT & "," & anim.Tags.Tag1.TagFound & "," & anim.Tags.Tag1.FoundOperator & "," & anim.Tags.FoundAND & "," & anim.Tags.FoundOR & "," &
                                    anim.Tags.MoreTags(0).FoundNOT & "," & anim.Tags.MoreTags(0).TagFound & "," & anim.Tags.MoreTags(0).FoundOperator & "," &
                                    anim.Tags.Tag1.p1_ObjType & "," & anim.Tags.Tag1.p2_ObjName & "," & anim.Tags.Tag1.p3_Property & "," &
                                    anim.Tags.MoreTags(0).p1_ObjType & "," & anim.Tags.MoreTags(0).p2_ObjName & "," & anim.Tags.MoreTags(0).p3_Property & "," &
                                    anim.Tags.FoundInTouch & "," & anim.Tags.Tag1.TagName & "," & anim.Tags.MoreTags(0).TagName & "," & MoreTagsCombined)
                        'TODO - Need to accomodate the rest of MoreTags list
                    Next
                Else
                    'Do nothing
                End If

                Dim sb As StringBuilder = New StringBuilder(O.GraphicFilename & "," & O.ObjectName)
                'If O.TextElements.Count > 10 Then MessageBox.Show(String.Join(vbCrLf, O.TextElements))
                For Each txt In O.TextElements
                    sb.Append("," & txt)
                Next
                TextElements.Add(sb.ToString)
                sb.Clear()
            Next
            IO.File.WriteAllLines(CsvFilename, CsvList)
            StatusResults = "All animations imported"
            If False Then Process.Start(ExcelFilename) 'TODO - Add an option to autoopen

            IO.File.WriteAllLines(TextFilename, TextElements)


            'Make a csv with all object types by screen.  
            CsvList = New List(Of String)
            CsvList.Add("GraphicFilename,ObjectType,ObjectTypeImported,ObjectName")
            For Each item In aaXML.aaObjects
                CsvList.Add(item.GraphicFilename & "," & item.ObjectType & "," & item.ObjectTypeImported & "," & item.ObjectName)
            Next
            CsvFilename = Path.Combine(StatusOutputFolder, "ObjectsPerImportedGraphic.csv")
            IO.File.WriteAllLines(CsvFilename, CsvList)

            Return StatusResults
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            StatusResults = ExResults(ex)
            Return StatusResults
        End Try

    End Function

    ''' <summary>
    ''' Determines which standard ojbect to copy then calls creates windows from standard templates and deletes standard ojbects from new windows
    ''' </summary>
    ''' <returns></returns>
    Public Function CreateGraphicsFromaaObjects(ByVal standardTemplate As String, ByVal updateExisting As String, Optional ByVal UseSymbolReference As Boolean = False) As String
        Dim ReturnStatusList As New List(Of String)
        ReturnStatusList.Add(aaGraphicName)
        Dim DeleteLastGroup As Boolean = False
        Try
            'The function below ADDS xml elements to the file if it exists, so we FIRST need to delete any existing versions
            For Each o In aaXML.aaObjects
                If IO.File.Exists(o.GraphicFilename) Then IO.File.Delete(o.GraphicFilename)
            Next
        Catch ex As Exception
        End Try

        Try
            'TODO - Clean up the T4_SideBar selection code throughout 
            If ExportCsvAsT4sidebar Then
                'OPTION A - Csv In/Out for T4_Sidebar_Builder
                'DeleteLastGroup = True
                'Group objects into Dictionaries. =Dictionary(Graphic Name, Dictionary(T4PageNum, aaobject))
                Dim Grouping = aaXML.aaObjects.GroupBy(Function(x) x.GraphicFilename).ToDictionary(Function(x) x.Key, Function(x) x.GroupBy(Function(y) y.T4PageNumber).ToDictionary(Function(y) "Page" & CInt(y.Key).ToString("00"), Function(y) y.ToList))
                'Start by going through the graphic file names
                For Each graphic In Grouping
                    'Grab SidebarBuilder template
                    Dim aaXmlDoc As XDocument = XDocument.Load(Path.Combine(xmlFolderTemplates, "T4_Template_SidebarBuilder.xml"))
                    'Get a list of the embedded symbols
                    Dim xSymbols As List(Of XElement) = aaXmlDoc.Descendants("EmbeddedSymbol").ToList

                    'Remove excess page numbers
                    For i As Integer = graphic.Value.Count To aaXmlDoc.Descendants("Group").Where(Function(x) x.Attribute("Name").Value.ToLowerInvariant.Contains("page")).ToList.Count - 1
                        aaXmlDoc.Descendants("Group").Where(Function(x) x.Attribute("Name").Value.ToLowerInvariant.Contains("page")).FirstOrDefault.Remove()
                    Next

                    'Get the main group elem
                    Dim pageGroups = aaXmlDoc.Descendants("Group").Where(Function(x) x.Attribute("Name").Value.ToLowerInvariant.Contains("page")).ToList
                    Dim pageElem = aaXmlDoc.Descendants("Group").FirstOrDefault
                    'Create a grouped list of xml elements to add all groups to file at once
                    Dim GroupXMLs As New List(Of XElement)
                    'Next start building the group XML element.
                    For Each PageNum In graphic.Value
                        'If PageNum.Value.Select(Function(x) x.T4PageName).Distinct.Count > 1 Then MessageBox.Show("More than one header for " & PageNum.Key & " in graphic " & graphic.Key & vbCrLf & "The first T4PageHeader listed in group will be used.")
                        'Reset the X and Y position for each new group
                        Dim YPos As Integer = 80
                        Dim XPos As Integer = 50
                        Dim updateGroup = pageGroups.Where(Function(x) x.Attribute("Name").Value = PageNum.Key).FirstOrDefault
                        ' Dim updateGroup = New XElement(pageElem)
                        Dim groupXGE = updateGroup.Element("GraphicElements")
                        'Update the name
                        'updateGroup.Attribute("Name").SetValue(PageNum.Key)
                        'Update the page number
                        'updateGroup.Descendants("TextBox").Where(Function(x) x.Attribute("Name").Value = "NavPage").FirstOrDefault.Element("TextStyleData").Attribute("Text").SetValue(PageNum.Value.FirstOrDefault.T4PageNumber)

                        'Next start going through the aaobjects in the current pagenum group
                        For Each obj In PageNum.Value
                            Dim tbObj As Boolean = obj.SymbolReference.ToLowerInvariant.Contains("textbox")
                            'Update the x and y pos of the aaobject
                            If T4_AsNew Then
                                obj.Xpos = XPos
                                obj.Ypos = YPos
                                YPos += 40
                                'Update the header
                                'updateGroup.Descendants("TextBox").Where(Function(x) x.Attribute("Name").Value = "Header").FirstOrDefault.Element("TextStyleData").Attribute("Text").SetValue(PageNum.Value.FirstOrDefault.T4PageName)
                            Else
                                'Some of the values are way off to the right and we don't want that. Force them to an aligned position.
                                'obj.Xpos = XPos
                            End If

                            'Standard objects start and end with "#"
                            Dim aaElementToCopy As String = "#" & obj.ObjectType & "#"
                            If tbObj Then
                                aaElementToCopy = "Header"
                            End If
                            'Limit filename to 32 characters
                            Dim templateElem = xSymbols.Where(Function(x) x.Attribute("EmbeddedSymbolReference").Value = obj.SymbolReference).FirstOrDefault
                            If tbObj Then templateElem = groupXGE.Elements.Where(Function(x) x.Attribute("Name").Value = "Header").FirstOrDefault
                            'If IsNothing(templateElem) Then MessageBox.Show("Not found")
                            If IsNothing(templateElem) Then templateElem = xSymbols.Where(Function(x) x.Attribute("Name").Value = "#NOT_FOUND#").FirstOrDefault
                            If Not IsNothing(templateElem) And Not obj.ObjectType = "ROOT" Then
                                Dim addToGroup = New XElement(templateElem)

                                'Increase the number for the last used identifier
                                Dim LastUsedId As Integer
                                LastUsedId = aaXmlDoc.Root.Attribute("LastUsedId")
                                LastUsedId += 1

                                'Update last used ID before saving
                                Dim csvElem = aaXML.DuplicateSymbol(addToGroup, obj.ObjectName, LastUsedId, obj, UseSymbolReference:=UseSymbolReference, Testing:=True)
                                'LastUsedId += 1
                                'updateGroup.Attribute("ElementID").SetValue(LastUsedId.ToString)
                                'Update last used ID before moving on
                                If tbObj Then csvElem.Element("TextStyleData").Attribute("Text").SetValue(Trim(obj.SubstituteStrings.FirstOrDefault.NewText.Split("|").LastOrDefault))
                                aaXmlDoc.Root.Attribute("LastUsedId").SetValue(LastUsedId.ToString)
                                Dim result01 As String = obj.ObjectName & " added to group " & PageNum.Key & " in graphic " & obj.GraphicFilename
                                ReturnStatusList.Add(result01)
                                groupXGE.Elements("TextBox").Where(Function(x) x.Attribute("Name").Value = "Header").FirstOrDefault.AddAfterSelf(csvElem)
                                'If Not IsNothing(csvElem) Then xSymbolsToAdd.Add(csvElem)
                            End If


                            'Update last used ID before saving
                            'rootElem.Attribute("LastUsedId").SetValue(LastUsedId.ToString)

                            'Save the xDoc with a new name so it is ready for import back to Archestra IDE
                            'aaXmlDoc.Save(newFileName)
                            'StatusResult = "aaObject " & O.ObjectName & " imported into " & Path.GetFileNameWithoutExtension(newFileName) & " using " & objectname & " standard object."
                            'Else
                            'StatusResult = "━━━━━━━ Error updating aaObject " & O.ObjectName & ".  " & xmlObjectName & " not found."
                            ' End If

                            'PRIMARY FUNCTION TO add xml elements to a new file in the IMPORTS folder with the same name as the EXPORTS folder
                            'Dim result01 As String = CreateXMLFromTemplate(aaElementToCopy, obj, updateGroup, newFileName, SidebarBuilder:=True)
                            'ReturnStatusList.Add(result01)
                        Next
                        'Remove the main header element
                        groupXGE.Elements("TextBox").Where(Function(x) x.Attribute("Name") = "Header").FirstOrDefault.Remove()
                        'Assume the header at the top of the page is the header for the page.
                        Dim topHeaderName = groupXGE.Elements("TextBox").OrderBy(Function(x) CInt(x.Attribute("Top").Value)).FirstOrDefault.Attribute("Name").Value
                        'Get the PageSelector embedded symbol
                        Dim PageSelector = updateGroup.Parent.Elements.Where(Function(x) x.Attribute("Name").Value = "PageSelector").FirstOrDefault
                        'Get the custom property that matches the current pagenum
                        Dim customProp = PageSelector.Element("CustomProperties").Elements.Where(Function(x) x.Attribute("Name").Value.Contains(PageNum.Key)).FirstOrDefault
                        'Set the exression to match the the name of our top header elment
                        customProp.Element("Expression").Attribute("Expression").SetValue(PageNum.Key & "." & topHeaderName & ".Text")
                        'GroupXMLs.Insert(0, updateGroup)
                    Next
                    Dim newFileName As String = ""
                    Dim shortName As String = graphic.Key
                    Select Case updateExisting
                        Case "Update Existing"
                            newFileName = Path.Combine(xmlFolderImport, shortName & ".xml")
                        Case "Add NEW_"
                            If graphic.Key.Length > 28 Then shortName = graphic.Key.Substring(0, 28)
                            newFileName = Path.Combine(xmlFolderImport, "NEW_" & shortName & ".xml")
                        Case "Add T2_"
                            If graphic.Key.Length > 29 Then shortName = graphic.Key.Substring(0, 29)
                            newFileName = Path.Combine(xmlFolderImport, "T2_" & shortName & ".xml")
                        Case "Add T4_"
                            If graphic.Key.Length > 29 Then shortName = graphic.Key.Substring(0, 29)
                            newFileName = Path.Combine(xmlFolderImport, "T4_" & shortName & ".xml")
                        Case Else
                            newFileName = Path.Combine(xmlFolderImport, "NEW_" & shortName & ".xml")
                    End Select
                    'ReturnStatusList.Add(result02)
                    'aaXmlDoc.Root.Element("GraphicElements").Elements("Group").FirstOrDefault.AddBeforeSelf(GroupXMLs)
                    'Get the page selector symbol
                    Dim PageSelect = aaXmlDoc.Descendants("EmbeddedSymbol").Where(Function(x) x.Attribute("Name").Value = "PageSelector").FirstOrDefault
                    'Get all the PageNumbers for the current graphic
                    Dim PagNums = graphic.Value.Select(Function(x) x.Key)
                    'Get the highest value page number for setting the wizard option
                    Dim MaxPage = PagNums.Max(Function(x) CInt(Regex.Match(x, "\d+").Value.ToString))
                    'Update the wizard option for the page numbers
                    PageSelect.Element("OverriddenWizardOptions").Elements.FirstOrDefault.Attribute("Value").SetValue(ConvertToWord(MaxPage))
                    'Remove the extra page numbers from the custom properties
                    PageSelect.Element("CustomProperties").Elements.Where(Function(x) Not PagNums.Contains(Replace(x.Attribute("Name").Value, "_Title", ""))).Remove()
                    aaXmlDoc.Save(newFileName)
                    'MessageBox.Show("Did it work?")
                Next
                'MessageBox.Show("Did it work?")
                'Dim GroupedList = aaXML.aaObjects.ToDictionary(Function(x) x.GraphicFilename, Function(y) y)
                'For Each kvp As KeyValuePair(Of String, aaObject) In GroupedList
                'kvp.Value = kvp.
                ' Next

            Else
                'OPTION B - Csv In/Out for Generic T2/T3/T4 aaSymbols

                Dim SortedaaObjects = aaXML.aaObjects.OrderBy(Function(s) s.GraphicFilename).ThenBy(Function(t) t.XposInt).ThenBy(Function(u) u.YposInt)
                'Dim showme As String = ""
                'For Each l In SortedaaObjects
                '    showme = showme & l.ObjectName & " | x=" & l.XposInt & " | y=" & l.YposInt & " | x=" & l.Xpos & vbCrLf
                'Next
                'MessageBox.Show(showme)

                'OrderBy(s >= s.PlayOrder).ThenBy(s >= s.Name)
                'Define X positions for each T4 type
                Dim xCtrl = 40
                Dim xAlarms = 550
                Dim xSCA = 1060
                Dim xTotals = 1570
                'Loop through all objects
                For Each o In SortedaaObjects 'Sorted version of aaXML.aaObjects
                    'Automatic adjustment of items for T4 displays
                    If aaTemplate = "T4_Template_For_Importer" Then
                        'Adjust left position to the correct sections
                        Select Case o.ObjectType
                            Case ObjectTypes.AI_Sidebar.ToString, ObjectTypes.AI_Basic.ToString
                                o.Xpos = xCtrl  'xTotals
                            Case ObjectTypes.Cntrl_ScadaHOA.ToString, ObjectTypes.Cntrl_Switch2pos.ToString, ObjectTypes.Cntrl_Switch3pos.ToString
                                o.Xpos = xSCA
                            Case ObjectTypes.Discrete_Alarm.ToString
                                o.Xpos = xAlarms
                            Case ObjectTypes.Discrete_Status.ToString, ObjectTypes.Multistate_Indicator.ToString, ObjectTypes.PushButton.ToString
                                o.Xpos = xCtrl
                            Case ObjectTypes.Toggle_Switch.ToString
                                o.Xpos = xCtrl
                            Case ObjectTypes.Setpoint.ToString
                                o.Xpos = xCtrl
                            Case ObjectTypes.SetpointAlarm.ToString, ObjectTypes.SetpointAlarm_DI.ToString
                                o.Xpos = xAlarms
                            Case ObjectTypes.PID.ToString
                                o.Xpos = xCtrl
                            Case ObjectTypes.RTC.ToString
                                o.Xpos = xCtrl
                            Case "<NOT_FOUND>"
                                'Do nothing
                            Case Else
                                'Do nothing
                        End Select
                    End If
                Next

                'Read the ObjectType (which comes from the name of the Group on import if the group starts with #)
                'Assign an aaGraphic Template according to that type.
                Dim yTop = 30
                Dim ySpacing = 40
                Dim CurrentGraphic = SortedaaObjects.FirstOrDefault.GraphicFilename
                'Each of the four T4 areas
                Dim yCtrl = yTop
                Dim yAlarms = yTop
                Dim ySCA = yTop
                Dim yTotals = yTop
                'Items that need additional vertical alignment
                Dim xVerticalSpacing = 150
                Dim xSS = 1080 - xVerticalSpacing
                Dim xAlm = 560 - xVerticalSpacing
                Dim xStat = 60 - xVerticalSpacing
                Dim xSS_count = 0
                Dim xAlm_count = 0
                Dim xStat_count = 0
                For Each o In SortedaaObjects 'Sorted version of aaXML.aaObjects
                    'Loop back through all objects again (after the X has been assigned) and space the Y components for each display
                    If aaTemplate = "T4_Template_For_Importer" Then
                        'Reset to the top Y and X positions each time your reach a new aaGraphic
                        If o.GraphicFilename <> CurrentGraphic Then
                            yCtrl = yTop
                            yAlarms = yTop
                            ySCA = yTop
                            yTotals = yTop
                            xSS = 1080
                            xAlm = 560
                            xStat = 60
                            CurrentGraphic = o.GraphicFilename
                        End If
                        'Increment to next Y position based on the T4 area (determined from X pos)
                        Select Case o.Xpos
                            Case xCtrl
                                yCtrl = yCtrl + ySpacing
                                o.Ypos = yCtrl
                            Case xAlarms
                                yAlarms = yAlarms + ySpacing
                                o.Ypos = yAlarms
                            Case xSCA
                                ySCA = ySCA + ySpacing
                                o.Ypos = ySCA
                            Case xTotals
                                yTotals = yTotals + ySpacing
                                o.Ypos = yTotals
                        End Select
                        'Special Case for Selector Switches to space vertically
                        Select Case o.ObjectType
                            Case ObjectTypes.Cntrl_ScadaHOA.ToString, ObjectTypes.Cntrl_Switch2pos.ToString, ObjectTypes.Cntrl_Switch3pos.ToString
                                If xSS_count > 2 Then
                                    xSS = 1080 - xVerticalSpacing
                                    xSS_count = 0
                                End If
                                xSS = xSS + xVerticalSpacing
                                o.Xpos = xSS
                                o.Ypos = o.Ypos - (ySpacing * xSS_count) 'Move it back up since it was previously sent lower
                                xSS_count = xSS_count + 1
                            Case ObjectTypes.Discrete_Alarm.ToString
                                If xAlm_count > 2 Then
                                    xAlm = 1080 - xVerticalSpacing
                                    xAlm_count = 0
                                End If
                                xAlm = xAlm + xVerticalSpacing
                                o.Xpos = xAlm
                                o.Ypos = o.Ypos - (ySpacing * xSS_count)  'Move it back up since it was previously sent lower
                                xAlm_count = xAlm_count + 1
                            Case ObjectTypes.Discrete_Status.ToString, ObjectTypes.Multistate_Indicator.ToString, ObjectTypes.PushButton.ToString
                                If xStat_count > 2 Then
                                    xStat = 1080 - xVerticalSpacing
                                    xStat_count = 0
                                End If
                                xStat = xStat + xVerticalSpacing
                                o.Xpos = xStat
                                o.Ypos = o.Ypos - (ySpacing * xSS_count)  'Move it back up since it was previously sent lower
                                xStat_count = xStat_count + 1
                        End Select
                    End If

                    'Standard objects start and end with "#"
                    Dim aaElementToCopy As String = "#" & o.ObjectType & "#"
                    'Limit filename to 32 characters
                    Dim newFileName As String = ""
                    Dim shortName As String = o.GraphicFilename
                    Select Case updateExisting
                        Case "Update Existing"
                            newFileName = Path.Combine(xmlFolderImport, shortName & ".xml")
                        Case "Add NEW_"
                            If o.GraphicFilename.Length > 28 Then shortName = o.GraphicFilename.Substring(0, 28)
                            newFileName = Path.Combine(xmlFolderImport, "NEW_" & shortName & ".xml")
                        Case "Add T2_"
                            If o.GraphicFilename.Length > 29 Then shortName = o.GraphicFilename.Substring(0, 29)
                            newFileName = Path.Combine(xmlFolderImport, "T2_" & shortName & ".xml")
                        Case "Add T4_"
                            If o.GraphicFilename.Length > 29 Then shortName = o.GraphicFilename.Substring(0, 29)
                            newFileName = Path.Combine(xmlFolderImport, "T4_" & shortName & ".xml")
                        Case Else
                            newFileName = Path.Combine(xmlFolderImport, "NEW_" & shortName & ".xml")
                    End Select

                    'PRIMARY FUNCTION TO add xml elements to a new file in the IMPORTS folder with the same name as the EXPORTS folder
                    Dim result01 As String = CreateXMLFromTemplate(aaElementToCopy, o, standardTemplate, newFileName, UseSymbolReference:=UseSymbolReference)
                    ReturnStatusList.Add(result01)
                Next


            End If

            'Delete any remaining standard objects from the xml
            Dim result02 As String = DeleteStandardObjects(DeleteLastGroup)
            ReturnStatusList.Add(result02)

            'Create a status text file output
            IO.File.WriteAllLines(Path.Combine(StatusOutputFolder, "CreateGraphicsFromaaObjects.txt"), ReturnStatusList)
            'Return status
            Return String.Join(vbCrLf, ReturnStatusList)

        Catch ex As Exception
            Return ExResults(ex)
        End Try
    End Function

    Private Function ConvertToWord(ByVal val As Integer) As String
        Dim returnStrg As String = ""
        Select Case val
            Case 1
                returnStrg = "PageCount.1_One"
            Case 2
                returnStrg = "PageCount.2_Two"
            Case 3
                returnStrg = "PageCount.3_Three"
            Case 4
                returnStrg = "PageCount.4_Four"
            Case 5
                returnStrg = "PageCount.5_Five"
            Case 6
                returnStrg = "PageCount.6_Six"
            Case 7
                returnStrg = "PageCount.7_Seven"
            Case 8
                returnStrg = "PageCount.8_Eight"
            Case 9
                returnStrg = "PageCount.9_Nine"
            Case 10
                returnStrg = "PageCount.10_Ten"
            Case 11
                returnStrg = "PageCount.11_Eleven"
            Case 12
                returnStrg = "PageCount.12_Twelve"
            Case 13
                returnStrg = "PageCount.13_Thirteen"
            Case 14
                returnStrg = "PageCount.14_Fourteen"
            Case 15
                returnStrg = "PageCount.15_Fifteen"
        End Select
        Return returnStrg
    End Function

    ''' <summary>
    ''' Removes anything EmbeddedSymbol with "#"(It's called a hashtag) in the name
    ''' </summary>
    ''' <returns></returns>
    Private Function DeleteStandardObjects(Optional ByVal delete1stgroup As Boolean = False) As String
        Dim StatusResult As String = ""
        Try
            'Get every file in the directory used for importing graphics to archestra. Only grab xml files.
            Dim files As String() = IO.Directory.GetFiles(xmlFolderImport, "*.xml")
            For Each file In files
                Dim xDoc As New XDocument
                'Load xml file into useable format
                xDoc = XDocument.Load(file)
                Dim LastElementID = CInt(xDoc.Root.Attribute("LastUsedId").Value)
                'Get the root, then the GraphicElements, then remove all descendant nodes that include both "EmbeddedSymbol" as element type and "#" as element name
                'Dim StandardObjectsCount = xDoc.Root.Element("GraphicElements").Elements.Where(Function(x) x.Attribute("Name").Value.Contains("#")).Count
                'Update the last used element ID
                'xDoc.Root.Attribute("LastUsedId").SetValue(LastElementID - StandardObjectsCount)
                'Remove 1st group if necessary
                If delete1stgroup Then xDoc.Root.Element("GraphicElements").Elements("Group").LastOrDefault.Remove()
                'Remove standard objects
                'xDoc.Root.Element("GraphicElements").Elements.Where(Function(x) x.Attribute("Name").Value.Contains("#")).Remove
                ' xDoc.Root.Elements()
                xDoc.Descendants("EmbeddedSymbol").Where(Function(x) If(Not IsNothing(x.Attribute("Name")), x.Attribute("Name").Value.Contains("#"), Nothing)).Remove

                'Dim elementIDList = xDoc.Root.Element("GraphicElements").Descendants.Where(Function(x) Not IsNothing(x.Attribute("ElementID"))).ToList
                'Update the elementID's
                'For Each elem In elementIDList
                'elem.Attribute("ElementID").SetValue(CInt(elem.Attribute("ElementID").Value) - StandardObjectsCount)
                'Next

                'Save the newly updated file
                xDoc.Save(file)
                StatusResult = "Standard objects removed from " & Path.GetFileNameWithoutExtension(file)
            Next
            'Update status

SomethingHappend:
            Return StatusResult
        Catch ex As Exception
            StatusResult = ExResults(ex)

            Return StatusResult
        End Try
    End Function


    ''' <summary>
    ''' Uses WindowTemplate_StandardObjects as base to create new archestra graphics from either csv or exported archestra graphics
    ''' </summary>
    ''' <param name="objectname"></param>
    ''' <param name="O"></param>
    ''' <returns></returns>
    Private Function CreateXMLFromTemplate(objectname As String, O As aaObject, ByVal standardTemplate As String, ByVal newFilename As String, Optional ByVal UseSymbolReference As Boolean = False, Optional ByVal FTViewObject As Boolean = False, Optional ByVal SidebarBuilder As Boolean = False) As String
        Dim aaXmlDoc As New XDocument
        Dim xmlObjectName As String = objectname
        'Dim NewFilename As String = Path.Combine(xmlFolderImport, "NEW_" & aaFilename & ".xml")
        Dim StatusResult As String = ""
        Try
            'InInitialize the xDoc by loading in the template that has one of each object type in it
            If File.Exists(newFilename) Then
                'UPDATE the existing XML file if it already exists in the Import folder. This means it was created from the template in the CREATE below.
                aaXmlDoc = XDocument.Load(newFilename)
            Else
                'CREATE the graphic using the TEMPLATE.  Successive attempts will use the UPDATE above.
                aaXmlDoc = XDocument.Load(Path.Combine(xmlFolderTemplates, standardTemplate & ".xml"))
            End If

            'Need to know last ID number so we can update it correctly
            Dim LastUsedId As Integer = aaXmlDoc.Root.Attribute("LastUsedId")
            Dim rootElem = aaXmlDoc.Root
            'Get the base aaSymbol important items as primary elements
            Dim xGE As XElement = aaXmlDoc.Root.Element("GraphicElements")
            'Second, Loop through each embedded symbol to get custom properties and options
            Dim xSymbols As List(Of XElement) = aaXmlDoc.Descendants("EmbeddedSymbol").ToList


            'Something a little different for the SidebarBuilder
            If SidebarBuilder Then



            Else
                'This one is for troubleshooting
                Dim xSymbolsToAdd As New List(Of XElement)

                'Find the object in the XML that has a 
                Dim templateElem As XElement

                If UseSymbolReference Then
                    templateElem = xSymbols.Where(Function(x) x.Attribute("EmbeddedSymbolReference").Value = O.SymbolReference).FirstOrDefault
                    xmlObjectName = O.ObjectName
                ElseIf FTViewObject Then
                    templateElem = xGE.Elements.Where(Function(x) x.Attribute("Name").Value = xmlObjectName).FirstOrDefault
                Else
                    templateElem = xSymbols.Where(Function(x) x.Attribute("Name").Value = xmlObjectName).FirstOrDefault
                    xmlObjectName = O.SourceName
                End If

                If Not IsNothing(templateElem) And Not O.ObjectType = "ROOT" Then
                    'Increase the number for the last used identifier
                    LastUsedId += 1
                    If FTViewObject Then
                        Dim csvElem = aaXML.DuplicateSymbol(templateElem, xmlObjectName, LastUsedId, O, UseSymbolReference:=UseSymbolReference, FTViewObject:=FTViewObject)
                        If Not IsNothing(csvElem) Then xGE.Add(csvElem)
                    Else
                        Dim csvElem = aaXML.DuplicateSymbol(templateElem, xmlObjectName, LastUsedId, O, UseSymbolReference:=UseSymbolReference, FTViewObject:=FTViewObject)
                        If Not IsNothing(csvElem) Then xSymbolsToAdd.Add(csvElem)
                    End If

                    'Update last used ID before saving
                    rootElem.Attribute("LastUsedId").SetValue(LastUsedId.ToString)

                    'Save the xDoc with a new name so it is ready for import back to Archestra IDE
                    aaXmlDoc.Save(newFilename)
                    StatusResult = "aaObject " & O.ObjectName & " imported into " & Path.GetFileNameWithoutExtension(newFilename) & " using " & objectname & " standard object."
                Else
                    StatusResult = "━━━━━━━ Error updating aaObject " & O.ObjectName & ".  " & xmlObjectName & " not found."
                End If

            End If

            Return StatusResult

        Catch ex As Exception
            StatusResult = ExResults(ex)
            Return StatusResult
        End Try

    End Function



    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   Standard Objects and set CustProp/Options from csv
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Public Function GetCustomPropertiesAndOptionsFromXML(ByVal fileStrg As String) As String
        Dim graphicName As String = Path.GetFileNameWithoutExtension(fileStrg)
        StatusResults = graphicName

        Try
            Dim xDoc As XDocument = XDocument.Load(fileStrg)
            'Get the base aaSymbol important items as primary elements
            Dim xGE As XElement = xDoc.Root.Element("GraphicElements")
            'Second, Loop through each embedded symbol to get custom properties and options
            Dim xSymbols As List(Of XElement) = xGE.Descendants("EmbeddedSymbol").ToList
            If IsNothing(xSymbols) Then StatusResults = "No Embedded Symbols found"

            For Each aaSym In xSymbols
                'Get each symbol
                Dim aaCustPropsOptionsValues = aaXML.GetCustomPropertiesAndOptionsFromXML(aaSym, aaSym.Attribute("Name").ToString, graphicName)
                Dim T4PageName As String = ""
                Dim T4PageNumber As String = ""
                T4PageName = aaSym.Parent.Descendants("TextBox").Where(Function(x) x.Attribute("Name").Value.StartsWith("Header_")).FirstOrDefault.Element("TextStyleData").Attribute("Text").Value
                If Not IsNothing(aaSym.Parent.Parent.Attribute("Name")) Then T4PageNumber = aaSym.Parent.Parent.Attribute("Name").Value
                If aaSym.Parent.Parent.Name.ToString = xDoc.Root.Name.ToString Then
                    T4PageNumber = ""
                End If
                aaXML.aaObjects.LastOrDefault.T4PageName = T4PageName
                aaXML.aaObjects.LastOrDefault.T4PageNumber = T4PageNumber

                If aaCustPropsOptionsValues.Count = 0 Then
                    StatusResults = StatusResults & vbCrLf & "No custom properties or options were found in the group " & aaSym.Name.ToString
                Else
                    StatusResults = StatusResults & vbCrLf & String.Join(vbCrLf, aaCustPropsOptionsValues)
                End If
            Next



            'StatusResults = StatusResults & vbCrLf & CustPropsOptions_CSV_Exporter()

            If StatusResults = aaGraphicName Then StatusResults = StatusResults & vbCrLf & "No custom properties or options were found."




#Region "Textboxes"
            'Get all text boxes within the graphic elements root.
            Dim textBoxElemnts = xGE.Descendants("TextBox").ToList
            'Output the textbox csv

            For Each tb In textBoxElemnts
                Dim aaObj As New aaObject
                aaObj.ObjectName = tb.Attribute("Name").Value
                aaObj.GraphicFilename = graphicName
                aaObj.Xpos = tb.Attribute("Left").Value.ToString
                aaObj.Ypos = tb.Attribute("Top").Value.ToString
                aaObj.SymbolReference = "TextBox"
                aaObj.SubstituteStrings.Add(New aaObject.aaString With {.ElementID = "TextBox", .NewText = tb.Element("TextStyleData").Attribute("Text").Value})
                aaXML.aaObjects.Add(aaObj)


                Dim T4PageName As String = ""
                Dim T4PageNumber As String = ""
                T4PageName = tb.Parent.Descendants("TextBox").Where(Function(x) x.Attribute("Name").Value.StartsWith("Header_")).FirstOrDefault.Element("TextStyleData").Attribute("Text").Value
                If Not IsNothing(tb.Parent.Parent.Attribute("Name")) Then T4PageNumber = tb.Parent.Parent.Attribute("Name").Value
                If tb.Parent.Parent.Name.ToString = xDoc.Root.Name.ToString Then
                    T4PageNumber = ""
                End If
                aaXML.aaObjects.LastOrDefault.T4PageName = T4PageName
                aaXML.aaObjects.LastOrDefault.T4PageNumber = T4PageNumber



                Dim tbRow As New StringBuilder
                tbRow.Append(graphicName)
                tbRow.Append("," & tb.Attribute("Name").Value)
                tbRow.Append("," & tb.Element("TextStyleData").Attribute("Text").Value)
                tbList.Add(tbRow.ToString)
            Next

#End Region
            Return StatusResults
        Catch ex As Exception
            StatusResults = ExResults(ex)
            Return StatusResults
        End Try

    End Function

    ''' <summary>
    ''' Fills in aaObject data from Custom properties and options csv
    ''' </summary>
    ''' <returns></returns>
    Public Function GetCustomPropertiesAndOptionsFromCSV(ByVal filepath As String) As String
        Dim StatusText As String = "Reading CSV...."
        Dim CustomProperties As New List(Of String)
        Dim Options As New List(Of String)
        Dim SubStrings As New List(Of String)
        aaXML.aaObjects.Clear()
        Try
            If File.Exists(filepath) Then
                Dim CSVList = File.ReadAllLines(filepath).ToList
                If CSVList.Count > 0 Then
                    Dim HeaderList = CSVList(0).Split(",").ToList
                    Dim Header As New Dictionary(Of String, Integer)
                    'Get the custom properties and options along with their column indexes
                    For i As Integer = 0 To HeaderList.Count - 1
                        Header.Add(HeaderList(i), i)
                    Next
                    'Get all the custom properties into one list
                    CustomProperties.AddRange(HeaderList.Where(Function(x) x.StartsWith("CP:")).ToList)
                    'Get all the options into one list
                    Options.AddRange(HeaderList.Where(Function(x) x.StartsWith("O:")).ToList)
                    SubStrings.AddRange(HeaderList.Where(Function(x) x.StartsWith("S:")).ToList)
                    For i As Integer = 1 To CSVList.Count - 1
                        Dim ObjList = CSVList(i).Split(",").ToList
                        'Only add object to update list if the update option is checked
                        If ObjList(0).ToLowerInvariant = "x" Then
                            'TODO - Clean up the T4_SideBar selection code throughout 
                            If ExportCsvAsT4sidebar Then
                                'OPTION A - Csv In/Out for T4_Sidebar_Builder
                                Dim aObj As aaObject = New aaObject With {.GraphicFilename = ObjList(1), .T4PageNumber = Regex.Match(ObjList(2), "\d+").Value.TrimStart("0"), .T4PageName = ObjList(3), .ObjectName = ObjList(4), .Xpos = ObjList(5), .Ypos = ObjList(6), .SymbolReference = ObjList(7)}
                                If aObj.T4PageNumber = "" Then aObj.T4PageNumber = "0"
                                For Each Prop In CustomProperties
                                    aObj.CustProps.Add(New aaObject.aaCustProp With {.Name = Prop.Replace("CP:", ""), .Value = ObjList(Header(Prop))})
                                    StatusText = StatusText & vbCrLf & "Custom Property: " & aObj.CustProps.LastOrDefault.Name & " added to " & aObj.ObjectName
                                Next

                                For Each opt In Options
                                    aObj.Options.Add(New aaObject.aaOption With {.Name = opt.Replace("O:", ""), .Value = ObjList(Header(opt))})
                                    StatusText = StatusText & vbCrLf & "Wizard Option: " & aObj.Options.LastOrDefault.Name & " added to " & aObj.ObjectName
                                Next

                                For Each strg In SubStrings
                                    aObj.SubstituteStrings.Add(New aaObject.aaString With {.NewText = ObjList(Header(strg)), .Order = SubStrings.IndexOf(strg)})
                                    StatusText = StatusText & vbCrLf & "Substring: " & aObj.SubstituteStrings.LastOrDefault.NewText & " added to " & aObj.ObjectName
                                Next
                                aObj.ObjectType = aObj.SymbolReference.Replace("Symbol:", "").Replace("#", "")
                                aaXML.aaObjects.Add(aObj)

                            Else
                                'OPTION B - Csv In/Out for Generic T2/T3/T4 aaSymbols
                                Dim aObj As aaObject = New aaObject With {.GraphicFilename = ObjList(1), .ObjectName = ObjList(2), .Xpos = ObjList(3), .Ypos = ObjList(4), .SymbolReference = ObjList(5)}

                                For Each Prop In CustomProperties
                                    aObj.CustProps.Add(New aaObject.aaCustProp With {.Name = Prop.Replace("CP:", ""), .Value = ObjList(Header(Prop))})
                                    StatusText = StatusText & vbCrLf & "Custom Property: " & aObj.CustProps.LastOrDefault.Name & " added to " & aObj.ObjectName
                                Next

                                For Each opt In Options
                                    aObj.Options.Add(New aaObject.aaOption With {.Name = opt.Replace("O:", ""), .Value = ObjList(Header(opt))})
                                    StatusText = StatusText & vbCrLf & "Wizard Option: " & aObj.Options.LastOrDefault.Name & " added to " & aObj.ObjectName
                                Next
                                aObj.ObjectType = aObj.SymbolReference.Replace("Symbol:", "").Replace("#", "")
                                aaXML.aaObjects.Add(aObj)
                            End If
                        Else
                            'Do nothing
                        End If
                    Next
                End If
            Else
                StatusText = "The file located at """ & filepath & """ does not exist."
            End If

        Catch ex As Exception
            StatusText = ex.ToString
        End Try
        Return StatusText
    End Function



    ''' <summary>
    ''' Creates CSV output of custom properties and options retrieved from archestra xml file
    ''' </summary>
    ''' <returns></returns>
    Public Function WriteCustomPropertiesAndOptionsToCSV() As String
        Dim StatusText As String = ""
        Dim CSVList As New List(Of String)

        'ExportCsvAsT4sidebar
        Try
#Region "TextBoxes"

            Dim tbHeader As New StringBuilder
            tbHeader.Append("GraphicName,TB:Name,TB:Value")
            'Add header to begging of list.
            If Not tbList(0).Contains("GraphicName") Then tbList.Insert(0, tbHeader.ToString)
            Dim tbFilename As String = Path.Combine(StatusOutputFolder, "aaTextBoxes.csv")
            IO.File.WriteAllLines(tbFilename, tbList)

#End Region



            'Get the Header started
            Dim header As StringBuilder = New StringBuilder("Update,T4Page,BaseGraphic,Name,PosLeft,PosTop,SybmolReference")
            Dim headerList As List(Of String) = {"Update", "BaseGraphic", "T4PageNumber", "T4PageHeader", "EmbeddedSymbolName", "PosLeft", "PosTop", "SymbolReference"}.ToList

            'TODO - Clean up the T4_SideBar selection code throughout 
            If ExportCsvAsT4sidebar Then
                'OPTION A - Csv In/Out for T4_Sidebar_Builder

                'Make sure the columns for CP, O, and SS are all together by object
                headerList.Add("CP:ObjName")
                Dim substringslist As New List(Of aaObject.aaString)
                For Each o In aaXML.aaObjects
                    substringslist.AddRange(o.SubstituteStrings)
                Next
                Dim Unique = substringslist.Select(Function(x) x.Order).Distinct.OrderBy((Function(x) x))
                For Each index In Unique
                    headerList.Add("S:" & index)
                Next

                For Each o In aaXML.aaObjects
                    'For Each strg In o.SubstituteStrings
                    '    headerList.Add("S:" & strg.ElementID)
                    'Next
                    For Each custProp In o.CustProps
                        If Not custProp.Name = "ObjName" Then
                            headerList.Add("CP:" & custProp.Name)
                        End If
                    Next
                    For Each opt In o.Options
                        headerList.Add("O:" & opt.Name)
                    Next
                Next
                headerList = headerList.Distinct.ToList
                CSVList.Add(String.Join(",", headerList))
            Else
                'OPTION B - Csv In/Out for Generic T2/T3/T4 aaSymbols

                'Add custom properties to the header
                For Each custProp In aaXML.AllaaCustomProperites
                    header.Append(",CP:" & custProp)
                Next
                'All the Options to the header
                For Each opt In aaXML.AllaaOptions
                    header.Append(",O:" & opt)
                Next
                'All the SubStrings to the header
                For Each t In aaXML.AllaaSubStrings
                    header.Append(",S:" & t)
                Next
                CSVList.Add(header.ToString)
            End If


            For Each obj In aaXML.aaObjects
                Dim csvHeader = CSVList(0)
                'Check for any custom properties or options attached to embedded symbol. If nothing then we don't want this in the output
                If obj.CustProps.Count > 0 Or obj.Options.Count > 0 Or obj.SymbolReference.ToLowerInvariant.Contains("textbox") Then
                    'Start the string builder
                    Dim PageName As String = "" 'TODO - Currently used for T4_Sidebar_Builder Page only, but eventually apply to groups for generic in/out
                    Dim sb As StringBuilder = New StringBuilder("x," & obj.GraphicFilename & "," & PageName & "," & obj.ObjectName & "," & obj.Xpos & "," & obj.Ypos & "," & obj.SymbolReference)
                    Dim NewCsvArray(headerList.Count - 1) As String
                    Dim NewCsvRow = NewCsvArray.ToList
                    NewCsvRow(0) = "x"
                    NewCsvRow(1) = obj.GraphicFilename
                    NewCsvRow(2) = obj.T4PageNumber
                    NewCsvRow(3) = obj.T4PageName
                    NewCsvRow(4) = obj.ObjectName
                    NewCsvRow(5) = obj.Xpos
                    NewCsvRow(6) = obj.Ypos
                    NewCsvRow(7) = obj.SymbolReference

                    'NewCsvRow.AddRange(sb.ToString.Split(",").ToList)
                    'NewCsvRow

                    'TODO - Clean up the T4_SideBar selection code throughout 
                    If ExportCsvAsT4sidebar Then
                        'OPTION A - Csv In/Out for T4_Sidebar_Builder

                        'Make sure the columns for CP, O, and SS are all together by object
                        Dim objCustProp = obj.CustProps.Where(Function(x) x.Name = "ObjName").ToList
                        If objCustProp.Count > 0 Then
                            NewCsvRow(8) = objCustProp(0).Value
                        Else
                            NewCsvRow(8) = ""
                        End If
                        'For i As Integer = 0 To obj.SubstituteStrings.Count - 1
                        '    Dim showElementID As Boolean = True
                        '    If showElementID Then
                        '        Dim LastPart As String = obj.SubstituteStrings(i).ElementID.Split(".").LastOrDefault
                        '        NewCsvRow(headerList.IndexOf("S:" & i)) = LastPart & " | " & obj.SubstituteStrings(i).NewText
                        '    Else
                        '        NewCsvRow(headerList.IndexOf("S:" & i)) = obj.SubstituteStrings(i).NewText
                        '    End If
                        'Next

                        For Each strg In obj.SubstituteStrings
                            Dim showElementID As Boolean = True
                            If showElementID Then
                                Dim LastPart As String = strg.ElementID.Split(".").LastOrDefault
                                NewCsvRow(headerList.IndexOf("S:" & strg.Order)) = LastPart & " | " & strg.NewText
                            Else
                                NewCsvRow(headerList.IndexOf("S:" & strg.Order)) = strg.NewText
                            End If

                        Next
                        For Each prop In obj.CustProps
                            If prop.Name = "ObjName" Then
                                'Do nothing
                            Else
                                NewCsvRow(headerList.IndexOf("CP:" & prop.Name)) = prop.Value
                            End If

                        Next
                        For Each opt In obj.Options
                            NewCsvRow(headerList.IndexOf("O:" & opt.Name)) = opt.Value
                        Next

                    Else
                        'OPTION B - Csv In/Out for Generic T2/T3/T4 aaSymbols

                        'Loop through each custom property and see if it exists within the object
                        For Each prop In aaXML.AllaaCustomProperites
                            Dim Val As String = ""
                            Dim PropList = obj.CustProps.Select(Function(x) x.Name).ToList
                            'If the property exists then fill in the value, if not then leave blank
                            If PropList.Contains(prop) Then
                                Val = obj.CustProps.Where(Function(x) x.Name = prop).FirstOrDefault.Value
                            End If
                            'Update the string builder
                            sb.Append("," & Val)
                        Next
                        'Loop through each option and see if it exists within in the object
                        For Each opt In aaXML.AllaaOptions
                            Dim Val As String = ""
                            Dim OptList = obj.Options.Select(Function(x) x.Name).ToList
                            'If the option exists then fill in the value, if not then leave blank
                            If OptList.Contains(opt) Then
                                Val = obj.Options.Where(Function(x) x.Name = opt).FirstOrDefault.Value
                            End If
                            'Update the string builder
                            sb.Append("," & Val)
                        Next
                        'Loop through each option and see if it exists within in the object
                        For Each s In aaXML.AllaaSubStrings
                            Dim Val As String = ""
                            Dim SubList = obj.SubstituteStrings.Select(Function(x) x.Order).ToList
                            'If the option exists then fill in the value, if not then leave blank
                            If SubList.Contains(s) Then
                                Val = obj.SubstituteStrings.Where(Function(x) x.Order = s).FirstOrDefault.NewText
                            End If
                            'Update the string builder
                            sb.Append("," & Val)
                        Next
                    End If

                    'Finally, update the csv output with our object
                    'TODO - Clean up the T4_SideBar selection code throughout 
                    If ExportCsvAsT4sidebar Then
                        'OPTION A - Csv In/Out for T4_Sidebar_Builder
                        Dim stringy = String.Join(",", NewCsvRow)
                        CSVList.Add(stringy)
                    Else
                        'OPTION B - Csv In/Out for Generic T2/T3/T4 aaSymbols
                        CSVList.Add(sb.ToString)
                    End If
                Else
                    'Do nothing 
                End If
            Next

            'Write csv
            Dim filename As String = ""
            If ExportCsvAsT4sidebar Then filename = Path.Combine(StatusOutputFolder, "aaCustomPropsAndOptions_T4.csv") Else filename = Path.Combine(StatusOutputFolder, "aaCustomPropsAndOptions.csv")
            IO.File.WriteAllLines(filename, CSVList)
            StatusText = "All objects exported to CSV"
        Catch ex As Exception
            StatusText = ex.ToString
        End Try
        Return StatusText
    End Function


#Region "FTViewSE"
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   Old Intouch Graphics manipulation
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Public Function GetAnimationsFromXml_FTViewSE(ByVal filename As String) As String 'Also saves as csv
        StatusResults = aaGraphicName
        Dim xmlDocToParse As New XDocument

        Try
            xmlDocToParse = XDocument.Load(filename)
            'Get the Last Used ElementID so that when you add new elements you can assign them a number
            'Dim LastUsedId As Integer = xmlDocToParse.Root.Attribute("LastUsedId")

            'Get the base aaSymbol important items as primary elements
            Dim base_GraphicElements As XElement = xmlDocToParse.Root
            'Dim base_CustomProperties As XElement = xmlDocToParse.Root.Element("CustomProperties")
            'Dim base_NamedScripts As XElement = xmlDocToParse.Root.Element("NamedScripts")
            'Dim base_PredefinedScripts As XElement = xmlDocToParse.Root.Element("PredefinedScripts")

            'Set the atrribute you are using for lookups
            Dim xAttrName = "Name"
            Dim xPrefix = "#"

            'Get all archestra groups that have names that start with "#"
            'Dim xGroupObjs = base_GraphicElements.Elements.Where(Function(x) x.Attribute(xAttrName).Value.StartsWith(xPrefix)).ToList
            Dim xGroupObjs = GetSimpleObjectsXMLElements_FTViewSE(base_GraphicElements)
            Dim xAnimationObjs = base_GraphicElements.Descendants.Where(Function(x) Not IsNothing(x.Element("animations"))).ToList

            'Get all the SimpleObjects
            If IsNothing(xGroupObjs) Then Return "No simple objects were found"
            For Each obj In xGroupObjs
                Dim tempElem = obj
                Dim grpStrg As String = ""
                While tempElem.Parent.Name = "group"
                    If grpStrg <> "" Then grpStrg = tempElem.Parent.Attribute("name").Value & ">" & grpStrg Else grpStrg = tempElem.Parent.Attribute("name").Value
                    tempElem = tempElem.Parent
                End While

                Dim newObj = GetFTViewaaObjects(obj, filename)
                If grpStrg <> "" Then newObj.FTViewGroupPath = grpStrg Else newObj.FTViewGroupPath = "N/A"
                If Not aaXML.aaObjects.Exists(Function(x) x.GraphicFilename = newObj.GraphicFilename And x.ObjectName = newObj.ObjectName) Then
                        aaXML.aaObjects.Add(newObj)
                        'aaXML.aaObjects.Add(GetFTViewaaObjects(obj, filename))
                    Else
                        'Do nothing
                    End If
            Next

            'If IsNothing(xAnimationObjs) Then Return "No animations were found"
            'For Each item In xAnimationObjs
            '    'Get all the animation expressions for this element as list of string in the format "aaObject|aaAnimation"
            '    Dim aaAnimationValues As List(Of String) = aaXML.GetAnimationsFromOneItem_FTViewSE(aaGraphicName, item, xAttrName, xPrefix)
            '    'Results
            '    If aaAnimationValues.Count = 0 Then
            '        'REMOVED DUE TO CLUTTER   StatusResults = StatusResults & vbCrLf & "No animations were found in the group " & item.Name.ToString
            '    Else
            '        StatusResults = StatusResults & vbCrLf & String.Join(vbCrLf, aaAnimationValues)
            '    End If
            'Next


            ''Find all animation types and values
            'Dim AllAnimationsList As New List(Of String)
            'AllAnimationsList.Add(vbCrLf & "Animation Types Obtained:")
            'For Each item In aaXML.aaObjects
            '    For Each anim In item.Animations
            '        If Not AllAnimationsList.Contains("⮡  " & anim.AnimType) Then AllAnimationsList.Add("⮡  " & anim.AnimType)
            '    Next
            'Next
            'AllAnimationsList.Add(vbCrLf & "Animation Values Obtained:")
            'For Each item In aaXML.aaObjects
            '    For Each anim In item.Animations
            '        If Not AllAnimationsList.Contains("⮡  " & anim.Value) Then AllAnimationsList.Add("⮡  " & anim.Value)
            '    Next
            'Next

            'AllAnimationsList.Add(vbCrLf & "Properties Report:")





            'StatusResults = StatusResults & vbCrLf & String.Join(vbCrLf, AllAnimationsList)

            ''Return
            If StatusResults = aaGraphicName Then StatusResults = StatusResults & vbCrLf & "No animations were found in " & aaGraphicName & "."
            Return StatusResults
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            StatusResults = ExResults(ex)
            Return StatusResults
        End Try

    End Function


    Private Function GetFTViewaaObjects(ByVal obj As XElement, ByVal filename As String) As aaObject
        Dim newObj As New aaObject

        Try


            'Get the properties of the element
            newObj.xElem = obj
            newObj.GraphicFilename = Path.GetFileNameWithoutExtension(filename).Replace(" ", "_")
            newObj.SourceName = obj.Attribute("name").Value
            newObj.ObjectName = newObj.SourceName

            'Get all attributes first so they can be adjust if necessary
            'Get the fontsize if the attribute exists
            If Not IsNothing(obj.Attribute("fontSize")) Then newObj.FontSize = obj.Attribute("fontSize").Value
            'Text alignment is either alignment or justification so just try both
            If Not IsNothing(obj.Attribute("justification")) Then
                Select Case obj.Attribute("justification").Value.ToLowerInvariant
                    Case "left"
                        newObj.Alignment = "MiddleLeft"
                    Case "center"
                        newObj.Alignment = "Centers"
                    Case "right"
                        newObj.Alignment = "MiddleRight"
                    Case Else
                        newObj.Alignment = "Centers"
                End Select
            End If

            If Not IsNothing(obj.Attribute("alignment")) Then
                Select Case obj.Attribute("alignment").Value.ToLowerInvariant
                    Case "topleft"
                        newObj.Alignment = "TopLeft"
                    Case "topcenter"
                        newObj.Alignment = "TopCenter"
                    Case "topright"
                        newObj.Alignment = "TopRight"
                    Case "middleleft"
                        newObj.Alignment = "MiddleLeft"
                    Case "middlecenter"
                        newObj.Alignment = "Centers"
                    Case "middleright"
                        newObj.Alignment = "MiddleRight"
                    Case "bottomleft"
                        newObj.Alignment = "BottomLeft"
                    Case "bottomcenter"
                        newObj.Alignment = "BottomCenter"
                    Case "bottomright"
                        newObj.Alignment = "BottomRight"
                End Select
            End If


            'Get the sizing if attributes exist
            If Not IsNothing(obj.Attribute("left")) Then newObj.Xpos = obj.Attribute("left").Value
            If Not IsNothing(obj.Attribute("top")) Then newObj.Ypos = obj.Attribute("top").Value
            If Not IsNothing(obj.Attribute("height")) Then newObj.Height = obj.Attribute("height").Value
            If Not IsNothing(obj.Attribute("width")) Then newObj.Width = obj.Attribute("width").Value

            newObj.ObjectType = obj.Name.ToString.ToLowerInvariant

            Dim genericNumberStrg As String = "#.##"
            Dim genericStrg As String = "ssssss"
            'Parse xml based on object type
            Select Case obj.Name.ToString.ToLowerInvariant
                Case "rectangle"
                    newObj.ObjectType = "rectangle"
                Case "polygon"
                    newObj.ObjectType = "rectangle"
                    newObj.ObjectName = "POLY_" & obj.Attribute("name").Value
                Case "text"
                    newObj.TextElements.Add(obj.Attribute("caption").Value)
                Case "taglabel"
                    newObj.TextElements.Add(obj.Attribute("tag").Value)
                Case "stringdisplay", "numericdisplay", "numericinput"
                    Dim exp = obj.Element("connections").Element("connection").Attribute("expression").Value
                    If obj.Name.ToString.ToLowerInvariant = "stringdisplay" Then
                        newObj.TextElements.Add(genericStrg)
                    Else
                        newObj.TextElements.Add(genericNumberStrg)
                    End If
                    newObj.TextElements.Add(exp)
                Case "button"
                    newObj.ObjectType = "button"
                    Dim captionElem = obj.Element("up").Element("caption")
                    newObj.FontSize = captionElem.Attribute("fontSize").Value
                    newObj.TextElements.Add(captionElem.Attribute("caption").Value)
                Case "line"
                    newObj.ObjectType = "line"
                Case Else
                    newObj.ObjectType = obj.Name.ToString
            End Select

            Select Case obj.Name.ToString.ToLowerInvariant
                Case "text", "taglabel", "stringdisplay", "numericdisplay", "numericinput"
                    'Adjust the size of the text box
                    If newObj.FontSize <> "" Then
                        newObj.Height = CInt(newObj.FontSize) * 2
                        newObj.Width = CInt(newObj.FontSize) * newObj.TextElements(0).Length * 2 / 3
                    Else
                        newObj.FontSize = Convert.ToInt64(CInt(obj.Attribute("charHeight").Value) * 72 / 95.142857)
                        newObj.Height = CInt(newObj.FontSize) * 2
                        newObj.Width = CInt(newObj.FontSize) * newObj.TextElements(0).Length * 2 / 3
                        'newObj.Width = CInt(newObj.FontSize) * CInt(obj.Attribute("width").Value) * 2 / 3
                    End If
                Case Else
                    'Do nothing
            End Select



            Return newObj


        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            MessageBox.Show(newObj.ObjectName)
            Return newObj
        End Try
    End Function

    Function GetSimpleObjectsXMLElements_FTViewSE(ByVal elem As XElement) As List(Of XElement)
        Dim simpleObjects As New List(Of XElement)
        Dim objectsList As New List(Of String)(New String() {"text", "line", "stringDisplay", "numericDisplay", "numericInput", "tagLabel", "button", "rectangle", "polygon"})
        'Get any element that is named in the objects list
        simpleObjects.AddRange(elem.Descendants.Where(Function(x) objectsList.Contains(x.Name.ToString)).ToList)

        Return simpleObjects
    End Function


    Private Function FTViewToaaObjectAnimationConverstion(ByVal ftviewanim As XElement) As String
        Dim returnStrg As String = ""
        Select Case ftviewanim.Name.ToString.ToLower.Replace("animate", "")
            Case "visibilty"
                Dim expression As String = ftviewanim.Attribute("expression").Value
            Case "touch"
                Dim PressAction As String = ftviewanim.Attribute("pressAction").Value
                Dim ReleaseAction As String = ftviewanim.Attribute("releaseAction").Value
            Case "fill"
                Dim expression As String = ftviewanim.Attribute("expression").Value
            Case "color"
                Dim value As String = ""
                Dim foreBehave As String = ""
                Dim foreColor1 As String = ""
                Dim foreColor2 As String = ""
                Dim backBehave As String = ""
                Dim backColor1 As String = ""
                Dim backColor2 As String = ""
                Dim fillColorMode As String = ""
            Case Else

        End Select
        Return returnStrg
    End Function



    ''' <summary>
    ''' Determines which standard ojbect to copy then calls creates windows from standard templates and deletes standard ojbects from new windows
    ''' </summary>
    ''' <returns></returns>
    Public Function CreateGraphicsFromaaObjects_FTViewSE(ByVal standardTemplate As String, ByVal updateExisting As String) As String
        Dim ReturnStatusList As New List(Of String)
        ReturnStatusList.Add(aaGraphicName)
        Try
            'The function below ADDS xml elements to the file if it exists, so we FIRST need to delete any existing versions
            For Each o In aaXML.aaObjects
                If IO.File.Exists(o.GraphicFilename) Then IO.File.Delete(o.GraphicFilename)
            Next
        Catch ex As Exception
        End Try
        Try
            'Read the ObjectType (which comes from the name of the Group on import if the group starts with #)
            'Assign an aaGraphic Template according to that type.
            For Each o In aaXML.aaObjects
                '*NOTE* Get the correct element name WAS MOVED to the original import as the function named Equate_ImportedObjectType_to_CorrentObjectType.  Now, you just use O.ObjectType and O.ObjectTypeImported.

                'Standard objects start and end with "#"
                Dim aaElementToCopy As String = "#" & o.ObjectType & "#"
                'PRIMARY FUNCTION TO add xml elements to a new file in the IMPORTS folder with the same name as the EXPORTS folder
                Dim newFileName As String = ""
                Select Case updateExisting
                    Case "Update Existing"
                        newFileName = Path.Combine(xmlFolderImport, o.GraphicFilename & ".xml")
                    Case "Add NEW_"
                        newFileName = Path.Combine(xmlFolderImport, "NEW_" & o.GraphicFilename & ".xml")
                    Case Else
                        newFileName = Path.Combine(xmlFolderImport, "NEW_" & o.GraphicFilename & ".xml")
                End Select
                Dim result01 As String = CreateXMLFromTemplate(aaElementToCopy, o, standardTemplate, newFileName, FTViewObject:=True)
                ReturnStatusList.Add(result01)
            Next

            'Delete any remaining standard objects from 
            Dim result02 As String = DeleteStandardObjects()
            ReturnStatusList.Add(result02)

            IO.File.WriteAllLines(Path.Combine(StatusOutputFolder, "CreateGraphicsFromaaObjects.txt"), ReturnStatusList)

            Return String.Join(vbCrLf, ReturnStatusList)
        Catch ex As Exception
            Return ExResults(ex)
        End Try
    End Function
#End Region

#Region "aaGroups"
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   Get those groups yo
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    ''' <summary>
    ''' Reads in gropus to the aaGroups list from the base level "GraphicElements" of the archestra graphic xml
    ''' </summary>
    ''' <param name="filename"></param>
    ''' <returns></returns>
    Public Function GetGroupsFromXL(ByVal filename As String) As String 'Also saves as csv
        Dim xmlDocToParse As New XDocument
        Try
            aaGraphicName = Path.GetFileNameWithoutExtension(filename)
            StatusResults = aaGraphicName
            xmlDocToParse = XDocument.Load(filename)
            'Get the base aaSymbol important items as primary elements
            Dim base_GraphicElements As XElement = xmlDocToParse.Root

            'Get the base groups
            Dim GroupSyms = base_GraphicElements.Element("GraphicElements").Elements("Group").ToList

            'Make sure we have something before continuing
            If IsNothing(GroupSyms) Then
                StatusResults = StatusResults & "No groups were found in " & filename
                Return StatusResults
            End If
            'Save the groups to the group obj
            If GroupSyms.Count < 1 Then
                StatusResults = StatusResults & vbCrLf & "No groups were found in " & aaGraphicName & "."
            Else
                For Each groupObj In GroupSyms
                    Dim newGroup As New aaGroupImport
                    'Fill in the group values
                    newGroup.GraphicFilename = Path.GetFileNameWithoutExtension(filename)
                    newGroup.GroupName = groupObj.Attribute("Name").Value
                    newGroup.Xpos = groupObj.Attribute("Left").Value
                    newGroup.Ypos = groupObj.Attribute("Top").Value
                    Dim elemHeader = groupObj.Element("GraphicElements").Elements.Where(Function(x) x.Attribute("Name").Value.StartsWith("Header_")).FirstOrDefault
                    If Not IsNothing(elemHeader) Then
                        newGroup.GroupHeader = elemHeader.Attribute("Name").Value
                        newGroup.GroupHeaderValue = elemHeader.Element("TextStyleData").Attribute("Text").Value
                    End If
                    If groupObj.Element("GraphicElements").Elements("EmbeddedSymbol").ToList.Count < 2 Then
                        newGroup.GroupIsEmpty = True
                    End If
                    newGroup.NewGroupName = newGroup.GroupName
                    aaXML.aaGroups.Add(newGroup)
                    'Update the status
                    StatusResults = StatusResults & vbCrLf & newGroup.GroupName & " | " & newGroup.Xpos & " | " & newGroup.Ypos
                Next
            End If

            Return StatusResults
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            StatusResults = ExResults(ex)
            Return StatusResults
        End Try

    End Function


    ''' <summary>
    ''' Writes the aaGroups list ot a csv titled "aaGroups.csv"
    ''' </summary>
    ''' <returns></returns>
    Public Function WriteGroupsToCSV() As String
        Dim StatusText As String = ""
        Dim CSVList As New List(Of String)

        Try
            'Get the header started
            Dim header As StringBuilder = New StringBuilder("Graphic Name,<New Graphic Name>,Group Name,<New Group Name>,PosLeft,PosTop,GroupIsEmpty,GroupHeader,GroupHeaderValue")

            'Add header to csv list
            CSVList.Add(header.ToString)

            'Loop through group objects and fill the list of string
            For Each groupObj In aaXML.aaGroups
                Dim sb As New StringBuilder
                sb.Append(groupObj.GraphicFilename)
                sb.Append("," & groupObj.NewGraphicFilename)
                sb.Append("," & groupObj.GroupName)
                sb.Append("," & groupObj.NewGroupName)
                sb.Append("," & groupObj.Xpos)
                sb.Append("," & groupObj.Ypos)
                sb.Append("," & groupObj.GroupIsEmpty)
                sb.Append("," & groupObj.GroupHeader)
                sb.Append("," & groupObj.GroupHeaderValue)
                CSVList.Add(sb.ToString)
            Next

            'Write csv
            Dim filename = Path.Combine(StatusOutputFolder, "aaGroups.csv")
            IO.File.WriteAllLines(filename, CSVList)
            StatusText = "All groups exported to CSV"
            'Process.Start(filename)
        Catch ex As Exception
            StatusText = ex.ToString
        End Try
        Return StatusText
    End Function


    ''' <summary>
    ''' Reads old xml file, updates the associated group object, removes other groups, then saves as new file for each object in aaGroups list.
    ''' </summary>
    ''' <returns></returns>
    Public Function UpdateGroupNames() As String
        Dim statusResults As String = ""
        Dim DeleteXmlList As New List(Of String)
        Dim DiscardGroupsList As New List(Of aaGraphicGroupPair)
        Try
            'Delete all existing items in the import folder
            'TODO - Ask user for confirmation
            For Each f In IO.Directory.GetFiles(xmlFolderImport)
                IO.File.Delete(f)
            Next
        Catch ex As Exception

        End Try
        Try
            'Load the list with all graphic names. Remove them in the next loop.
            For Each g In aaXML.aaGroups
                DeleteXmlList.Add(g.GraphicFilename)
                If g.NewGraphicFilename.Trim = "" Then g.NewGraphicFilename = g.GraphicFilename
            Next
            'Loop through each object and migrate from export to import folder
            For Each groupObj In aaXML.aaGroups
                Dim filepath = Path.Combine(xmlFolderExport, groupObj.GraphicFilename & ".xml")
                Dim newPath = Path.Combine(xmlFolderImport, groupObj.NewGraphicFilename & ".xml")
                'If the new graphic name is the same as the old, then remove from the "delete" list since it will be updated
                If groupObj.GraphicFilename = groupObj.NewGraphicFilename Then
                    Try
                        DeleteXmlList.Remove(groupObj.GraphicFilename)
                    Catch ex As Exception 'FIX LATER - Ignore if the item already exists in the list
                    End Try
                End If
                'Create a new graphic based on the old graphic
                If Not IO.File.Exists(newPath) Then IO.File.Copy(filepath, newPath)
                'Flag groups that are no longer needed so they can be removed later
                If (groupObj.NewGroupName <> groupObj.GroupName) And (groupObj.NewGroupName.Trim <> "") Then
                    Try
                        DiscardGroupsList.Add(New aaGraphicGroupPair With {.Graphic = groupObj.NewGraphicFilename, .Group = groupObj.GroupName})
                    Catch ex As Exception  'FIX LATER - Ignore if the item already exists in the list
                    End Try
                End If
            Next
            statusResults = statusResults & vbCrLf & "First Loop Complete"
            'Loop again to avoid timing issues with the file being created
            For Each groupObj In aaXML.aaGroups
                Dim newPath = Path.Combine(xmlFolderImport, groupObj.NewGraphicFilename & ".xml")
                '
                If File.Exists(newPath) Then
                    'Load the xDoc
                    Dim xmlDocToParse As XDocument = XDocument.Load(newPath)
                    'If the new groupname is blank, then update the old group
                    If groupObj.NewGroupName.Trim = "" Then
                        'Update the OLD group with new position only
                        Dim UpdateThisOne = xmlDocToParse.Root.Element("GraphicElements").Elements("Group").ToList.Where(Function(x) x.Attribute("Name").Value = groupObj.GroupName).FirstOrDefault
                        If Not IsNothing(UpdateThisOne) Then
                            UpdateThisOne.Attribute("Left").SetValue(groupObj.Xpos)
                            UpdateThisOne.Attribute("Top").SetValue(groupObj.Ypos)
                            Dim elemHeader = UpdateThisOne.Element("GraphicElements").Elements.Where(Function(x) x.Attribute("Name").Value = groupObj.GroupHeader).FirstOrDefault
                            If Not IsNothing(elemHeader) Then elemHeader.Element("TextStyleData").Attribute("Text").Value = groupObj.GroupHeaderValue
                            Dim stoptotest = 1
                        Else
                            MessageBox.Show("Could not find " & groupObj.GroupName & " inside " & groupObj.NewGraphicFilename & ".xml.")
                        End If
                    Else
                        'Update the group name with its NEW group name and new position
                        Dim UpdateThisOne = xmlDocToParse.Root.Element("GraphicElements").Elements("Group").ToList.Where(Function(x) x.Attribute("Name").Value = groupObj.GroupName).FirstOrDefault
                        If Not IsNothing(UpdateThisOne) Then
                            UpdateThisOne.Attribute("Name").SetValue(groupObj.NewGroupName)
                            UpdateThisOne.Attribute("Left").SetValue(groupObj.Xpos)
                            UpdateThisOne.Attribute("Top").SetValue(groupObj.Ypos)
                            Dim elemHeader = UpdateThisOne.Element("GraphicElements").Elements.Where(Function(x) x.Attribute("Name").Value = groupObj.GroupHeader).FirstOrDefault
                            If Not IsNothing(elemHeader) Then elemHeader.Element("TextStyleData").Attribute("Text").Value = groupObj.GroupHeaderValue
                            Dim stoptotest = 1
                        Else
                            MessageBox.Show("Could not find " & groupObj.GroupName & " inside " & groupObj.NewGraphicFilename & ".xml.")
                        End If
                    End If

                    'TODO <MODIFY> - Doesn't account for different csv rows doing the same thing
                    Dim GroupsToRemove = xmlDocToParse.Root.Element("GraphicElements").Elements("Group").ToList.Where(Function(x) x.Attribute("Name").Value <> groupObj.NewGroupName).ToList
                    For Each elem In GroupsToRemove
                        elem.Remove()
                        statusResults = statusResults & vbCrLf & "Removed " & groupObj.NewGraphicFilename & ".xml." & " | Group: " & elem.Attribute("Name").Value
                    Next
                    xmlDocToParse.Save(newPath)
                    'MSmith REMOVED - Create new graphic name
                    'Dim newPath = Path.Combine(xmlFolderImport, groupObj.NewGraphicFilename & ".xml")
                    'xmlDocToParse.Save(newPath)
                    'statusResults = statusResults & vbcrlf &  vbCrLf & groupObj.NewGraphicFilename & " created."
                    statusResults = statusResults & vbCrLf & "Updated " & groupObj.NewGraphicFilename & ".xml." & " | Group: " & groupObj.GroupName
                Else
                    MessageBox.Show("The new graphic named " & groupObj.NewGraphicFilename & ".xml did not get created as it should have been.")
                End If
            Next
            'Go back and remove groups that are no longer needed
            'TODO - THIS DOESN'T WORK BUT NEEDS TO REPLACE <MODIFY_01> ABOVE
            For Each aa In DiscardGroupsList
                'Load the xDoc for this graphic
                Dim xmlpath = Path.Combine(xmlFolderImport, aa.Graphic & ".xml")
                Dim xmlDocToParse As XDocument = XDocument.Load(xmlpath)
                'Remove the group
                Dim RemoveThisOne = xmlDocToParse.Root.Element("GraphicElements").Elements("Group").ToList.Where(Function(x) x.Attribute("Name").Value = aa.Group).FirstOrDefault
                Try
                    RemoveThisOne.Remove()
                Catch ex As Exception 'TODO - When a group is renamed above, it is still in the list.  Need to fix
                End Try
                xmlDocToParse.Save(xmlpath)
                statusResults = statusResults & vbCrLf & "Removed " & aa.Group & " from " & aa.Graphic
            Next
            'Now go back and delete the originals as required
            For Each x In DeleteXmlList
                Dim xmlpath = Path.Combine(xmlFolderImport, x & ".xml")
                IO.File.Delete(xmlpath) 'This requires some extra work to not delete ones where the new path is same as the old path
                statusResults = statusResults & vbCrLf & "Deleted " & x
            Next
        Catch ex As Exception
            Return ex.ToString
        End Try

        'Leaving this here for reference

        'Get the group names
        'Dim GraphicNames = aaXML.aaGroups.Select(Function(x) x.GraphicFilename).ToList.Distinct.ToList
        'For Each name In GraphicNames
        '    Dim filepath As String = Path.Combine(xmlFolderImport, name & ".xml")
        '    If File.Exists(filepath) Then
        '        Dim GroupsToUpdate = aaXML.aaGroups.Where(Function(x) x.GraphicFilename = name).Where(Function(x) x.NewGroupName <> "").ToList
        '        Dim xmlDocToParse As XDocument = XDocument.Load(filepath)
        '        For Each grp In GroupsToUpdate
        '            Dim UpdateThisOne = xmlDocToParse.Root.Element("GraphicElements").Elements("Group").ToList.Where(Function(x) x.Attribute("Name").Value = grp.GroupName).FirstOrDefault
        '            If Not IsNothing(UpdateThisOne) Then
        '                UpdateThisOne.Attribute("Name").SetValue(grp.NewGroupName)
        '                UpdateThisOne.Attribute("Left").SetValue(grp.Xpos)
        '                UpdateThisOne.Attribute("Top").SetValue(grp.Ypos)
        '            End If
        '        Next
        '        xmlDocToParse.Save(filepath)

        '    End If
        'Next
        Return statusResults
    End Function


    ''' <summary>
    ''' Fills the aaGroups list from csv
    ''' </summary>
    ''' <param name="filepath"></param>
    ''' <returns></returns>
    Public Function GetGroupsFromCSV(ByVal filepath As String) As String
        Dim statusResults As String = "Reading CSV...."
        'Make sure our list is clear so we don't get duplicates or reuse old groups.
        aaXML.aaGroups.Clear()
        Try
            'Make sure file exists
            If File.Exists(filepath) Then
                'Read csv to list of string
                Dim CSVList = File.ReadAllLines(filepath).ToList
                If CSVList.Count > 2 Then
                    For i As Integer = 1 To CSVList.Count - 1
                        'Parse each line and fill in aaGroup object valeus
                        Dim newObj As New aaGroupImport
                        Dim oneLine = CSVList(i).Split(",")
                        newObj.GraphicFilename = oneLine(0)
                        newObj.NewGraphicFilename = oneLine(1)
                        newObj.GroupName = oneLine(2)
                        newObj.NewGroupName = oneLine(3)
                        newObj.Xpos = oneLine(4)
                        newObj.Ypos = oneLine(5)
                        newObj.GroupIsEmpty = oneLine(6)
                        newObj.GroupHeader = oneLine(7)
                        newObj.GroupHeaderValue = oneLine(8)
                        aaXML.aaGroups.Add(newObj)
                        statusResults = statusResults & vbCrLf & newObj.GraphicFilename & " | " & newObj.NewGroupName & " added to aaGroups"
                    Next
                End If
            End If
        Catch ex As Exception
            Return "ERROR> " & ex.ToString
        End Try

        Return statusResults
    End Function
#End Region


    ''' <summary>
    ''' Quickly update some attributes that Michael Smith specified in email sent to Caden on 2019-07-16 at 9:09AM
    ''' </summary>
    ''' <param name="fileStrg"></param>
    ''' <returns></returns>
    Public Function QuickAttributeUpdate(ByVal fileStrg As String) As String
        'Get the graphic name
        Dim graphicName As String = Path.GetFileNameWithoutExtension(fileStrg)
        StatusResults = graphicName & ":"

        Try
            Dim xDoc As XDocument = XDocument.Load(fileStrg)
            'Get only the graphic elemetns
            Dim xGE As XElement = xDoc.Root.Element("GraphicElements")
            'Now grab all the embedded symboles
            Dim xSymbols As List(Of XElement) = xGE.Descendants("EmbeddedSymbol").ToList

            'Iterate through the symbols
            For Each sym In xSymbols
                Dim ObjName As String = ""
                'Return any elements with the following conditions. 1. The attribute("Name") exists, 2. The attribute("Name").Value = "ObjName"
                Dim objNameElements = sym.Descendants.Where(Function(x) Not IsNothing(x.Attribute("Name"))).Where(Function(x) x.Attribute("Name").Value = "ObjName").ToList
                'Iterate through the the objNameElements if any were found
                For Each obj In objNameElements
                    'Get objname from the Expression element
                    ObjName = obj.Element("Expression").Attribute("Text").Value
                    'Get the EmbeddedSymbolReference attribute
                    Dim EmbeddedAttribute = sym.Attribute("EmbeddedSymbolReference")
                    'Check if it matches our startswith.
                    If EmbeddedAttribute.Value.StartsWith("Symbol:#") Then
                        'Get the new value. Makes this easier to read and follow when placed outside of the setvalue
                        Dim NewStrg = EmbeddedAttribute.Value.Replace("Symbol:#", " Symbol:" & ObjName & ".#")
                        'Update to the new reference
                        EmbeddedAttribute.SetValue(NewStrg)
                        'Update this attribute from 1 to 2
                        sym.Attribute("SymbolLocation").SetValue(2)
                        'Update the status text.
                        StatusResults = StatusResults & vbCrLf & vbTab & ObjName & " attributes updated."
                    End If
                Next
            Next

            'Update the file location to the import directory
            Dim SaveAsName = Path.Combine(xmlFolderImport, Path.GetFileName(fileStrg))
            'Save the xmlfile
            xDoc.Save(SaveAsName)
        Catch ex As Exception
            StatusResults = ex.Message
            MessageBox.Show(ex.ToString)
        End Try
        Return StatusResults
    End Function

End Class
