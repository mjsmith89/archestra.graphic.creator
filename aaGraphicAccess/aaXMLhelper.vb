﻿Imports System
Imports System.Text
Imports ArchestrA.GRAccess
Imports ArchestrA.Visualization.GraphicAccess
Imports System.Xml.Linq
Imports System.Linq
Imports System.Collections.Generic
Imports System.IO
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports System.Reflection


Public Class aaXMLhelper

    Public aaObjects As New List(Of aaObject)
    Public aaGroups As New List(Of aaGroupImport)
    Public AllaaCustomProperites As New List(Of String)
    Public AllaaOptions As New List(Of String)
    Public AllaaSubStrings As New List(Of String)
    'TODO - This is duplicated.  Need to clean ALL OF THIS up eventually to stop passing so many variables over and over, and just use global variables.  This will be required for the easy multi-threading.
    Public aaTemplate As String = ""
    'TODO - This entire class also needs to be changed back to its intent as a true "helper".  It currently just has a mix and match of subs with the Archestra2XML class.

    'Make a list of all animations that would never contain tags
    Public Sub New()
        Call FillAnimList()
    End Sub

    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   Get all animations from a SINGLE symbol group
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Public Function GetAnimationsFromOneItem(aaGraphicname As String, xSourceElem As XElement, xAttrName As String, xPrefix As String) As List(Of String)
        Dim ErrorItem As String = ""
        Dim ReturnStatusList As New List(Of String)

        Try
            'First, get all animation elements
            Dim xAnimations As New List(Of XElement)
            xAnimations = xSourceElem.Descendants("Animations").ToList
            'Add text elements
            'MICHAEL - TODO - This is grabbing text items that have no animations
            xAnimations.AddRange(xSourceElem.Descendants.Where(Function(x) x.Name = "Text").ToList)

            If xAnimations.Count > 0 Then
                'xAnimations.AddRange(xSourceElem.Descendants.Where(Function(x) Not IsNothing(x.Element("Animations"))).ToList)
                'Instantiate 
                Dim aaObject As New aaObject
                aaObject.xElem = xSourceElem
                Dim SourceName = xSourceElem.Attribute("Name").Value.ToString
                aaObject.SourceName = SourceName
                'Remove the # (MJS - REMOVED AND NOW CHECKED INSIDE Equate_ImportedObjectType_to_CorrentObjectType()
                'aaObject.SourceName = SourceName.Substring(1)
                If Not IsNothing(aaObject.SourceName) Then
                    If aaObject.SourceName.Contains("_") And aaObject.SourceName.StartsWith("#") Then
                        Dim parts() = aaObject.SourceName.Split("_")
                        aaObject.ObjectTypeImported = parts(0)
                        aaObject.ObjectName = parts(1)
                        'If more than 3 parts, then something is wrong since it is only suppossed to be #ObjType_ObjName
                        If parts.Count > 2 Then
                            'THIS IS INTENDED TO REPAIR when the user created more parts than they should have
                            Select Case parts(2)
                                Case "0", "1", "2", "3", "00", "01"
                                    'This is where Alex thought he was suppossed to add a 0 or 1 behind each
                                    '   > Just proceed
                                Case Else
                                    'TODO - We need more error checking here for more complex objects like pumps, etc
                                    'Combine the extra parts into one name
                                    For i = 2 To parts.Count - 1
                                        aaObject.ObjectName = aaObject.ObjectName & "_" & parts(i)
                                    Next
                            End Select
                            For i = 2 To parts.Count - 1
                                If i = 2 Then
                                    'Show the 3rd part in Overflow column
                                    aaObject.SourceName_Overflow = parts(i)
                                Else
                                    'Error if more than 3 parts
                                    aaObject.SourceName_Overflow = "<ERROR: " & parts.Count & " parts>"
                                End If
                            Next
                        End If
                    Else
                        'Items does not start with # or contain underscores
                        aaObject.ObjectTypeImported = "<Unknown>"
                        aaObject.ObjectName = SourceName
                    End If
                    'Equate any unknown object types (from human error in the aaGraphic #Group naming) to an object type that exists in our standard object list
                    aaObject.ObjectType = Equate_ImportedObjectType_to_CorrentObjectType(aaObject.ObjectTypeImported)
                    'TODO - Ask Caden why one is CInt and the other is Int
                    aaObject.Xpos = CInt(xSourceElem.Attribute("Left").Value)
                    aaObject.Ypos = Int(xSourceElem.Attribute("Top").Value)
                    aaObject.WidthIn = CInt(xSourceElem.Attribute("Width").Value)
                    'Automatically adjust to nearest grid location for T4 displays
                    If aaTemplate = "T4_Template_For_Importer" Then
                        'Remove the 1's digit and replace with a 0
                        aaObject.Xpos = aaObject.Xpos.Substring(0, aaObject.Xpos.Length - 1) & "0"
                        If aaObject.Xpos < 40 Then aaObject.Xpos = 40
                        aaObject.Ypos = aaObject.Ypos.Substring(0, aaObject.Ypos.Length - 1) & "0"
                    End If
                    'Add object to the return list for display
                    ReturnStatusList.Add(aaObject.ObjectName & " | " & aaObject.ObjectTypeImported & " | " & aaObject.Xpos & " | " & aaObject.Ypos)

                    'Loop through all the xElements that it found
                    For Each item In xAnimations
                        For Each elem In item.Descendants()
                            'Loop through all decendant
                            For Each e In elem.Descendants
                                ErrorItem = item.Parent.Name.ToString
                                'Instantiate a new object
                                Dim anim As New aaObject.aaAnim

                                'Check if the Attirbute "Name" exists in the parent element before reading it.
                                If Not IsNothing(item.Parent.Attribute("Name")) Then
                                    If item.Parent.Attribute("Name").Value = SourceName Then
                                        anim.Parent = item.Parent.Attribute("Name").Value
                                    Else
                                        anim.Parent = SourceName & "." & item.Parent.Attribute("Name").Value
                                    End If
                                Else
                                    'If no name attribute in parent, then the parent is GraphicElements 
                                    anim.Parent = item.Parent.Parent.Attribute("Name").Value
                                End If

                                ErrorItem = anim.Parent & " | " & elem.Name.ToString & " | " & e.Name.ToString

                                'Add the parent element names for any attributes all the way back up to 'Animations'
                                Dim test1 = item.Name.ToString
                                Dim test2 = elem.Name.ToString
                                Dim test3 = e.Name.ToString
                                If item.Name.ToString = "Text" Then
                                    'Dim stophere1 = 1
                                    'Filling in animation values even though these aren't animations. Doing this only to fill in the blank outputs.
                                    anim.Tags.Tag1.TagFound = item.Attribute("Name").Value.Trim
                                    anim.Value = item.Element("TextStyleData").Attribute("Text").Value
                                    anim.AnimType = "Text.TextStyleData.Text"
                                    'We are looping through all the text attributes but don't really care about them. This only adds the animation item if the tag(Name of text element) doesn't exist yet
                                    If aaObject.Animations.Where(Function(x) x.Tags.Tag1.TagFound = anim.Tags.Tag1.TagFound).Count = 0 Then
                                        aaObject.Animations.Add(anim)
                                    End If
                                    'Read next element in list
                                    Continue For
                                ElseIf e.Name.ToString = "Scripts" Then
                                    'Get the animation type
                                    anim.AnimType = e.Parent.Name.ToString & "." & e.Name.ToString & "." & e.Elements.FirstOrDefault.Name.ToString
                                    'Fill in the animation value from the first script element
                                    anim.Value = e.Elements.FirstOrDefault.Value
                                    'Remove any carriage returns or line feeds
                                    anim.Value = anim.Value.Replace(vbCr, "").Replace(vbLf, "")
                                    'Add to the list unless it Is a duplicate (This Is because it finds the element nested.  There Is probably a better way, but this works.)
                                    If Not aaObject.Animations.Contains(anim) Then aaObject.Animations.Add(anim)
                                    Continue For
                                ElseIf e.Parent.Name = "Animations" Then
                                    anim.AnimType = e.Name.ToString
                                    Dim stopme = 1
                                ElseIf e.Parent.Parent.Name = "Animations" Then
                                    anim.AnimType = e.Parent.Name.ToString & "." & e.Name.ToString
                                    Dim stopme = 1
                                ElseIf e.Parent.Parent.Parent.Name = "Animations" Then
                                    anim.AnimType = e.Parent.Parent.Name.ToString & "." & e.Parent.Name.ToString & "." & e.Name.ToString
                                Else
                                    anim.AnimType = e.Parent.Parent.Parent.Name.ToString & "." & e.Parent.Parent.Name.ToString & "." & e.Parent.Name.ToString & "." & e.Name.ToString
                                End If
                                'Add the first attributes value (TODO MICHAEL - This isn't perfect since some have more than one, but should be OK for what we are doing in this project
                                Try
                                    Dim FirstAttrName As String
                                    Dim FirstAttrValue As String
                                    If Not IsNothing(e.FirstAttribute) Then
                                        FirstAttrName = e.FirstAttribute.Name.ToString()
                                        FirstAttrValue = e.FirstAttribute.Value.ToString()

                                        'Ignore the Enable attributes
                                        If FirstAttrName = "Enabled" Then
                                            'Ignore
                                        Else
                                            anim.Value = FirstAttrValue
                                            'Add to the list unless it is a duplicate (This is because it finds the element nested.  There is probably a better way, but this works.)
                                            If Not aaObject.Animations.Contains(anim) Then
                                                aaObject.Animations.Add(anim)
                                            End If
                                        End If
                                    End If

                                Catch ex As Exception
                                    Dim reportthis = ErrorItem
                                End Try
                            Next
                        Next
                    Next
                    aaObject.GraphicFilename = Path.GetFileName(aaGraphicname)
                    aaObject.TextElements = xSourceElem.Descendants("Text").Select(Function(x) x.Element("TextStyleData").Attribute("Text").Value).ToList
                    'Add to the objects list
                    aaObjects.Add(aaObject)

                    For Each item In aaObject.Animations
                        'Return for display
                        ReturnStatusList.Add("⮡  " & item.Parent & " | " & item.AnimType & " | " & item.Value)
                    Next
                Else
                    'Do nothing
                End If

            Else
                'Do nothing
            End If

            Return ReturnStatusList

        Catch ex As Exception
            MessageBox.Show(ErrorItem & vbCrLf & ex.ToString)
            Return New List(Of String)({ErrorItem, "", "", ExResults(ex)})
        End Try
    End Function

    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   Get all custom properties and options from a SINGLE embedded symbol
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Public Function GetCustomPropertiesAndOptionsFromXML(aaSym As XElement, name As String, graphicName As String) As List(Of String)
        Dim ErrorItem As String = ""
        Dim ReturnStatusList As New List(Of String)

        Try
            'Get basics
            Dim aaObject As New aaObject
            'Make sure we are working with clean slates here
            aaObject.CustProps.Clear()
            aaObject.Options.Clear()
            aaObject.SubstituteStrings.Clear()
            aaObject.GraphicFilename = Path.GetFileNameWithoutExtension(graphicName)
            aaObject.xElem = aaSym
            aaObject.ObjectName = aaSym.Attribute("Name").Value.ToString
            If aaObject.ObjectName = "" Then aaObject.ObjectName = name
            aaObject.Xpos = aaSym.Attribute("Left").Value.ToString
            aaObject.Ypos = aaSym.Attribute("Top").Value.ToString
            'Find out if object is in a group
            If aaSym.Parent.Name.ToString = "Group" Then
                'If in group, is this a base layer group?
                If aaSym.Parent.Parent.Parent.Name = "aa:Symbol" Then
                    aaObject.InGroup = True
                    aaObject.GroupName = aaSym.Parent.Attribute("Name").ToString
                End If

            Else
                'Do nothing
            End If
            Dim GraphicElements As XElement
            If aaSym.Parent.Name.ToString = "GraphicElements" And aaSym.Parent.Parent.Name.LocalName.ToString = "Symbol" Then
                GraphicElements = aaSym.Parent
            Else
                GraphicElements = aaSym.Parent.Parent.Parent
            End If
            Dim elemHeader = GraphicElements.Descendants("TextBox").Where(Function(x) x.Attribute("Name").Value = "Header").FirstOrDefault
            Dim elemNavPage = GraphicElements.Descendants("TextBox").Where(Function(x) x.Attribute("Name").Value = "NavPage").FirstOrDefault
            If Not IsNothing(elemHeader) Then
                aaObject.T4PageName = elemHeader.Element("TextStyleData").Attribute("Text").Value
            End If

            If Not IsNothing(elemNavPage) Then
                aaObject.T4PageNumber = elemNavPage.Element("TextStyleData").Attribute("Text").Value
            End If


            Try
                aaObject.SymbolReference = aaSym.Attribute("EmbeddedSymbolReference").Value.ToString
                'Equate any unknown object types (from human error in the aaGraphic #Group naming) to an object type that exists in our standard object list
                aaObject.ObjectType = Equate_ImportedObjectType_to_CorrentObjectType(aaObject.SymbolReference)
            Catch ex As Exception
                'The root element will not have the value above
                aaObject.ObjectType = "ROOT"
            End Try

            'Find all custom properties inside each embedded symbol
            Dim xCustProps = aaSym.Descendants.Where(Function(x) x.Name = "CustomProperty").ToList
            'If IsNothing(xCustProps) Then Return New List(Of String)({"No objects found"})
            For Each CP In xCustProps
                Dim aaCP As New aaObject.aaCustProp
                aaCP.Name = CP.Attribute("Name").Value.ToString()
                ErrorItem = aaObject.ObjectName & "." & aaCP.Name
                aaCP.Overridden = CP.Attribute("Overridden").Value.ToString()
                aaCP.DataType = CP.Attribute("DataType").Value.ToString()
                'Expressition changes with type, so get it dynamically
                aaCP.ExpressionType = CP.Element("Expression").FirstAttribute.Name.ToString
                aaCP.Value = CP.Element("Expression").Attribute(aaCP.ExpressionType).Value.ToString()
                aaObject.CustProps.Add(aaCP)
            Next
            'Find all options inside each embedded symbol
            Dim xOptions = aaSym.Descendants.Where(Function(x) x.Name = "WizardOption").ToList
            'If IsNothing(xOptions) Then Return New List(Of String)({"No objects found"})
            For Each O In xOptions
                Dim aaO As New aaObject.aaOption
                aaO.Name = O.Attribute("Name").Value.ToString()
                ErrorItem = aaObject.ObjectName & "." & aaO.Name
                aaO.Overridden = O.Attribute("Overridden").Value.ToString()
                aaO.Value = O.Attribute("Value").Value.ToString()
                aaObject.Options.Add(aaO)
            Next

            'Check for substrings
            Dim xSubStrings = aaSym.Descendants("SubstituteStrings").Descendants("String").ToList
            'If xSubStrings.Count < 1 Then Return New List(Of String)({"No substrings found"})
            'Fill in our substrings list if we have some. Otherwise our list will remain blank.
            aaObject.SubstituteStrings.AddRange(xSubStrings.Select(Function(x) New aaObject.aaString With {.OldText = x.Attribute("Old"), .NewText = x.Attribute("New"), .ElementID = x.Attribute("ElementID"), .Order = xSubStrings.IndexOf(x)}).ToList)



            ReturnStatusList.Add(aaObject.ObjectName & ",Type," & aaObject.ObjectType)
            ReturnStatusList.Add(aaObject.ObjectName &
                               ",Position,Left," & aaObject.Xpos & ",False,Integer")
            ReturnStatusList.Add(aaObject.ObjectName &
                               ",Position,Top," & aaObject.Ypos & ",False,Integer")

            'Convert to string for status/csv
            For Each item In aaObject.CustProps
                ReturnStatusList.Add("Custom Property: " & item.Name & " added to " & aaObject.ObjectName)
                If Not AllaaCustomProperites.Contains(item.Name) Then
                    AllaaCustomProperites.Add(item.Name)
                End If
            Next
            For Each item In aaObject.Options
                ReturnStatusList.Add("Option: " & item.Name & " added to " & aaObject.ObjectName)
                If Not AllaaOptions.Contains(item.Name) Then
                    AllaaOptions.Add(item.Name)
                End If
            Next
            For Each item In aaObject.SubstituteStrings
                ReturnStatusList.Add("SubString: " & item.NewText & " added to " & aaObject.ObjectName)
                If Not AllaaSubStrings.Contains(item.ElementID) Then
                    AllaaSubStrings.Add(item.ElementID)
                End If
            Next


            'Add to the objects list
            aaObjects.Add(aaObject)

            Return ReturnStatusList
        Catch ex As Exception
            Return New List(Of String)({ErrorItem, "", "", ExResults(ex)})
        End Try
    End Function



    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   Get all custom properties and options from a SINGLE embedded symbol
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Public Function GetSymbolCustPropsAndOptions(S As XElement, Name As String) As List(Of String)
        Dim ErrorItem As String = ""
        Dim ReturnStatusList As New List(Of String)

        Try
            'Get basics
            Dim aaObject As New aaObject
            aaObject.xElem = S
            aaObject.ObjectName = S.Attribute("Name").Value.ToString
            If aaObject.ObjectName = "" Then aaObject.ObjectName = Name
            aaObject.Xpos = S.Attribute("Left").Value.ToString
            aaObject.Ypos = S.Attribute("Top").Value.ToString
            Try
                aaObject.ObjectTypeImported = S.Attribute("EmbeddedSymbolReference").Value.ToString
                'Equate any unknown object types (from human error in the aaGraphic #Group naming) to an object type that exists in our standard object list
                aaObject.ObjectType = Equate_ImportedObjectType_to_CorrentObjectType(aaObject.ObjectTypeImported)
            Catch ex As Exception
                'The root element will not have the value above
                aaObject.ObjectType = "ROOT"
            End Try

            'Find all custom properties inside each embedded symbol
            Dim xCustProps = S.Descendants.Where(Function(x) x.Name = "CustomProperty").ToList
            If IsNothing(xCustProps) Then Return New List(Of String)({"No objects found"})
            For Each CP In xCustProps
                Dim aaCP As New aaObject.aaCustProp
                aaCP.Name = CP.Attribute("Name").Value.ToString()
                ErrorItem = aaObject.ObjectName & "." & aaCP.Name
                aaCP.Overridden = CP.Attribute("Overridden").Value.ToString()
                aaCP.DataType = CP.Attribute("DataType").Value.ToString()
                'Expressition changes with type, so get it dynamically
                aaCP.ExpressionType = CP.Element("Expression").FirstAttribute.Name.ToString
                aaCP.Value = CP.Element("Expression").Attribute(aaCP.ExpressionType).Value.ToString()
                aaObject.CustProps.Add(aaCP)
            Next
            'Find all options inside each embedded symbol
            Dim xOptions = S.Descendants.Where(Function(x) x.Name = "WizardOption").ToList
            If IsNothing(xOptions) Then Return New List(Of String)({"No objects found"})
            For Each O In xOptions
                Dim aaO As New aaObject.aaOption
                aaO.Name = O.Attribute("Name").Value.ToString()
                ErrorItem = aaObject.ObjectName & "." & aaO.Name
                aaO.Overridden = O.Attribute("Overridden").Value.ToString()
                aaO.Value = O.Attribute("Value").Value.ToString()
                aaObject.Options.Add(aaO)
            Next

            ReturnStatusList.Add(aaObject.ObjectName & ",Type," & aaObject.ObjectType)
            ReturnStatusList.Add(aaObject.ObjectName &
                               ",Position,Left," & aaObject.Xpos & ",False,Integer")
            ReturnStatusList.Add(aaObject.ObjectName &
                               ",Position,Top," & aaObject.Ypos & ",False,Integer")

            'Convert to string for status/csv
            For Each item In aaObject.CustProps
                ReturnStatusList.Add(aaObject.ObjectName &
                               ",CustProp," & item.Name & "," & item.Value & "," & item.Overridden & "," & item.DataType)
            Next
            For Each item In aaObject.Options
                ReturnStatusList.Add(aaObject.ObjectName &
                               ",Option," & item.Name & "," & item.Value & "," & item.Overridden)
            Next

            'Add to the objects list
            aaObjects.Add(aaObject)

            Return ReturnStatusList
        Catch ex As Exception
            Return New List(Of String)({ErrorItem, "", "", ExResults(ex)})
        End Try
    End Function

    ''' <summary>
    ''' Updates the Value and Overridden attributes of the option if the value has changed.
    ''' ed WizardOption xml element
    ''' </summary>
    ''' <param name="aElem"></param>
    ''' <param name="aName"></param>
    ''' <param name="Value"></param>
    ''' <returns></returns>
    Private Function UpdateWizardOption(ByRef aElem As XElement, aName As String, Value As String) As String
        Dim valStrg As String = Value
        Try
            Dim WizardList = aElem.Descendants.Where(Function(x) x.Attribute("Name") = aName).ToList
            Select Case Value.ToLowerInvariant
                Case "true"
                    valStrg = "True"
                Case "false"
                    valStrg = "False"
                Case Else
                    'Do nothing
            End Select
            For Each wiz In WizardList
                'Only update if the value has changed.
                If Not wiz.Attribute("Value").Value.ToLowerInvariant = Value.ToLowerInvariant Then
                    wiz.Attribute("Value").SetValue(valStrg)
                    wiz.Attribute("Overridden").SetValue("True")
                End If
            Next
            Return "Updated " & aName & " to " & Value
        Catch ex As Exception
            Return ExResults(ex)
        End Try
    End Function

    ''' <summary>
    ''' Updates attributes/elements within the CustomProperties. This will need to be refined later on to include all custom property options
    ''' </summary>
    ''' <param name="aElem"></param>
    ''' <param name="Value"></param>
    ''' <param name="updatedesc"></param>
    ''' <param name="desc"></param>
    ''' <returns></returns>
    Private Function UpdateCustomProperty(ByRef aElem As XElement, Value As String, Optional updatedesc As Boolean = False, Optional desc As String = "No description included") As String
        Dim Override As Boolean = False
        Try
            If Not IsNothing(aElem) Then
                'Check to see if the text attribute exists, if it does then update it. If not update the expression attribute.
                Select Case True
                    Case Not IsNothing(aElem.Element("Expression").Attribute("Text"))
                        'Only update if the value has changed.
                        If Not aElem.Element("Expression").Attribute("Text").Value = Value Then
                            aElem.Element("Expression").Attribute("Text").SetValue(Value)
                            Override = True
                        End If
                    Case Not IsNothing(aElem.Element("Expression").Attribute("Expression"))
                        'Only update if the value has changed.
                        If Not aElem.Element("Expression").Attribute("Expression").Value = Value Then
                            aElem.Element("Expression").Attribute("Expression").SetValue(Value)
                            Override = True
                        End If
                    Case Not IsNothing(aElem.Element("Expression").Attribute("Reference"))
                        'Only update if the value has changed.
                        If Not aElem.Element("Expression").Attribute("Reference").Value = Value Then
                            aElem.Element("Expression").Attribute("Reference").SetValue(Value)
                            Override = True
                        End If
                    Case Else
                        MessageBox.Show(aElem.ToString)

                End Select
                'Update the decription string if updatedesc is true
                If updatedesc Then aElem.Element("Description").Value = desc
                'Only update the override if the value actually changed.
                If Override Then aElem.Attribute("Overridden").SetValue("True")
                Return "Updated CustProp " & Value
            Else
                Return "Element doesn't exist"
            End If
        Catch ex As Exception
            Return "Error: " & ExResults(ex)
        End Try
    End Function




    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   Duplicate a symbol template so that you can modify the xelements from the csv import
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Public Function DuplicateSymbol(ByRef xBaseElem As XElement, ByVal elementName As String, ByRef LastUsedID As String, ByRef O As aaObject, Optional ByVal UseSymbolReference As Boolean = False, Optional ByVal FTViewObject As Boolean = False, Optional ByVal Testing As Boolean = False) As XElement
        Dim ErrorItem As String = ""
        Dim ReturnStatusList As New List(Of String)


        'EXAMPLE xAttrName = "Name"
        'EXAMPLE xElemName = "Tank1"
        Try

            'Create new xElement based on the element passed into function
            'Select Case elementName.ToLowerInvariant
            '    Case 
            '    Case Else

            'End Select
            Dim oldName As String = xBaseElem.Attribute("Name").Value
            oldName = oldName.Replace("#", "")
            Dim newElem As XElement = New XElement(xBaseElem)
            'Adjust the anchor position by comparing the new and old left and top positions
            Dim LeftDifference = CInt(O.Xpos) - CInt(xBaseElem.Attribute("Left").Value)
            Dim TopDifference = CInt(O.Ypos) - CInt(xBaseElem.Attribute("Top").Value)


            'Update the necessary attributes
            newElem.Attribute("Name").SetValue(elementName.Replace("#", ""))
            newElem.Attribute("Left").SetValue(O.Xpos)
            newElem.Attribute("LocalLeft").SetValue(O.Xpos)
            newElem.Attribute("Top").SetValue(O.Ypos)
            newElem.Attribute("LocalTop").SetValue(O.Ypos)
            'Set the anchor point, we shouldn't set this to the center of the object so just offset it from it's postion in the template
            If Not IsNothing(newElem.Attribute("AnchorPointX")) Then
                Dim AnchorX = CInt(xBaseElem.Attribute("AnchorPointX").Value) + LeftDifference
                newElem.Attribute("AnchorPointX").SetValue(AnchorX.ToString("F0"))
            End If
            If Not IsNothing(newElem.Attribute("AnchorPointY")) Then
                Dim AnchorY = CInt(xBaseElem.Attribute("AnchorPointY").Value) + TopDifference
                newElem.Attribute("AnchorPointY").SetValue(AnchorY.ToString("F0"))
            End If


            'OriginX and OriginY are the relative origins of the graphic objects. Change these moves the "center" point of the object away from it's true center
            If Not IsNothing(newElem.Attribute("OriginX")) Then newElem.Attribute("OriginX").SetValue("0")
            If Not IsNothing(newElem.Attribute("OriginY")) Then newElem.Attribute("OriginY").SetValue("0")
            If O.Width <> "" Then If Not IsNothing(newElem.Attribute("Width")) Then newElem.Attribute("Width").SetValue(O.Width)
            If O.Height <> "" Then If Not IsNothing(newElem.Attribute("Height")) Then newElem.Attribute("Height").SetValue(O.Height)

            'If this is a text box, we have to update both the BaseRectPoints and BaseRectLocalPoints
            If newElem.Name.ToString = "TextBox" Then
                Dim OriginalPosValues As New List(Of Integer)(New Integer() {xBaseElem.Attribute("Left").Value, xBaseElem.Attribute("Top").Value, xBaseElem.Attribute("LocalLeft").Value, xBaseElem.Attribute("LocalTop").Value})
                Dim Differences As New List(Of Integer)(New Integer() {O.Xpos - OriginalPosValues(0), O.Ypos - OriginalPosValues(1)})
                newElem.Attribute("LocalLeft").SetValue(OriginalPosValues(2) + Differences(0))
                newElem.Attribute("LocalTop").SetValue(OriginalPosValues(3) + Differences(1))
                For Each pos In newElem.Element("BaseRectPoints").Elements
                    Dim oldX = pos.Attribute("X").Value
                    Dim oldY = pos.Attribute("Y").Value
                    pos.Attribute("X").SetValue(oldX + Differences(0))
                    pos.Attribute("Y").SetValue(oldX + Differences(1))
                Next

                For Each pos In newElem.Element("BaseRectLocalPoints").Elements
                    Dim oldX = pos.Attribute("X").Value
                    Dim oldY = pos.Attribute("Y").Value
                    pos.Attribute("X").SetValue(oldX + Differences(0))
                    pos.Attribute("Y").SetValue(oldY + Differences(1))
                Next
            End If

            'UseSymbolReference=TRUE >  We are updating graphics from the spreadsheet 
            'UseSymbolReference=FALSE > We are converting an InTouch window to a new graphic based on the animations
            'TODO - Ask Caden why he decided to call this variable UseSymbolReference
            If UseSymbolReference Then

                'Update all custom properties
                Dim CustomProps = newElem.Descendants.Where(Function(x) x.Name = "CustomProperty")
                For Each cust In CustomProps
                    For Each prop In O.CustProps
                        'Check for a blank custom property value. If blank then we can ignore
                        If prop.Value <> "" Then
                            'Check if custom propery even exists. If it does, update it
                            If prop.Name = cust.Attribute("Name").Value Then
                                Dim stat = UpdateCustomProperty(cust, prop.Value)
                                ReturnStatusList.Add(stat)
                            Else
                                'Do nothing
                            End If
                        Else
                        End If
                    Next
                Next

                'Update all wizard options
                Dim WizardOptions = newElem.Descendants.Where(Function(x) x.Name = "OverriddenWizardOptions").FirstOrDefault
                For Each opt In O.Options
                    'Check for a blank custom property value. If blank then we can ignore
                    If opt.Value <> "" Then

                        Dim statusRes = UpdateWizardOption(WizardOptions, opt.Name, opt.Value)

                        ReturnStatusList.Add(statusRes)
                    Else
                        'Do nothing
                    End If
                Next

                Dim SubStrings As List(Of XElement) = newElem.Descendants("SubstituteStrings").Elements("String").ToList

                'Dim NewElementID As String = newElem.Attribute("Name").Value.Replace(,)
                For Each strg In SubStrings
                    Dim NewElementID = strg.Attribute("ElementID").Value.Replace(O.SymbolReference.Replace("Symbol:", ""), newElem.Attribute("Name").Value)
                    Dim updatePageNum = NewElementID.Split(".")(0)
                    'Add the group name to the elementID
                    If O.T4PageName <> "" And O.T4PageNumber <> "" Then NewElementID = NewElementID.Replace(updatePageNum, "Page" & CInt(O.T4PageNumber).ToString("00")) ' = "Page" & CInt(O.T4PageNumber).ToString("00") & "." & NewElementID
                    'Doeing this check prevents a index out of range error
                    Dim index = SubStrings.IndexOf(strg)
                    If O.SubstituteStrings.Count > index Then
                        'Get the new string value
                        Dim Value = O.SubstituteStrings(index).NewText
                        'Check for pipes, if it has a pipe then remove it.
                        If Value.Contains("|") Then Value = Trim(Value.Split("|").LastOrDefault)
                        'Update the new value, but don't put in blank strings
                        If Value <> "" Then
                            strg.Attribute("New").SetValue(Value)
                            'Set the element ID
                            strg.Attribute("ElementID").SetValue(NewElementID)
                        Else
                            'Do nothing
                        End If
                    End If
                Next

                'TODO - Need to make the substitute strings dynamic based on the options/choices that are set.
                'TEMPORARY - Just ignore any past the description for the problem children below
                Select Case oldName
                    Case "pushbutton", "toggle_switch"
                        If SubStrings.Count > 1 Then
                            For i As Integer = 1 To SubStrings.Count - 1
                                newElem.Descendants("SubstituteStrings").Elements("String").LastOrDefault.Remove()
                                'CADEN TODO - Add a text box with the deleted text
                            Next
                        End If
                End Select


            ElseIf FTViewObject Then
                newElem.Attribute("Name").SetValue(O.ObjectName)
                'Parse xml based on object type
                Select Case O.ObjectType.ToLowerInvariant
                    Case "rectangle", "polygon"
                        'Do nothing for now
                    Case "text"
                        newElem.Element("TextStyleData").Attribute("Text").SetValue(O.TextElements(0))
                        If O.Alignment <> "" Then newElem.Element("TextStyleData").Attribute("Alignment").SetValue(O.Alignment)
                        If O.FontSize <> "" Then newElem.Element("TextStyleData").Element("Font").Attribute("Size").SetValue(O.FontSize)
                    Case "taglabel"
                        newElem.Element("TextStyleData").Attribute("Text").SetValue(O.TextElements(0))
                        If O.Alignment <> "" Then newElem.Element("TextStyleData").Attribute("Alignment").SetValue(O.Alignment)
                        If O.FontSize <> "" Then newElem.Element("TextStyleData").Element("Font").Attribute("Size").SetValue(O.FontSize)
                    Case "stringdisplay", "numericdisplay", "numericinput"
                        newElem.Element("TextStyleData").Attribute("Text").SetValue(O.TextElements(0))
                        If O.Alignment <> "" Then newElem.Element("TextStyleData").Attribute("Alignment").SetValue(O.Alignment)
                        If O.FontSize <> "" Then newElem.Element("TextStyleData").Element("Font").Attribute("Size").SetValue(O.FontSize)
                    Case "button"
                        newElem.Element("TextStyleData").Attribute("Text").SetValue(O.TextElements(0))
                    Case "line"
                        newElem.Element("Start").Attribute("X").SetValue(O.Xpos)
                        newElem.Element("Start").Attribute("Y").SetValue(O.Ypos)
                        If O.Height > O.Width Then
                            newElem.Element("End").Attribute("X").SetValue(O.Xpos)
                            Dim endY = CInt(O.Ypos) + CInt(O.Height)
                            newElem.Element("End").Attribute("Y").SetValue(endY.ToString)
                            'newElem.Element("LineStyleData").Element("LineStyle").Attribute("Weight").SetValue(O.Width)
                        Else
                            newElem.Element("End").Attribute("Y").SetValue(O.Ypos)
                            Dim endX = CInt(O.Xpos) + CInt(O.Width)
                            newElem.Element("End").Attribute("X").SetValue(endX.ToString)
                            'newElem.Element("LineStyleData").Element("LineStyle").Attribute("Weight").SetValue(O.Height)
                        End If


                    Case Else
                End Select

            Else
                'Update any customproperites with ObjName as the element
                'Find the existing CustProp named "ObjName" inside of the template.
                Dim ObjNameElement = newElem.Descendants.Where(Function(x) x.Name = "CustomProperty").Where(Function(x) x.Attribute("Name").Value = "ObjName").FirstOrDefault
                'CALL UpdateCustomProperty to update the CustProp names ObjName with the aaObject ObjName
                Dim stat = UpdateCustomProperty(ObjNameElement, O.ObjectName)
                ReturnStatusList.Add(stat)

                'Update the Description (when applicable)
                Try
                    Dim SubStrings As XElement = newElem.Descendants.Where(Function(x) x.Name = "SubstituteStrings").FirstOrDefault
                    'If Description field is blank, then do nothing
                    If O.Description <> "" Then
                        Dim NewElementID As String = newElem.Attribute("Name").Value & O.Description_ElementID_Postfix
                        SubStrings.Element("String").Attribute("New").SetValue(O.Description)
                        SubStrings.Element("String").Attribute("ElementID").SetValue(NewElementID)
                        Dim statusRes = "Updated " & O.ObjectName & " description to " & O.Description
                        ReturnStatusList.Add(statusRes)
                    Else
                        'Do nothing
                    End If
                Catch ex As Exception
                    Dim statusRes = "ERROR: 'SubstituteStrings' attribute not found in " & O.ObjectName & "."
                    ReturnStatusList.Add(statusRes)
                End Try

                'Update specific wizard options of a select few for testing
                'Get Wizard Options. There is only one of these sections per object
                Dim WizardElement = newElem.Descendants.Where(Function(x) x.Name = "OverriddenWizardOptions").FirstOrDefault
                'Loop through all animations
                For Each anim In O.Animations
                    Select Case anim.Tags.Tag1.p3_Property.ToLowerInvariant
                        Case "FTS"
                            stat = UpdateWizardOption(WizardElement, "HasFTS", "True")
                            ReturnStatusList.Add(stat)
                        Case ""

                        Case Else

                    End Select
                Next
            End If



            'Update the last elmentID
            newElem.Attribute("ElementID").SetValue(LastUsedID)
            If FTViewObject Then
                ' xBaseElem.Parent.Add(newElem)
            Else
                If xBaseElem.Attribute("Name").Value = elementName Or Testing Then
                    'Update the element rather than add a new one if the name matches
                    xBaseElem = newElem
                ElseIf Not Testing Then
                    'Add a new element if we actually need a copy
                    xBaseElem.Parent.Add(newElem)
                Else
                End If
            End If


            Return newElem
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            Return Nothing
        End Try
    End Function



    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   INTERNAL FUNCTIONS (called from functions above)
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

    'NOT USED YET
    Public Function GetRootAttributesAsDictionary(ByRef xmlDoc As XDocument) As String
        Try
            'Get all root attributes as a dictionary
            '******* THIS DOESN'T WORK ************
            'Dim RootAttributes = xmlDoc.Root.Attributes.ToDictionary()
            Return "success"
        Catch ex As Exception
            Return ExResults(ex)
        End Try
    End Function

    'NOT USED YET
    Public Function GetFirstElementByAttrName(GraphicElements As XElement, xAttrName As String, xElemName As String) As XElement
        'EXAMPLE xAttrName = "Name"
        'EXAMPLE xElemName = "Tank1"
        Try
            'The "where" searches ALL the descendant elements for any attribute named "Name".  NOTE: Descendants are elements, not attributes.
            '                                    Visualize->     Where(Function(x) x.Attribute("Name") = "Tank1").FirstOrDefault
            Dim TargetElement As XElement = GraphicElements.Descendants.Where(Function(x) x.Attribute(xAttrName) = xElemName).FirstOrDefault
            'Caden wrote a note to me offering an alternate way, but I lost it somehow
            Return TargetElement
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    'NOT USED YET
    Public Function GetAllCustomProperties(ByVal xTargetElem As XElement) As List(Of CustomProperty)
        Dim ReturnStatusList As New List(Of CustomProperty)
        Try
            For Each elem In xTargetElem.Descendants
                If elem.Name = "CustomProperty" Then
                    ReturnStatusList.Add(FillCustomProperties(elem))
                End If
            Next
        Catch ex As Exception
            Return Nothing
        End Try
        Return ReturnStatusList
    End Function
    Public Function FillCustomProperties(ByVal custprop As XElement) As CustomProperty
        Dim CP As New CustomProperty
        Try
            CP.Name = custprop.Attribute("Name")
            CP.DataType = custprop.Attribute("DataType")
            CP.Locked = custprop.Attribute("Locked")
            CP.Visibility = custprop.Attribute("Visibility")
            CP.Overridden = custprop.Attribute("Overridden")
            If Not IsNothing(custprop.Element("Expression").Attribute("Text")) Then
                CP.Expression.Text = custprop.Element("Expression").Attribute("Text")
            Else
                CP.Expression.Expression = custprop.Element("Expression").Attribute("Expression")
            End If
        Catch ex As Exception
            Return Nothing
        End Try
        Return CP
    End Function

    'Equate any unknown object types (from human error in the aaGraphic #Group naming) to an object type that exists in our standard object list
    Public Function Equate_ImportedObjectType_to_CorrentObjectType(ByVal ObjectTypeImported As String) As String
        Dim CorrectObjectType As String = ""
        'Check for the # Prefix
        If ObjectTypeImported.StartsWith("#") Then
            ObjectTypeImported = ObjectTypeImported.Substring(1)
        Else
            'Bypass the Select Case and report as an animation found in an element that was not modified with a # prefix
            CorrectObjectType = "<# Not Found>"
            Return CorrectObjectType
        End If

        Select Case ObjectTypeImported
            Case "AI", "BarScrn", "EGAI"
                If aaTemplate = "T4_Template_For_Importer" Then
                    CorrectObjectType = ObjectTypes.AI_Sidebar.ToString
                Else
                    CorrectObjectType = ObjectTypes.AI_Basic.ToString
                End If
            Case "AerBasin", "AITank"
                CorrectObjectType = ObjectTypes.AI_Tank.ToString
            Case "Total", "Totalizer"
                CorrectObjectType = ObjectTypes.AI_Sidebar.ToString
            Case "HOA", "HOAAnalog", "HOADigital"
                CorrectObjectType = ObjectTypes.Cntrl_ScadaHOA.ToString
            Case "2PosSw"
                CorrectObjectType = ObjectTypes.Cntrl_Switch2pos.ToString
            Case "3PosSw"
                CorrectObjectType = ObjectTypes.Cntrl_Switch3pos.ToString
            Case "Alarms", "DA"
                CorrectObjectType = ObjectTypes.Discrete_Alarm.ToString
            Case "DI", "Fltr"
                CorrectObjectType = ObjectTypes.Discrete_Status.ToString
            Case "Gen"
                CorrectObjectType = ObjectTypes.Generator.ToString
            Case "Blower", "ChemPmp", "EGPmp", "Fan", "Grinder", "GritWasher", "MLGrind", "MLPmp", "Pmp", "SumpPmp"
                CorrectObjectType = ObjectTypes.Motor_Standard.ToString
            Case "TroughStatus", "VlvStatus"
                CorrectObjectType = ObjectTypes.Multistate_Indicator.ToString
            Case "Multistate"
                CorrectObjectType = ObjectTypes.Multistate_Indicator.ToString
            Case "AlmRst", "PushButton"
                CorrectObjectType = ObjectTypes.PushButton.ToString
            Case "ToggleSw", "ToggleSwitch", "EnDis"
                CorrectObjectType = ObjectTypes.Toggle_Switch.ToString
            Case "ScumTrough", "Trough"
                CorrectObjectType = ObjectTypes.ScumTrough.ToString
            Case "SP", "Setpoint"
                CorrectObjectType = ObjectTypes.Setpoint.ToString
            Case "AlarmSP", "SPAlm", "AlmSP"
                CorrectObjectType = ObjectTypes.SetpointAlarm.ToString
            Case "AlmDI"
                CorrectObjectType = ObjectTypes.SetpointAlarm_DI.ToString
            Case "2DirVlv", "2StateVlv", "3WayVlv", "AerVlv", "FEQVlv", "ModVlv", "SludgeVlv", "Vlv"
                CorrectObjectType = ObjectTypes.Valve_Standard.ToString
            Case "PID"
                CorrectObjectType = ObjectTypes.PID.ToString
            Case "RTC"
                CorrectObjectType = ObjectTypes.RTC.ToString
            Case Else
                CorrectObjectType = "<NOT_FOUND>"
        End Select
        Return CorrectObjectType
    End Function

    Public Function RemoveOperators(ByVal comparator As String, ByRef Tagname As String, ByRef FoundOperator As String) As Boolean 'Return = "Yes, I found that operator"
        'Split out the operators ==, <>, <=, >=, <, >, *, +, /, -, (, )
        Try
            If Tagname.Contains(comparator) Then
                Dim parts() = Tagname.Replace(" ", "").Split(comparator)
                Tagname = parts(0).Trim
                FoundOperator = comparator & parts(1).Trim
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public ITPROP As String = "{ITPROP}"
    Public Function ContainsInTouchProperty(ByRef val As String) As Boolean
        Dim result As Boolean = False
        'Find any stragglers and correct them.  (This used to use .ToLowerInvariant, but it ruined the entire anim.value)
        Dim v As String = val.Replace(".alarm", ".Alarm").Replace(".ack", ".Ack").Replace(".Unack", ".UnAck").Replace(".unack", ".UnAck").
                Replace(".Qualitystatus", ".QualityStatus").Replace(".qualitystatus", ".QualityStatus").
                Replace(".Alarmdisabled", ".AlarmDisabled").Replace(".alarmdisabled", ".AlarmDisabled").
                Replace(".Engunits", ".EngUnits").Replace(".engunits", ".EngUnits").Replace(".name", ".Name")

        'Add <ITPROP> to detect InTouch properties
        If v.Contains(".Alarm") Then
            result = True
            v = v.Replace(".Alarm", ITPROP & "Alarm")
        End If
        If v.Contains(".UnAck") Then
            result = True
            v = v.Replace(".UnAck", ITPROP & "UnAck")
        End If
        If v.Contains(".Ack") Then
            result = True
            v = v.Replace(".Ack", ITPROP & "Ack")
        End If
        If v.Contains(".QualityStatus") Then
            result = True
            v = v.Replace(".QualityStatus", ITPROP & "QualityStatus")
        End If
        If v.Contains(".AlarmDisabled") Then
            result = True
            v = v.Replace(".AlarmDisabled", ITPROP & "AlarmDisabled")
        End If
        If v.Contains(".EngUnits") Then
            result = True
            v = v.Replace(".EngUnits", ITPROP & "EngUnits")
        End If
        If v.Contains(".Name") Then
            result = True
            v = v.Replace(".Name", ITPROP & "Name")
        End If
        'Load back the new string with replacements
        val = v

        Return result
    End Function

    'Make a list of all animations that would never contain tags
    Dim AnimationsWithoutTags As New List(Of String)
    Public Sub FillAnimList()
        AnimationsWithoutTags.Add("Blink.FillColor")
        AnimationsWithoutTags.Add("Blink.FillColor.SolidFill.FillColor")
        AnimationsWithoutTags.Add("Blink.LineColor")
        AnimationsWithoutTags.Add("Blink.LineColor.SolidFill.FillColor")
        AnimationsWithoutTags.Add("Blink.TextColor")
        AnimationsWithoutTags.Add("Blink.TextColor.SolidFill.FillColor")
        AnimationsWithoutTags.Add("FillChangeColor.FalseColor")
        AnimationsWithoutTags.Add("FillChangeColor.FalseColor.SolidFill.FillColor")
        AnimationsWithoutTags.Add("FillChangeColor.TrueColor")
        AnimationsWithoutTags.Add("FillChangeColor.TrueColor.SolidFill.FillColor")
        AnimationsWithoutTags.Add("FillChangeColor.TruthTableBreakPoints")
        AnimationsWithoutTags.Add("FillChangeColor.TruthTableBreakPoints.TruthTableRow.BreakPoint")
        AnimationsWithoutTags.Add("FillChangeColor.TruthTableBreakPoints.TruthTableRow.Color")
        AnimationsWithoutTags.Add("HorizontalLocation.MoveDown")
        AnimationsWithoutTags.Add("HorizontalLocation.MoveUp")
        AnimationsWithoutTags.Add("HorizontalLocation.ValueBottom")
        AnimationsWithoutTags.Add("HorizontalLocation.ValueTop")
        AnimationsWithoutTags.Add("HorizontalPercentFill.PercentMax")
        AnimationsWithoutTags.Add("HorizontalPercentFill.PercentMin")
        AnimationsWithoutTags.Add("HorizontalPercentFill.UnfilledStyle")
        AnimationsWithoutTags.Add("HorizontalPercentFill.UnfilledStyle.SolidFill.FillColor")
        AnimationsWithoutTags.Add("HorizontalPercentFill.ValueMax")
        AnimationsWithoutTags.Add("HorizontalPercentFill.ValueMin")
        AnimationsWithoutTags.Add("PushButton.Discrete.Send")
        AnimationsWithoutTags.Add("Text.TextStyleData.Text")
        AnimationsWithoutTags.Add("TextChangeColor.FalseColor")
        AnimationsWithoutTags.Add("TextChangeColor.FalseColor.SolidFill.FillColor")
        AnimationsWithoutTags.Add("TextChangeColor.FalseFont")
        AnimationsWithoutTags.Add("TextChangeColor.TrueColor")
        AnimationsWithoutTags.Add("TextChangeColor.TrueColor.SolidFill.FillColor")
        AnimationsWithoutTags.Add("TextChangeColor.TrueFont")
        AnimationsWithoutTags.Add("Tooltip.Expression")
        AnimationsWithoutTags.Add("TruthTableRow.Color.SolidFill.FillColor")
        AnimationsWithoutTags.Add("UserInput.Analog.Interaction")
        AnimationsWithoutTags.Add("UserInput.Analog.ValueLimits.Maximum")
        AnimationsWithoutTags.Add("UserInput.Analog.ValueLimits.Minimum")
        AnimationsWithoutTags.Add("UserInput.MessageToUser")
        AnimationsWithoutTags.Add("ValueDisplay.Discrete.FalseMessage")
        AnimationsWithoutTags.Add("ValueDisplay.Discrete.TrueMessage")
        AnimationsWithoutTags.Add("VerticalLocation.MoveDown")
        AnimationsWithoutTags.Add("VerticalLocation.MoveUp")
        AnimationsWithoutTags.Add("VerticalLocation.ValueBottom")
        AnimationsWithoutTags.Add("VerticalLocation.ValueTop")
        AnimationsWithoutTags.Add("VerticalPercentFill.PercentMax")
        AnimationsWithoutTags.Add("VerticalPercentFill.PercentMin")
        AnimationsWithoutTags.Add("VerticalPercentFill.UnfilledStyle")
        AnimationsWithoutTags.Add("VerticalPercentFill.UnfilledStyle.SolidFill.FillColor")
        AnimationsWithoutTags.Add("VerticalPercentFill.ValueMax")
        AnimationsWithoutTags.Add("VerticalPercentFill.ValueMin")
        AnimationsWithoutTags.Add("VerticalSlider.MoveDown")
        AnimationsWithoutTags.Add("VerticalSlider.MoveUp")
        AnimationsWithoutTags.Add("VerticalSlider.ValueBottom")
        AnimationsWithoutTags.Add("VerticalSlider.ValueTop")
    End Sub
    'Determines if the animation type would never contain a tagname
    Public Function AnimTypeWithoutTag(ByRef AnimType As String) As Boolean
        If AnimationsWithoutTags.Contains(AnimType) Then
            Return True
        Else
            Return False
        End If
    End Function


#Region "FTViewSE"

    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   Get all animations from a SINGLE symbol group
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Public Function GetAnimationsFromOneItem_FTViewSE(aaGraphicname As String, xSourceElem As XElement, xAttrName As String, xPrefix As String) As List(Of String)
        Dim ErrorItem As String = ""
        Dim ReturnStatusList As New List(Of String)

        Try
            'First, get all animation elements
            Dim xAnimations As New List(Of XElement)
            xAnimations = xSourceElem.Descendants("animations").ToList




            If xAnimations.Count > 0 Then
                'xAnimations.AddRange(xSourceElem.Descendants.Where(Function(x) Not IsNothing(x.Element("Animations"))).ToList)
                'Instantiate 
                Dim aaObject As New aaObject
                Dim SourceName = xSourceElem.Attribute("Name").Value.ToString
                If Not aaObjects.Exists(Function(x) x.GraphicFilename = aaGraphicname And x.ObjectName = SourceName) Then
                    aaObject.xElem = xSourceElem
                    aaObject.SourceName = SourceName
                Else
                    'Do nothing
                End If

                'Remove the # (MJS - REMOVED AND NOW CHECKED INSIDE Equate_ImportedObjectType_to_CorrentObjectType()
                'aaObject.SourceName = SourceName.Substring(1)
                If Not IsNothing(aaObject.SourceName) Then
                    If aaObject.SourceName.Contains("_") Then
                        Dim parts() = aaObject.SourceName.Split("_")
                        aaObject.ObjectTypeImported = parts(0)
                        aaObject.ObjectName = parts(1)
                        'If more than 3 parts, then something is wrong since it is only suppossed to be #ObjType_ObjName
                        If parts.Count > 2 Then
                            'THIS IS INTENDED TO REPAIR when the user created more parts than they should have
                            Select Case parts(2)
                                Case "0", "1", "2", "3", "00", "01"
                                    'This is where Alex thought he was suppossed to add a 0 or 1 behind each
                                    '   > Just proceed
                                Case Else
                                    'TODO - We need more error checking here for more complex objects like pumps, etc
                                    'Combine the extra parts into one name
                                    For i = 2 To parts.Count - 1
                                        aaObject.ObjectName = aaObject.ObjectName & "_" & parts(i)
                                    Next
                            End Select
                            For i = 2 To parts.Count - 1
                                If i = 2 Then
                                    'Show the 3rd part in Overflow column
                                    aaObject.SourceName_Overflow = parts(i)
                                Else
                                    'Error if more than 3 parts
                                    aaObject.SourceName_Overflow = "<ERROR: " & parts.Count & " parts>"
                                End If
                            Next
                        End If
                    Else
                        aaObject.ObjectTypeImported = "<Unknown>"
                        aaObject.ObjectName = SourceName
                    End If
                    'Equate any unknown object types (from human error in the aaGraphic #Group naming) to an object type that exists in our standard object list
                    aaObject.ObjectType = Equate_ImportedObjectType_to_CorrentObjectType(aaObject.ObjectTypeImported)

                    aaObject.Xpos = CInt(xSourceElem.Attribute("Left").Value)
                    aaObject.Ypos = Int(xSourceElem.Attribute("Top").Value)
                    'Add object to the return list for display
                    ReturnStatusList.Add(aaObject.ObjectName & " | " & aaObject.ObjectTypeImported & " | " & aaObject.Xpos & " | " & aaObject.Ypos)

                    'Loop through all the xElements that it found
                    For Each item In xAnimations
                        For Each elem In item.Descendants()
                            'Loop through all decendant
                            For Each e In elem.Descendants
                                ErrorItem = item.Parent.Name.ToString
                                'Instantiate a new object
                                Dim anim As New aaObject.aaAnim

                                'Check if the Attirbute "Name" exists in the parent element before reading it.
                                If Not IsNothing(item.Parent.Attribute("Name")) Then
                                    If item.Parent.Attribute("Name").Value = SourceName Then
                                        anim.Parent = item.Parent.Attribute("Name").Value
                                    Else
                                        anim.Parent = SourceName & "." & item.Parent.Attribute("Name").Value
                                    End If
                                Else
                                    'If no name attribute in parent, then the parent is GraphicElements 
                                    anim.Parent = item.Parent.Parent.Attribute("Name").Value
                                End If

                                ErrorItem = anim.Parent & " | " & elem.Name.ToString & " | " & e.Name.ToString

                                'Add the parent element names for any attributes all the way back up to 'Animations'
                                Dim test1 = item.Name.ToString
                                Dim test2 = elem.Name.ToString
                                Dim test3 = e.Name.ToString
                                If item.Name.ToString = "Text" Then
                                    'Dim stophere1 = 1
                                    'Filling in animation values even though these aren't animations. Doing this only to fill in the blank outputs.
                                    anim.Tags.Tag1.TagFound = item.Attribute("Name").Value.Trim
                                    anim.Value = item.Element("TextStyleData").Attribute("Text").Value
                                    anim.AnimType = "Text.TextStyleData.Text"
                                    'We are looping through all the text attributes but don't really care about them. This only adds the animation item if the tag(Name of text element) doesn't exist yet
                                    If aaObject.Animations.Where(Function(x) x.Tags.Tag1.TagFound = anim.Tags.Tag1.TagFound).Count = 0 Then
                                        aaObject.Animations.Add(anim)
                                    End If
                                    'Read next element in list
                                    Continue For
                                ElseIf e.Name.ToString = "Scripts" Then
                                    'Get the animation type
                                    anim.AnimType = e.Parent.Name.ToString & "." & e.Name.ToString & "." & e.Elements.FirstOrDefault.Name.ToString
                                    'Fill in the animation value from the first script element
                                    anim.Value = e.Elements.FirstOrDefault.Value
                                    'Remove any carriage returns or line feeds
                                    anim.Value = anim.Value.Replace(vbCr, "").Replace(vbLf, "")
                                    'Add to the list unless it Is a duplicate (This Is because it finds the element nested.  There Is probably a better way, but this works.)
                                    If Not aaObject.Animations.Contains(anim) Then aaObject.Animations.Add(anim)
                                    Continue For
                                ElseIf e.Parent.Name = "Animations" Then
                                    anim.AnimType = e.Name.ToString
                                    Dim stopme = 1
                                ElseIf e.Parent.Parent.Name = "Animations" Then
                                    anim.AnimType = e.Parent.Name.ToString & "." & e.Name.ToString
                                    Dim stopme = 1
                                ElseIf e.Parent.Parent.Parent.Name = "Animations" Then
                                    anim.AnimType = e.Parent.Parent.Name.ToString & "." & e.Parent.Name.ToString & "." & e.Name.ToString
                                Else
                                    anim.AnimType = e.Parent.Parent.Parent.Name.ToString & "." & e.Parent.Parent.Name.ToString & "." & e.Parent.Name.ToString & "." & e.Name.ToString
                                End If
                                'Add the first attributes value (TODO MICHAEL - This isn't perfect since some have more than one, but should be OK for what we are doing in this project
                                Try
                                    Dim FirstAttrName As String
                                    Dim FirstAttrValue As String
                                    If Not IsNothing(e.FirstAttribute) Then
                                        FirstAttrName = e.FirstAttribute.Name.ToString()
                                        FirstAttrValue = e.FirstAttribute.Value.ToString()

                                        'Ignore the Enable attributes
                                        If FirstAttrName = "Enabled" Then
                                            'Ignore
                                        Else
                                            anim.Value = FirstAttrValue
                                            'Add to the list unless it is a duplicate (This is because it finds the element nested.  There is probably a better way, but this works.)
                                            If Not aaObject.Animations.Contains(anim) Then
                                                aaObject.Animations.Add(anim)
                                            End If
                                        End If
                                    End If

                                Catch ex As Exception
                                    Dim reportthis = ErrorItem
                                End Try
                            Next
                        Next
                    Next
                    aaObject.GraphicFilename = Path.GetFileName(aaGraphicname)
                    aaObject.TextElements = xSourceElem.Descendants("Text").Select(Function(x) x.Element("TextStyleData").Attribute("Text").Value).ToList
                    'Add to the objects list
                    aaObjects.Add(aaObject)

                    For Each item In aaObject.Animations
                        'Return for display
                        ReturnStatusList.Add("⮡  " & item.Parent & " | " & item.AnimType & " | " & item.Value)
                    Next
                Else
                    'Do nothing
                End If

            Else
                'Do nothing
            End If


            Return ReturnStatusList

        Catch ex As Exception
            MessageBox.Show(ErrorItem & vbCrLf & ex.ToString)
            Return New List(Of String)({ErrorItem, "", "", ExResults(ex)})
        End Try
    End Function

#End Region


End Class


'━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
'   aaObject Classes (used for creation of new graphics)
'━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Public Class aaObject
    Public Property SourceName As String
    Public Property SourceName_Overflow As String = ""
    Public Property xElem As XElement
    Public Property ObjectTypeImported As String
    Public Property ObjectType As String
    Public Property ObjectName As String
    Public Property Xpos As String 'Left
    Public Property Ypos As String 'Top
    Public Property WidthIn As String
    Public Property Animations As New List(Of aaAnim)
    Public Property CustProps As New List(Of aaCustProp)
    Public Property Options As New List(Of aaOption)
    Public Property SubstituteStrings As New List(Of aaString)
    Public Property Tags As New aaTags
    Public Property GraphicFilename As String
    Public Property SymbolReference As String
    Public Property TextElements As New List(Of String)
    Public Property Height As String
    Public Property Width As String
    Public Property Description As String = ""
    Public Property InGroup As Boolean = False
    Public Property GroupName As String = ""
    Public Property FTViewGroupPath As String = ""
    Public Property T4PageNumber As String = ""
    Public Property T4PageName As String = ""
    Public Property FontSize As String = ""
    Public Property Alignment As String = ""

    Public Property Description_ElementID_Postfix As String = ".Description" 'Equal to the GraphicElement Name of the object that should have it's text updated
    Public ReadOnly Property XposInt As Integer
        Get
            Dim xAdjust As Integer = CInt(Xpos) + CInt(WidthIn)
            Dim xNew As String = xAdjust
            Return xNew.Substring(0, xNew.Length - 2) & "00"
        End Get
    End Property
    Public ReadOnly Property YposInt As Integer
        Get
            Return Ypos
        End Get
    End Property


    Public Class aaAnim
        'Parent of the Animations element.  Will typically be a <Group>
        Public Property Parent As String
        'ValueDisplay.Enabled, Analog.Expression, etc.  This will either be element.attribute, or element.element.attribute, etc.
        Public Property AnimType As String
        'Reference/Expression/etc.  It doesn't matter what the element's name is, just get any attribute that has a value.
        Public Property Value As String
        'Tagname extracted from the animation based on ObjType.ObjName.Property
        Public Property Tags As New aaAnimDetails
    End Class
    Public Class aaAnimDetails
        Public Property Tag1 As New aaAnimTag
        Public Property MoreTags As New List(Of aaAnimTag)
        'Public Property Tag3 As aaAnimTag
        Public Property FoundInTouch As String = "" 'Displays InTouch: if it is removed
        Public Property FoundOR As String = "" 'NOT was found
        Public Property FoundAND As String = "" 'AND was found
        Public Property ErrorCheck As String = ""
        Public Property ErrorNotes As String = ""
    End Class
    Public Class aaAnimTag
        Public Property TagFound As String = "" 'Tagname.Property after stripping the operators
        Public Property FoundOperator As String = "" '=0, =1, =2
        Public Property FoundNOT As String = "" 'NOT was found
        Public Property p1_ObjType As String = "" 'part1
        Public Property p2_ObjName As String = "" 'part2
        Public Property p3_Property As String = "" 'part3
        Public Property TagName As String = "" 'Tagname after removing the property
    End Class
    Public Class aaCustProp
        Public Property Name As String
        Public Property Value As String
        Public Property DataType As String
        Public Property Overridden As String
        Public Property ExpressionType As String
    End Class
    Public Class aaOption
        Public Property Name As String
        Public Property Value As String
        Public Property Overridden As String
    End Class
    Public Class aaString
        Public Property OldText As String
        Public Property NewText As String
        Public Property ElementID As String
        Public Property Order As Integer
    End Class
    Public Class aaTags '(TODO - Possible Future Use)
        Public Property Val As String
        Public Property Units As String
        Public Property Alarm As String
        Public Property Run As String
        Public Property Speed As String
        Public Property RunCall As String
        Public Property Amps As String
        Public Property Auto As String
        Public Property Hand As String
        Public Property future1 As String
        Public Property future2 As String
        Public Property future3 As String
    End Class
End Class


Public Class aaGroupImport
    Public Property xElem As XElement
    Public Property GraphicFilename As String = ""
    Public Property NewGraphicFilename As String = ""
    Public Property GroupName As String = ""
    Public Property NewGroupName As String = ""
    Public Property Xpos As String = ""
    Public Property Ypos As String = ""
    Public Property GroupIsEmpty As Boolean = False
    Public Property GroupHeader As String = ""
    Public Property GroupHeaderValue As String = ""
End Class
Public Class aaGraphicGroupPair
    Public Property Graphic As String = ""
    Public Property Group As String = ""
End Class

''' <summary>
''' All the standard object types available
''' </summary>
Public Enum ObjectTypes
    AI_Basic
    AI_Tank
    AI_Sidebar
    Cntrl_ScadaHOA
    Cntrl_Switch2pos
    Cntrl_Switch3pos
    Discrete_Alarm
    Discrete_Status
    Generator
    Motor_Standard
    Multistate_Indicator
    PID
    PushButton
    RTC
    ScumTrough
    Setpoint
    SetpointAlarm
    SetpointAlarm_DI
    Toggle_Switch
    Valve_Standard
    'NOT_FOUND - Need to add <>, so can't use as an enum
End Enum



'Caden's original ones.  Not used currently.
Public Class CustomProperty
    Public Property Name As String
    Public Property DataType As String
    Public Property Locked As String
    Public Property Visibility As String
    Public Property Overridden As String
    Public Property Expression As New CustExpression
End Class
'Caden's original ones.  Not used currently.
Public Class CustExpression
    Public Property Text As String
    Public Property Expression As String
End Class






'Dividers

'▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼

'▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲

'■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

'━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
