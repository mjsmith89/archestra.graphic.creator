﻿Module ExceptionResults

    Public ShowAdvancedExResults As Boolean = False

    Public Function ExResults(ex As Exception) As String
        Dim result As String
        If ShowAdvancedExResults Then
            result = ex.ToString
        Else
            result = ex.Message
        End If
        Return result
    End Function

End Module
