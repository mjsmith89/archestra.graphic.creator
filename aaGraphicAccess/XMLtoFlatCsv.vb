﻿Imports System
Imports ArchestrA.GRAccess
Imports ArchestrA.Visualization.GraphicAccess
Imports System.Xml.Linq
Imports System.Linq
Imports System.Collections.Generic
Imports System.IO
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports System.Reflection
Imports System.ComponentModel
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Runtime.CompilerServices
Imports System.Diagnostics
Imports System.Threading

Public Class XMLtoFlatCsv

    Public CsvSaveFilename = "C:\Files\Temp\Benchmark_XML_Visualizer.csv"
    Public ExcelFilename = "C:\Files\Temp\Benchmark_XML_Visualizer.xlsm"

    Public Sub ConvertXMLtoCSV(ByVal filename As String)
        Dim SubTime As New Stopwatch
        SubTime.Start()
        Dim CreateEmptyCollections As Boolean = False
        Dim xDoc = XDocument.Load(filename)
        Dim xRoot As XElement = xDoc.Root
        Dim xDescendants = xRoot.DescendantsAndSelf.ToList
        Dim xRootName = xRoot.Name.ToString
        Dim LastTreeLevel As String = "-1"
        Dim TreeLevelMax As Integer = -1
        Dim LastAncestors As New List(Of String)

        Dim XmlList As New List(Of FlatXML)
        For Each xElem In xDescendants
            Dim x As New FlatXML
            x.Name = xElem.Name.ToString
            'x.Index = xDescendants.IndexOf(xElem)

            xElem.AncestorsAndSelf.Select(Function(s) s.Name.ToString).Reverse.ToList

            x.TreeLevel = xElem.Ancestors.Count
            If x.TreeLevel > TreeLevelMax Then TreeLevelMax = x.TreeLevel
            If x.TreeLevel <> LastTreeLevel Then
                x.Ancestors = xElem.Ancestors.Select(Function(s) s.Name.ToString).Reverse.ToList
                If x.Ancestors.Count = 0 Then
                    x.Name = " " 'root removed for a cleaner csv
                Else
                    x.Ancestors(0) = "" 'root removed for a cleaner csv
                End If
                LastTreeLevel = x.TreeLevel
                LastAncestors = x.Ancestors
            Else
                x.Ancestors = LastAncestors
            End If

            x.Attributes = xElem.Attributes.ToDictionary(Function(s) s.Name.ToString, Function(s) s.Value.ToString)
            If Not x.Attributes.Any Then
                Dim stopandcheck = 1
            End If

            '** Returns all attributes **
            If CreateEmptyCollections Then
                'OPTION 1 - Fill the dictionary regardless
                x.Attributes = xElem.Attributes.ToDictionary(Function(s) s.Name.ToString, Function(s) s.Value.ToString)
            Else
                'OPTION 2 - Force the dictionary as 'Nothing' if no attributes to save memory
                x.Attributes = xElem.Attributes.ToDictionary(Function(s) s.Name.ToString, Function(s) s.Value.ToString)
                If x.Attributes.Any Then
                    x.HasAttributes = True
                Else
                    x.Attributes = Nothing
                End If
            End If

            '** Finds all elements that have a text value (in leiu of attributes or sub-elements) **
            If CreateEmptyCollections Then
                'OPTION 1 - Fill the dictionary regardless
                x.TextElements = xElem.Elements.Where(Function(s) Not s.HasElements And Not s.IsEmpty).ToDictionary(Function(s) s.Name.ToString, Function(s) s.Value.ToString)
            Else 'OPTION 2 - Keep the dictionary as 'Nothing' if no elements to save memory
                Dim xElements = xElem.Elements.Where(Function(s) Not s.HasElements And Not s.IsEmpty).ToList
                If xElements.Any Then
                    x.HasTextElements = True
                    x.TextElements = xElements.ToDictionary(Function(s) s.Name.ToString, Function(s) s.Value.ToString)
                End If
            End If

            'If the parent has child text elements, then you are a TextOnly Element
            Try
                If Not xElem.HasElements And Not xElem.IsEmpty Then x.IsTextElement = True
            Catch ex As Exception 'Ignore the root since it is index 0
            End Try

            'Add to the list ONLY if it has attributes or text elements.  Otherwise, it adds the text elements twice
            'If x.HasAttributes Or x.HasTextElements Then XmlList.Add(x)
            XmlList.Add(x)
        Next

        'SURPRISINGLY, both options use about the same amount of time.  
        'Leaving the dictionaries open saves about 5% memory for a large XML file (78MB vs 83MB for a 8MB XML file).  But it is the same for small XML files (37MB for a 1.2MB XML)
        Dim MemUsageinMB As Double = System.Diagnostics.Process.GetCurrentProcess.WorkingSet64 / 1024 / 1024
        Dim HowLongDidItTake = SubTime.Elapsed

        'Get a list of exclusive attributes and (FUTURE) sort by occurrence count
        Dim AttrNamesDict As New Dictionary(Of String, Integer)
        For Each item In XmlList
            If Not IsNothing(item.Attributes) Then
                For Each a In item.Attributes
                    If AttrNamesDict.ContainsKey(a.Key) Then
                        AttrNamesDict(a.Key) += 1 'Increment the occurence counter if the entry exists
                    Else
                        AttrNamesDict.Add(a.Key, 1) 'Add the entry if it doesn't exist
                    End If
                Next
            End If
        Next
        'Convert the keys to a list for use below
        Dim AttrNames = AttrNamesDict.Keys.ToList
        Dim AttrNamesHeader = String.Join(",", AttrNames)

        'Do the same for text elements
        Dim TextElemNames As New List(Of String)
        For Each item In XmlList
            If Not IsNothing(item.TextElements) Then
                For Each a In item.TextElements
                    'Add the entry if it doesn't exist
                    If Not TextElemNames.Contains(a.Key) Then TextElemNames.Add(a.Key)
                Next
            End If
        Next
        'Add an E: to the header to designate it is an Element, not attribute
        Dim TextElemNamesHeader = "E:" & String.Join(",E:", TextElemNames)

        'Form the csv
        Dim CsvList As New List(Of String)
        Dim LevelsHeader = String.Join(",", Enumerable.Repeat("Element", TreeLevelMax))
        CsvList.Add("Index,Tree," & LevelsHeader & "," & "HasAttr,HasTxtElem," & AttrNamesHeader & "," & TextElemNamesHeader)
        For Each i In XmlList
            'Don't print text element lines since they are printed earlier with their parent
            If i.IsTextElement Then Continue For
            'Add blank columns so they line up well
            Dim BlankColumnCount = TreeLevelMax - i.TreeLevel
            If i.TreeLevel = 0 Then BlankColumnCount = TreeLevelMax - i.TreeLevel - 1
            Dim AddColumns = String.Join("", Enumerable.Repeat(",", BlankColumnCount))
            'Form the base columns
            Dim base = XmlList.IndexOf(i) & "," & i.TreeLevel & String.Join(",", i.Ancestors) & "," & i.Name & AddColumns
            'Add Attribute Columns
            Dim ThisRowAttrCount = ""
            Dim CombinedAttr As String = ""
            If i.HasAttributes Then
                ThisRowAttrCount = i.Attributes.Count.ToString
                Dim AttrColumns As List(Of String) = Enumerable.Repeat("", AttrNames.Count).ToList
                For Each v In i.Attributes
                    Dim col = AttrNames.IndexOf(v.Key)
                    AttrColumns(col) = v.Value
                Next
                CombinedAttr = "," & String.Join(",", AttrColumns)
            Else
                CombinedAttr = "," & String.Join(",", Enumerable.Repeat("", AttrNames.Count))
            End If
            'Add Text Element Columns
            Dim ThisRowTextElemCount = ""
            Dim CombinedTextElem As String = ""
            If i.HasTextElements Or False Then
                ThisRowTextElemCount = i.TextElements.Count.ToString
                Dim TextElemColumns As List(Of String) = Enumerable.Repeat("", TextElemNames.Count).ToList
                For Each v In i.TextElements
                    Dim col = TextElemNames.IndexOf(v.Key)
                    TextElemColumns(col) = v.Value
                Next
                CombinedTextElem = "," & String.Join(",", TextElemColumns)
            Else
                CombinedTextElem = "," & String.Join(",", Enumerable.Repeat("", TextElemNames.Count))
            End If
            'Add the csv row
            CsvList.Add(base & "," & ThisRowAttrCount & "," & ThisRowTextElemCount & CombinedAttr & CombinedTextElem)
        Next
        'Save the csv and open the Excel visualizer
        IO.File.WriteAllLines(CsvSaveFilename, CsvList)
        Process.Start(ExcelFilename)

    End Sub

    Public Sub XML_Testing(ByVal filename As String)
        Dim xDoc = XDocument.Load(filename)

        Dim base As XElement = xDoc.Root.FirstNode

        Dim xRoot = base '.Elements.FirstOrDefault.Element("FillStyleData")
        'The whole tree underneath
        Dim xDescendantNodes = xRoot.DescendantNodes.ToList 'All elements PLUS any element values in the tree. Does NOT include any of the attributes.  NOT in order.  **NOT USEFUL - Use .Descendants**
        Dim xDescendants = xRoot.Descendants.Select(Function(x) x.Name).ToList 'Same as above except for it doesn't mix in the element values as strings.  That is OK because the values are still inside the elements and can be extracted.  
        'Items directly underneath, not the whole tree
        Dim xNodes = xRoot.Nodes.ToList 'Same as DescendantNodes but only the ones directly underneath this one.  NOT the entire tree.
        Dim xElements = xRoot.Elements.Select(Function(x) x.Name).ToList 'The elements directly underneath this one.  NOT the entire tree.
        Dim xAttributes = xRoot.Attributes.Select(Function(x) x.Name).ToList 'The attributes directly underneath this one.  NOT the entire tree.
        'Parent
        Dim xParent = xRoot.Parent.Name
        'Details about a node
        Dim xHasElements = xRoot.HasElements
        Dim xHasAttributes = xRoot.HasAttributes
        Dim xIsEmpty = xRoot.IsEmpty 'This means it has no Elements.  It will still report back as true if it has attributes and no elements.
        Dim xNodeType = xRoot.NodeType
        'Doesn't work
        Dim xDescendantNodeTypes = xRoot.DescendantNodes.Select(Function(x) x.NodeType).ToList 'For some reason, this doesn't work.  It reports back attributes as elements
        'Specialized lists
        Dim a1 = xRoot.Descendants.Where(Function(x) x.NodeType.ToString = "Element").ToList
        Dim a2 = xRoot.Descendants.Where(Function(x) x.NodeType.ToString = "Attribute").ToList
        Dim ElementsWithNoChildElements = xRoot.Descendants.Where(Function(x) x.IsEmpty).ToList 'All the elements in the tree that don't have any child elements.  It will still report back as true if it has attributes and no elements.
        Dim ChildElementsInTree = xRoot.Descendants.Where(Function(x) x.HasElements).ToList 'Reports back all elements in the descendant tree.
        Dim ChildElementsAttributesInTree = xRoot.Descendants.Where(Function(x) x.HasAttributes).ToList 'Reports back all attributes in the descendant tree EXCEPT it does NOT include any of the attributes directly underneath for some odd reason.
        Dim ElementValues = xRoot.Elements.Select(Function(x) x.Value).ToList 'This is for element values when the element has a direct value (not an attribute value or a child element)

        Dim HowManyLevelsDownTheTreeAmI = xRoot.Ancestors.ToList.Count
        If xRoot.HasElements Then Dim ElementBelongsToMe = xRoot.Elements.FirstOrDefault.Parent.Equals(xRoot)
        If xRoot.HasAttributes Then Dim AttributeBelongsToMe = xRoot.Attributes.FirstOrDefault.Equals(xRoot)

        Dim XTreeLevels = xRoot.Descendants.Select(Function(x) x.Ancestors.Count).ToList

        Dim t3 = xRoot.Elements.FirstOrDefault.Attributes.ToList

    End Sub

    'TXElement.NodeType Enum
    '     None = 0
    ' An element (for example, <item> )
    '     Element = 1`
    'An attribute (for example, id='123')
    '     Attribute = 2
    'The text content of a node
    '     Text = 3
    'A CDATA section (for example, <![CDATA[my escaped text]]> )
    '     CDATA = 4
    'A reference to an entity (for example, &num; )
    '     EntityReference = 5
    'An entity declaration (for example, <!ENTITY...> )
    '     Entity = 6
    'A processing instruction (for example, <?pi test?> )
    '     ProcessingInstruction = 7
    'A comment (for example, <!-- my comment --> )
    '     Comment = 8
    'A document object that, as the root of the document tree, provides access to the entire XML document
    '     Document = 9
    'The document type declaration, indicated by the following tag (for example, <!DOCTYPE...>)
    '     DocumentType = 10
    'A document fragment
    '     DocumentFragment = 11
    ' A notation in the document type declaration (for example, <!NOTATION...> )
    '     Notation = 12
    'White space between markup
    '     Whitespace = 13
    'White space between markup in a mixed content model or white space within the xml:space="preserve" scope
    'SignificantWhitespace = 14
    'An end element tag (for example, </item> )
    '     EndElement = 15
    'Returned when XmlReader gets to the end of the entity replacement as a result of a call to System.Xml.XmlReader.ResolveEntity
    '     EndEntity = 16
    'The XML declaration (for example, <?xml version='1.0'?> )
    '     XmlDeclaration = 17


End Class

Public Class FlatXML
    Public Property Name As String
    Public Property TreeLevel As String
    Public Property Ancestors As List(Of String)
    Public Property HasAttributes As Boolean = False
    Public Property HasTextElements As Boolean = False
    Public Property IsTextElement As Boolean = False
    Public Property Attributes As Dictionary(Of String, String)
    Public Property TextElements As Dictionary(Of String, String)
End Class


