﻿Imports System
Imports ArchestrA.GRAccess
Imports ArchestrA.Visualization.GraphicAccess
Imports System.Xml.Linq
Imports System.Linq
Imports System.Collections.Generic
Imports System.IO
Imports System.Windows.Forms
Imports System.Threading
Imports System.ComponentModel

Public Class aaGraphicAccess

    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   CADEN - Modify this to merge with what Michael is already doing
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Private Sub UpdateStatusBox(ByVal status As String, ByVal progress As Integer)
        'Use codes for various actions like clearing the box
        Select Case status
            Case "clearstatus"
                StatusList = New List(Of String)
                txt_StatusError.Text = ""
            Case "turn status box red"
                'future
            Case "Export All Complete"
                btn_aaExport_Toolset.BackColor = Color.LawnGreen
            Case "Single Export Complete"
                btn_aaExport.BackColor = Color.LawnGreen
            Case Else
                StatusList.Add(status)
                txt_StatusError.Text = String.Join(vbCrLf, StatusList.ToArray)
        End Select
        'Update Progress (optional)
        If progress > 0 And progress < 101 Then
            ' lbl_Progress1.Text = progress & "%"
        Else
            ' lbl_Progress1.Text = ""
        End If
    End Sub

    Dim StatusList As New List(Of String)
    'Instantiate the class library
    Dim a2x As New Archestra2Xml
    Dim processAnim As New InTouchTag_to_GalaxyAttribute
    Dim Initialized As Boolean = False
    'Setup background worker so UI can update
    Friend WithEvents BGW As New BackgroundWorker
    Friend WithEvents TmrNow As New System.Windows.Forms.Timer
    Dim BGW_Status As String = ""
    Dim BGW_Caller As String = ""

    Private Sub Loaded() Handles Me.Load
        txt_aaGraphicName.Text = "IDE_Import_Symbol"
        txt_Galaxy.Text = "restored"
        'Setup BGW
        BGW.WorkerReportsProgress = True
        BGW.WorkerSupportsCancellation = True
        btn_Cancel_BGW.Visible = False
        TmrNow.Enabled = True
        'Preload the text boxes
        txt_ExportDir.Text = a2x.xmlFolderExport
        txt_ImportDir.Text = a2x.xmlFolderImport
        txt_TemplatesDir.Text = a2x.xmlFolderTemplates
        tb_CustPropsCSVDir.Text = a2x.StatusOutputFolder
        txt_ExportDir_Group.Text = a2x.xmlFolderExport
        tb_GroupsCSVDir.Text = "C:\Files\Temp"
        lbl_ImportingItem.Visible = False

    End Sub

    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   100: Export - IDE aaGraphics to XML
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Private Sub btn_GalaxyLogin_Click(sender As Object, e As EventArgs) Handles btn_GalaxyLogin.Click
        txt_StatusError.Text = a2x.GalaxyLogin(a2x.GalaxyName, "", "") ', "Benchmark", "Software")
        If txt_StatusError.Text.Substring(0, 15) = "Login succesful" Then
            lbl_GalaxyLoginRequired.Visible = False
            txt_Galaxy.BackColor = Color.LawnGreen
            a2x.GalaxyLoggedIn = True
        Else
            lbl_GalaxyLoginRequired.Visible = True
            txt_Galaxy.BackColor = Color.Red
            a2x.GalaxyLoggedIn = False
        End If
    End Sub

    Private Sub btn_aaExport_Click(sender As Object, e As EventArgs) Handles btn_aaExport.Click
        btn_aaExport.BackColor = Color.PeachPuff
        If Not a2x.GalaxyLoggedIn Then btn_GalaxyLogin_Click(New Object, New EventArgs)
        'Set a2x parameters
        a2x.aaGraphicName = txt_aaGraphicName.Text
        a2x.DeleteOneFileInDirectory(a2x.xmlFolderExport, lbl_XMLgraphic.Text)
        'Call as separate thread by subname
        a2x.CallThisSub = NameOf(a2x.Export_aaGraphicToXml)
        a2x.thread = New ThreadCallBack(Me, AddressOf a2x.DoWork, AddressOf UpdateStatusBox)
        a2x.thread.ID = a2x.CallThisSub
        a2x.thread.Start()
    End Sub

    'Do the above for an entire toolset(folder)
    Private Sub btn_aaExport_Toolset_Click(sender As Object, e As EventArgs) Handles btn_aaExport_Toolset.Click
        btn_aaExport_Toolset.BackColor = Color.PeachPuff
        If Not a2x.GalaxyLoggedIn Then btn_GalaxyLogin_Click(New Object, New EventArgs)
        'Ask to Delete existing
        a2x.DeleteAllFilesInDirectory(a2x.xmlFolderExport)
        'Call as separate thread by subname
        a2x.CallThisSub = NameOf(a2x.aaExport_Toolset)
        a2x.thread = New ThreadCallBack(Me, AddressOf a2x.DoWork, AddressOf UpdateStatusBox)
        a2x.thread.ID = a2x.CallThisSub
        a2x.thread.Start()
        Initialized = True
    End Sub

    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   200: Old Intouch Graphics manipulation by grabbing all animations and creating new objects from that 
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '200: Part 1 of 2
    Private Sub btn_AnimFromXml_Click(sender As Object, e As EventArgs) Handles btn_AnimFromXml.Click
        btn_AnimFromXml.BackColor = Color.PeachPuff
        'Initialize the xDoc
        If Not Initialized Then
            a2x.aaGraphicName = txt_aaGraphicName.Text
            Initialized = True
        End If
        'Dim MethodCall As String = NameOf(Archestra2Xml.InitializeXmlDoc)
        'Parse the XML file
        txt_StatusError.Text = a2x.GetAnimationsFromXml(Path.Combine(a2x.xmlFolderExport, a2x.aaGraphicName & ".xml"))
        'Save objects as CSV
        a2x.WriteAnimationsToCsv()
        btn_AnimFromXml.BackColor = Color.LawnGreen
    End Sub
    '200B: Entire Directory
    Private Sub btn_AnimFromXml_Directory_Click(sender As Object, e As EventArgs) Handles btn_AnimFromXml_Directory.Click
        btn_AnimFromXml_Directory.BackColor = Color.PeachPuff
        btn_Cancel_BGW.Visible = True
        'Set the calling button name so that the BGW can be used for multiple buttons
        BGW_Caller = sender.Name.ToString 'NameOf(aaGraphicAccess.btn_AnimFromXml_Directory_Click)
        'Start the Background Worker so the UI can update
        BGW.RunWorkerAsync() 'This starts the sub BGW_DowWork
    End Sub

    '205: Process animation report. Requires part 1 since we need to have an animations report ready to process
    Private Sub btn_ProcessAnmi_Click(sender As Object, e As EventArgs) Handles btn_ProcessAnim.Click
        btn_ProcessAnim.BackColor = Color.PeachPuff
        processAnim.ConvertCsv()
        btn_ProcessAnim.BackColor = Color.LawnGreen
    End Sub

    '210: Part 2 of 2 (requires 200:Part1 since that sub fills the aaObjects list)
    Private Sub btn_CreateGraphicfromObjectNames_Click(sender As Object, e As EventArgs) Handles btn_CreateGraphicfromObjectNames.Click
        btn_CreateGraphicfromObjectNames.BackColor = Color.PeachPuff
        'Delete all existing files in the Import folder
        Dim files As String() = IO.Directory.GetFiles(a2x.xmlFolderImport, "*.xml")
        If files.Length > 0 Then
            If MessageBox.Show("Delete the existing files in the Import directory or exit?", "Files Exist in the Directory", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                For Each file In files
                    IO.File.Delete(file)
                Next
                'Create XML for ALL Objects.  This function creates all the xml files, it is not just for one aaGraphic
                txt_StatusError.Text = txt_StatusError.Text & vbCrLf & a2x.CreateGraphicsFromaaObjects(txt_aaGraphicTemplate.Text, cb_UpdateExistingGraphics.SelectedItem)
            Else
                'Exit the sub
            End If
        Else
            'Create XML for ALL Objects.  This function creates all the xml files, it is not just for one aaGraphic
            txt_StatusError.Text = txt_StatusError.Text & vbCrLf & a2x.CreateGraphicsFromaaObjects(txt_aaGraphicTemplate.Text, cb_UpdateExistingGraphics.SelectedItem)
        End If
        btn_CreateGraphicfromObjectNames.BackColor = Color.LawnGreen
    End Sub

    ' **ALL-IN-ONE** COMBINES 200B, 210, 900B
    Private Sub btn_aaExport_Update_Click(sender As Object, e As EventArgs) Handles btn_aaExport_Update.Click
        btn_aaExport_Update.BackColor = Color.PeachPuff
        Try
            'TODO - TEMPORARY Setup status output
            Dim StatusOutputDir As String = a2x.xmlFolderExport & "\StatusOutput"
            IO.Directory.CreateDirectory(StatusOutputDir)
            'Get every file in the EXPORT directory and get animations from each
            Dim files As String() = IO.Directory.GetFiles(a2x.xmlFolderExport, "*.xml")
            For Each file In files
                Dim fileonly As String = IO.Path.GetFileNameWithoutExtension(file)
                If file.EndsWith(".xml") Then
                    a2x.aaGraphicName = file.Replace(".xml", "")
                    txt_aaGraphicName.Text = a2x.aaGraphicName
                    a2x.InitializeXmlDoc()
                    'Parse the XML file
                    txt_StatusError.Text = a2x.GetAnimationsFromXml(file)
                    'TODO - TEMPORARY Save a status text file since we can't see the updates because there were thread-safe problems
                    IO.File.WriteAllText(a2x.xmlFolderExport & "\StatusOutput\" & fileonly & ".txt", "SUCCESS")
                End If
            Next
            Dim FoundText As New List(Of String)(a2x.FoundTextList)
            'Save objects as CSV
            a2x.WriteAnimationsToCsv()
            'Cleanup status text files (TEMP until new threading works)
            For Each f In IO.Directory.GetFiles(StatusOutputDir)
                IO.File.Delete(f)
            Next

            'Delete all existing files in the Import folder
            For Each file In IO.Directory.GetFiles(a2x.xmlFolderImport, "*.xml")
                IO.File.Delete(file)
            Next

            'Create XML for ALL Objects and save to the IMPORT folder.  This function creates all the xml files, it is not just for one aaGraphic
            txt_StatusError.Text = txt_StatusError.Text & vbCrLf & a2x.CreateGraphicsFromaaObjects(txt_aaGraphicTemplate.Text, cb_UpdateExistingGraphics.SelectedItem)
            'Import to IDE
            'txt_StatusError.Text = txt_StatusError.Text & vbCrLf & a2x.aaImport_Directory(a2x.GalaxyName)
        Catch ex As Exception
            txt_StatusError.Text = "ERROR - aaExport_Update" & vbCrLf & ExResults(ex)
        End Try
        btn_aaExport_Update.BackColor = Color.LawnGreen
    End Sub

    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    ' 300: Grab Standard Objects and CustProp/Options from aaGraphics xml files
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Private Sub btn_GetCustPropandOptions_Click(sender As Object, e As EventArgs) Handles btn_GetCustPropandOptions.Click
        btn_GetCustPropandOptions.BackColor = Color.PeachPuff
        'Get key attributes
        Dim fileStrg As String = Path.Combine(a2x.xmlFolderExport, txt_aaGraphicName.Text & ".xml")
        txt_StatusError.Text = txt_StatusError.Text & a2x.GetCustomPropertiesAndOptionsFromXML(fileStrg)
        'Save objects as CSV
        txt_StatusError.Text = txt_StatusError.Text & vbCrLf & a2x.WriteCustomPropertiesAndOptionsToCSV()
        btn_GetCustPropandOptions.BackColor = Color.LawnGreen
    End Sub
    '300B: Entire Directory
    Private Sub btn_CustPropsAndOptionsFromXmlrectory_Click(sender As Object, e As EventArgs) Handles btn_CustPropsAndOptionsFromXml_Directory.Click
        btn_CustPropsAndOptionsFromXml_Directory.BackColor = Color.PeachPuff
        'Get every file in a directory and get animations from each
        Dim files As String() = IO.Directory.GetFiles(a2x.xmlFolderExport, "*.xml")
        For Each file In files
            If file.EndsWith(".xml") Then
                a2x.aaGraphicName = file.Replace(".xml", "")
                txt_StatusError.Text = txt_StatusError.Text & vbCrLf & a2x.GetCustomPropertiesAndOptionsFromXML(file)
            End If
        Next
        'Save the custom properties and options to CSV
        txt_StatusError.Text = txt_StatusError.Text & vbCrLf & a2x.WriteCustomPropertiesAndOptionsToCSV()
        btn_CustPropsAndOptionsFromXml_Directory.BackColor = Color.LawnGreen
    End Sub

    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    ' 400: Create aaGraphics xml from csv for Standard Objects.  Also sets all CustProp/Options(DOES NOT require that you run 300 in the same session)
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Private Sub btn_CreateGraphicFromCsv_Click(sender As Object, e As EventArgs) Handles btn_CreateGraphicFromSymbolReference.Click
        btn_CreateGraphicFromSymbolReference.BackColor = Color.PeachPuff
        'Combine the path and the filename into one file path
        Dim fileStrg As String = Path.Combine(a2x.StatusOutputFolder, lbl_CustPropsCSVFilename.Text)
        'Read the CSV into aaObjects
        txt_StatusError.Text = txt_StatusError.Text & vbCrLf & a2x.GetCustomPropertiesAndOptionsFromCSV(fileStrg)
        Dim XmlImportFiles = Directory.GetFiles(a2x.xmlFolderImport)
        'Delete any existing xml files in the directory. Should eventually update this to only delete the ones we are creating
        For Each xmlFile In XmlImportFiles
            If Path.GetExtension(xmlFile).ToLowerInvariant = ".xml" Then File.Delete(xmlFile)
        Next
        'If the CSV exists then create some graphics yo!  
        'Create Xml For ALL Objects.  This Function creates all the xml files, it Is Not just for one aaGraphic
        If File.Exists(fileStrg) Then txt_StatusError.Text = txt_StatusError.Text & vbCrLf & a2x.CreateGraphicsFromaaObjects(txt_aaGraphicTemplate.Text, cb_UpdateExistingGraphics.SelectedItem, UseSymbolReference:=True)
        btn_CreateGraphicFromSymbolReference.BackColor = Color.LawnGreen
    End Sub
    'Combines 400, 900
    Private Sub btn_aaGraphicRead_Import_Click(sender As Object, e As EventArgs) Handles btn_aaGraphicRead_Import.Click
        btn_aaGraphicRead_Import.BackColor = Color.PeachPuff
        'Create graphics from CSV
        Dim fileStrg As String = Path.Combine(a2x.StatusOutputFolder, lbl_CustPropsCSVFilename.Text)
        txt_StatusError.Text = txt_StatusError.Text & vbCrLf & a2x.GetCustomPropertiesAndOptionsFromCSV(fileStrg)
        'Create XML for ALL Objects.  This function creates all the xml files, it is not just for one aaGraphic
        If File.Exists(fileStrg) Then txt_StatusError.Text = txt_StatusError.Text & vbCrLf & a2x.CreateGraphicsFromaaObjects(txt_aaGraphicTemplate.Text, cb_UpdateExistingGraphics.SelectedItem, UseSymbolReference:=True)

        'Import into Archestra IDE
        txt_StatusError.Text = txt_StatusError.Text & vbCrLf & a2x.aaImport_Directory(a2x.GalaxyName)
        btn_aaGraphicRead_Import.BackColor = Color.LawnGreen
    End Sub

    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   900: Import - XML to IDE aaGraphics
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Private Sub btn_aaImport_Click(sender As Object, e As EventArgs) Handles btn_aaImport.Click
        btn_aaImport.BackColor = Color.PeachPuff
        If Not a2x.GalaxyLoggedIn Then btn_GalaxyLogin_Click(New Object, New EventArgs)
        'Get all existing graphics to verify overwrites
        If a2x.ExistingGraphicsList.Count = 0 Then a2x.ExistingGraphicsList = WWSP_Get_aaGraphicNames(a2x.GalaxyName)
        'Import a single graphic
        a2x.OverwriteFunction = 1 'No
        txt_StatusError.Text = txt_StatusError.Text & vbCrLf & a2x.aaImport(lbl_aaGraphicNameImport.Text, a2x.GalaxyName)
        btn_aaImport.BackColor = Color.LawnGreen
    End Sub

    'Do the above for an entire directory
    Private Sub btn_aaImport__Directory_Click(sender As Object, e As EventArgs) Handles btn_aaImport_Directory.Click
        btn_aaImport_Directory.BackColor = Color.PeachPuff
        If Not a2x.GalaxyLoggedIn Then btn_GalaxyLogin_Click(New Object, New EventArgs)
        'MODIFIED SO IT WILL MULTITHREAD and UDPATE UI
        'TODO - Apply this to the other versions of a2x.aaImport_Directory
        'txt_StatusError.Text = txt_StatusError.Text & vbCrLf & a2x.aaImport_Directory(a2x.GalaxyName)
        txt_StatusError.Text = ""
        lbl_ImportingItem.Visible = True
        'Get all existing graphics to verify overwrites
        If a2x.ExistingGraphicsList.Count = 0 Then a2x.ExistingGraphicsList = GetAllaaGraphicNames(a2x.GalaxyName)
        Try
            'Get every file in a directory and get animations from each
            a2x.OverwriteFunction = 0 'Initialized
            Dim files As String() = IO.Directory.GetFiles(a2x.xmlFolderImport, "*.xml")
            For Each file In files
                a2x.aaGraphicName = Path.GetFileNameWithoutExtension(file)
                lbl_ImportingItem.Text = a2x.aaGraphicName
                txt_StatusError.Text = txt_StatusError.Text & vbCrLf & a2x.aaImport(a2x.aaGraphicName, a2x.GalaxyName)
            Next
            lbl_ImportingItem.Visible = False
        Catch ex As Exception
            txt_StatusError.Text = ExResults(ex)
        End Try
        btn_aaImport_Directory.BackColor = Color.LawnGreen
    End Sub

    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   General
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Private Sub btn_OpenXML_Click(sender As Object, e As EventArgs)
        Process.Start(a2x.xmlfilename)
    End Sub

    Private Sub btn_Initiallize_Click(sender As Object, e As EventArgs) Handles btn_Initiallize.Click
        'Reinitialize everything
        BGW_Status = ""
        a2x.StatusResults = ""
        a2x = Nothing
        a2x = New Archestra2Xml
        Initialized = False
        txt_StatusError.Text = "a2x initialized"
    End Sub

    Private Sub btn_GetSymbols_Click(sender As Object, e As EventArgs) Handles btn_GetSymbols.Click
        If Not a2x.GalaxyLoggedIn Then btn_GalaxyLogin_Click(New Object, New EventArgs)
        Dim results = WWSP_GetMenuTree_FromIDE(a2x.aaGraphicToolset, a2x.GalaxyName)
        txt_StatusError.Text = String.Join(vbCrLf, results)
    End Sub




    'TODO - REMOVE AND REPLACE WITH THE EASIER METHOD
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   BGW - Background Worker on separate thread so that the UI can update
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Private Sub BGW_DoWork(ByVal sender As System.Object, ByVal e As DoWorkEventArgs) Handles BGW.DoWork
        Try
            BGW_Status = ""
            'Determine the calling sub
            Select Case BGW_Caller
                Case "btn_AnimFromXml_Directory"
                    'Get every file in a directory and get animations from each
                    Dim files As String() = IO.Directory.GetFiles(a2x.xmlFolderExport, "*.xml")
                    For Each file In files
                        If file.EndsWith(".xml") Then
                            a2x.aaGraphicName = file.Replace(".xml", "")
                            a2x.InitializeXmlDoc()
                            'Parse the XML file and return status/error results
                            BGW_Status = BGW_Status & a2x.GetAnimationsFromXml(file)
                            'Write UI results to csv 
                            Dim CsvList As List(Of String) = BGW_Status.Split(vbCrLf).ToList
                            Dim aaG = IO.Path.GetFileName(a2x.xmlfilename)
                            Dim CsvFilename = a2x.StatusOutputFolder & "\StatusResults\200_" & aaG & ".csv"
                            IO.File.WriteAllLines(CsvFilename, CsvList)
                        End If
                    Next
                    Dim FoundText As New List(Of String)(a2x.FoundTextList)
                    'Save objects as CSV
                    a2x.WriteAnimationsToCsv()
                    btn_AnimFromXml_Directory.BackColor = Color.LawnGreen
                Case Else

            End Select
        Catch ex As Exception
            BGW_Status = "HALTED at " & a2x.aaGraphicName & ".xml" & vbCrLf & ">> " & ex.Message
        End Try
    End Sub
    'BGW Completed Handler
    Private Sub BGW_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles BGW.RunWorkerCompleted
        'Update UI
        btn_Cancel_BGW.Visible = False
        txt_StatusError.Text = BGW_Status
        txt_StatusError.SelectionStart = 0
        txt_StatusError.ScrollToCaret()
    End Sub
    'Cancel Button
    Private Sub btn_Cancel_BGW_Click(sender As Object, e As EventArgs)
        BGW.CancelAsync()
        btn_Cancel_BGW.Visible = False
        txt_StatusError.Text = BGW_Status
    End Sub
    'Update Form UI every 100 msec (INDEPENDENT of the BGW) - I like to use this instead of BGW_ProgressChanged because its more flexible
    Private Sub TmrNow_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrNow.Tick
        'Update progress if BGW is working
        If BGW.IsBusy Then
            txt_StatusError.Text = a2x.StatusResults 'Update StatusResults early with individual status.  It will update with full resultswhen the BGW is complete.
            txt_aaGraphicName.Text = IO.Path.GetFileName(a2x.aaGraphicName)
        End If
    End Sub

    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   Events
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Private Sub txt_Galaxy_TextChanged(sender As Object, e As EventArgs) Handles txt_Galaxy.TextChanged
        If Not IsNothing(a2x) Then a2x.GalaxyName = txt_Galaxy.Text
    End Sub

    Private Sub txt_ToolsetName_TextChanged(sender As Object, e As EventArgs) Handles txt_ToolsetName.TextChanged
        If Not IsNothing(a2x) Then a2x.aaGraphicToolset = txt_ToolsetName.Text
        txt_ToolsetFolderName.Text = txt_ToolsetName.Text
    End Sub

    Private Sub txt_ToolsetFolderName_TextChanged(sender As Object, e As EventArgs) Handles txt_ToolsetFolderName.TextChanged
        If Not IsNothing(a2x) Then a2x.aaGraphicToolset = txt_ToolsetFolderName.Text
        txt_ToolsetName.Text = txt_ToolsetFolderName.Text
    End Sub

    Private Sub txt_aaGraphicName_TextChanged(sender As Object, e As EventArgs) Handles txt_aaGraphicName.TextChanged
        lbl_XMLgraphic.Text = txt_aaGraphicName.Text & ".xml"
        lbl_aaGraphicNameImport.Text = "NEW_" & txt_aaGraphicName.Text
    End Sub

    Private Sub txt_ExportDir_TextChanged(sender As Object, e As EventArgs) Handles txt_ExportDir.TextChanged
        Try
            a2x.xmlFolderExport = txt_ExportDir.Text
        Catch ex As Exception
            'Ignore if the a2x hasn't been instantiated yet
        End Try
    End Sub

    Private Sub txt_ImportDir_TextChanged(sender As Object, e As EventArgs) Handles txt_ImportDir.TextChanged
        Try
            a2x.xmlFolderImport = txt_ImportDir.Text
        Catch ex As Exception
            'Ignore if the a2x hasn't been instantiated yet
        End Try
    End Sub

    Private Sub txt_TemplatesDir_TextChanged(sender As Object, e As EventArgs) Handles txt_TemplatesDir.TextChanged
        Try

            a2x.xmlFolderTemplates = txt_TemplatesDir.Text
        Catch ex As Exception
            'Ignore if the a2x hasn't been instantiated yet
        End Try
    End Sub

    Private Sub tb_CustPropsCSVDir_TextChanged(sender As Object, e As EventArgs) Handles tb_CustPropsCSVDir.TextChanged
        Try
            a2x.StatusOutputFolder = tb_CustPropsCSVDir.Text
        Catch ex As Exception
            'Ignore if the a2x hasn't been instantiated yet
        End Try
    End Sub

    Private Sub txt_StatusError_TextChanged(sender As Object, e As EventArgs) Handles txt_StatusError.TextChanged

    End Sub

    Private Sub chk_ExAdvanced_CheckedChanged(sender As Object, e As EventArgs) Handles chk_ExAdvanced.CheckedChanged
        ShowAdvancedExResults = chk_ExAdvanced.Checked
    End Sub

    Private Sub cbo_aaGraphicTemplate_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbo_aaGraphicTemplate.SelectedIndexChanged
        If cbo_aaGraphicTemplate.SelectedItem.ToString = "T2_Template_For_Importer" Then
            chk_T4sidebar.Checked = False
        Else
            chk_T4sidebar.Checked = True
        End If
        txt_aaGraphicTemplate.Text = cbo_aaGraphicTemplate.SelectedItem.ToString
        a2x.aaTemplate = txt_aaGraphicTemplate.Text
        a2x.aaXML.aaTemplate = txt_aaGraphicTemplate.Text 'TODO - TEMPORARY UNTIL THE PUBLIC VARIABLES ARE CLEANED UP
    End Sub
    Private Sub txt_aaGraphicTemplate_TextChanged(sender As Object, e As EventArgs) Handles txt_aaGraphicTemplate.TextChanged
        a2x.aaTemplate = txt_aaGraphicTemplate.Text
        a2x.aaXML.aaTemplate = txt_aaGraphicTemplate.Text 'TODO - TEMPORARY UNTIL THE PUBLIC VARIABLES ARE CLEANED UP
    End Sub

    Private Sub chk_T4sidebar_CheckedChanged(sender As Object, e As EventArgs) Handles chk_T4sidebar.CheckedChanged
        If Not IsNothing(a2x) Then a2x.ExportCsvAsT4sidebar = chk_T4sidebar.Checked
    End Sub

#Region "FTViewSE"
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   1200: Convert FTViewSE graphic elements to InTouch elements
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '1200: Part 1 of 2
    Private Sub btn_AnimFromXml_FTViewSE_Click(sender As Object, e As EventArgs) Handles btn_AnimFromXml_FTViewSE.Click
        btn_AnimFromXml_FTViewSE.BackColor = Color.PeachPuff
        'Initialize the xDoc
        If Not Initialized Then
            a2x.aaGraphicName = txt_aaGraphicName.Text
            Initialized = True
        End If
        'Dim MethodCall As String = NameOf(Archestra2Xml.InitializeXmlDoc)
        'Parse the XML file
        txt_StatusError.Text = a2x.GetAnimationsFromXml_FTViewSE(Path.Combine(txt_ExportDir_FTViewSE.Text, txt_aaGraphicName_FTViewSE.Text & ".xml"))
        'Save objects as CSV
        a2x.WriteFTViewToCsv()
        'a2x.WriteAnimationsToCsv()
        btn_AnimFromXml_FTViewSE.BackColor = Color.LawnGreen
    End Sub
    '1200B: Entire Directory
    Private Sub btn_AnimFromXml_Directory_FTViewSE_Click(sender As Object, e As EventArgs) Handles btn_AnimFromXml_Directory_FTViewSE.Click
        btn_AnimFromXml_Directory_FTViewSE.BackColor = Color.PeachPuff
        Dim Files = Directory.GetFiles(txt_ExportDir_FTViewSE.Text, "*.xml")
        For Each file In Files
            txt_StatusError.Text = a2x.GetAnimationsFromXml_FTViewSE(file)
        Next
        'Save objects as CSV
        a2x.WriteAnimationsToCsv()
        Initialized = True
        btn_AnimFromXml_Directory_FTViewSE.BackColor = Color.LawnGreen
    End Sub

    '1210: Part 2 of 2 (requires 200:Part1 since that sub fills the aaObjects list)
    Private Sub btn_CreateGraphicfromObjectNames_FTViewSE_Click(sender As Object, e As EventArgs) Handles btn_CreateGraphicfromObjectNames_FTViewSE.Click
        btn_CreateGraphicfromObjectNames_FTViewSE.BackColor = Color.PeachPuff
        txt_StatusError.Text = txt_StatusError.Text & vbCrLf & a2x.CreateGraphicsFromaaObjects_FTViewSE(txt_aaGraphicTemplate.Text, cb_UpdateExistingGraphics.SelectedItem)
        btn_CreateGraphicfromObjectNames_FTViewSE.BackColor = Color.LawnGreen
    End Sub
#End Region

#Region "aaGroups"
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   2000: Convert FTViewSE graphic elements to InTouch elements
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Private Sub btn_GroupsFromXML_Click(sender As Object, e As EventArgs) Handles btn_GroupsFromXML.Click
        btn_GroupsFromXML.BackColor = Color.PeachPuff
        'Initialize the xDoc
        'If Not Initialized Then
        '    a2x.aaGraphicName = txt_aaGraphicName_Group.Text
        '    Initialized = True
        'End If
        'Parse the XML file
        a2x.aaXML.aaGroups.Clear()
        txt_StatusError.Text = a2x.GetGroupsFromXL(Path.Combine(txt_ExportDir_Group.Text, txt_aaGraphicName_Group.Text))
        a2x.WriteGroupsToCSV()
        btn_GroupsFromXML.BackColor = Color.LawnGreen
    End Sub

    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   2000B: Convert FTViewSE graphic elements to InTouch elements
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Private Sub btn_GroupFromXML_Directory_Click(sender As Object, e As EventArgs) Handles btn_GroupFromXML_Directory.Click
        btn_GroupFromXML_Directory.BackColor = Color.PeachPuff
        'Initialize the xDoc
        'If Not Initialized Then
        '    a2x.aaGraphicName = txt_aaGraphicName.Text
        '    Initialized = True
        'End If
        a2x.aaXML.aaGroups.Clear()
        'Dim MethodCall As String = NameOf(Archestra2Xml.InitializeXmlDoc)
        Dim files = Directory.GetFiles(txt_ExportDir_Group.Text, "*.xml")
        For Each file In files
            'Parse the XML file
            txt_StatusError.Text = txt_StatusError.Text & vbCrLf & a2x.GetGroupsFromXL(file)
        Next
        'Save objects as CSV
        a2x.WriteGroupsToCSV()
        btn_GroupFromXML_Directory.BackColor = Color.LawnGreen
    End Sub


    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   2010: Convert FTViewSE graphic elements to InTouch elements
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '2010: Part 1 of 2
    Private Sub btn_OpenGroupCSVXML_Click(sender As Object, e As EventArgs) Handles btn_OpenGroupCSVXML.Click
        Dim ofd As New OpenFileDialog
        ofd.Multiselect = False
        Dim result = ofd.ShowDialog()
        If result = DialogResult.OK Then
            Dim file As String = ofd.FileName
            lbl_GroupsCSVFileName.Text = Path.GetFileName(file)
            tb_GroupsCSVDir.Text = Path.GetDirectoryName(file)
            a2x.StatusOutputFolder = Path.GetDirectoryName(file)
        End If
    End Sub

    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   2010: Convert FTViewSE graphic elements to InTouch elements
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '2010: Part 2 of 2
    Private Sub btn_UpdateGroupsFromCSV_Click(sender As Object, e As EventArgs) Handles btn_UpdateGroupsFromCSV.Click
        btn_UpdateGroupsFromCSV.BackColor = Color.PeachPuff
        'Parse the XML file

        txt_StatusError.Text = a2x.GetGroupsFromCSV(Path.Combine(tb_GroupsCSVDir.Text, lbl_GroupsCSVFileName.Text))
        If txt_StatusError.Text.Substring(0, 5) = "ERROR" Then Exit Sub
        txt_StatusError.Text = a2x.UpdateGroupNames()
        btn_UpdateGroupsFromCSV.BackColor = Color.LawnGreen
    End Sub
#End Region

    Private Sub btn_OpenCSVXML_Click(sender As Object, e As EventArgs) Handles btn_OpenCSVXML.Click
        Dim ofd As New OpenFileDialog
        ofd.Multiselect = False
        Dim result = ofd.ShowDialog()
        If result = DialogResult.OK Then
            Dim file As String = ofd.FileName
            lbl_CustPropsCSVFilename.Text = Path.GetFileName(file)
            a2x.StatusOutputFolder = Path.GetDirectoryName(file)
        End If
    End Sub

    Private Sub cb_UpdateExistingGraphics_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cb_UpdateExistingGraphics.SelectedIndexChanged

    End Sub


    Private Class DASnode
        Public Property Name As String
        Public Property HostName As String
        Public Property ProcessorType As String
    End Class
    Private Sub btn_GetDASnodes_Click(sender As Object, e As EventArgs) Handles btn_GetDASnodes.Click
        Dim ofd As New OpenFileDialog
        ofd.Multiselect = False
        Dim result = ofd.ShowDialog()
        Dim file As String = ""
        Dim dir As String = ""
        If result = DialogResult.OK Then
            file = ofd.FileName
            Dim fileonly = Path.GetFileName(file)
            dir = Path.GetDirectoryName(file)
        End If
        Dim xmlDocToParse As New XDocument
        Try
            xmlDocToParse = XDocument.Load(file)
            Dim base_GraphicElements As XElement = xmlDocToParse.Root
            Dim xmlRoot = base_GraphicElements.Elements("DeviceNode").Elements("DeviceNode").ToList   '.Element("DASConfiguration").Elements("DeviceNode").ToList
            Dim NodeList As New List(Of DASnode)
            For Each node In xmlRoot
                Dim x As New DASnode
                Try
                    x.Name = node.Attribute("NAME").Value
                Catch ex As Exception
                    Dim fu1 = 1
                End Try
                Try
                    x.HostName = node.Element("HostName").Value
                    x.ProcessorType = node.Element("ProcessorType").Value
                Catch ex As Exception
                    Try
                        x.HostName = node.Element("IPAddress").Value
                        x.ProcessorType = node.Attribute("TYPE").Value
                    Catch ex2 As Exception
                        Dim fu2 = 2
                    End Try
                End Try
                NodeList.Add(x)
            Next
            Dim csvfilename As String = file.Replace(".xml", ".csv")
            If csvfilename.EndsWith(".csv") Then
                Dim csvList As New List(Of String)
                For Each x In NodeList
                    csvList.Add(x.Name & "," & x.HostName & "," & x.ProcessorType)
                Next
                IO.File.WriteAllLines(csvfilename, csvList)
                Process.Start(csvfilename)
            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try


    End Sub


    Private Class aapkgInstance
        Public Property template As String
        Public Property instance As String
        Public Property toolset As String
    End Class
    Private Sub btn_GetAApkgInstances_Click(sender As Object, e As EventArgs) Handles btn_GetAApkgInstances.Click
        Dim ofd As New OpenFileDialog
        ofd.Multiselect = False
        Dim result = ofd.ShowDialog()
        Dim file As String = ""
        Dim dir As String = ""
        If result = DialogResult.OK Then
            file = ofd.FileName
            Dim fileonly = Path.GetFileName(file)
            dir = Path.GetDirectoryName(file)
        End If
        Dim xmlDocToParse As New XDocument
        Try
            xmlDocToParse = XDocument.Load(file)
            Dim base_GraphicElements As XElement = xmlDocToParse.Root
            Dim NodeList As New List(Of aapkgInstance)
            'Get object count
            Dim TotalObjectCount = base_GraphicElements.Element("TotalObjectCount").Attribute("objectcount").Value
            NodeList.Add(New aapkgInstance With {.template = "Total Object Count", .instance = TotalObjectCount, .toolset = ""})
            'Get Instances
            Dim xmlTemplates = base_GraphicElements.Elements("template").ToList
            For Each t In xmlTemplates
                Dim xmlInstances = t.Element("derived_instances").Elements("instance").ToList
                For Each node In xmlInstances
                    Dim x As New aapkgInstance
                    'Template from outer loop
                    Try
                        x.template = t.Attribute("tag_name").Value
                    Catch ex As Exception
                        x.template = ex.Message
                    End Try
                    'Instance and Toolset from inner loop
                    Try
                        x.instance = node.Attribute("tag_name").Value
                    Catch ex As Exception
                        x.instance = ex.Message
                    End Try
                    Try
                        x.toolset = node.Attribute("toolset_name").Value
                    Catch ex As Exception
                        x.toolset = ex.Message
                    End Try
                    NodeList.Add(x)
                Next
            Next
            Dim SortedNodeList = NodeList.OrderBy(Function(s) s.toolset).ThenBy(Function(t) t.template).ThenBy(Function(u) u.instance)
            Dim csvfilename As String = file.Replace(".xml", ".csv")
            If csvfilename.EndsWith(".csv") Then
                Dim csvList As New List(Of String)
                For Each x In SortedNodeList
                    csvList.Add(x.toolset & "," & x.template & "," & x.instance)
                Next
                IO.File.WriteAllLines(csvfilename, csvList)
                Process.Start(csvfilename)
            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub





    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   Galaxy Dump/Load Csv
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    Private Sub btn_GalaxyDumpConvert_Click(sender As Object, e As EventArgs) Handles btn_GalaxyDumpConvert.Click
        Dim GalCsv As New GalaxyCsvLoadDump
        GalCsv.GalaxyDumpCsv = txt_GalaxyDumpFilename.Text
        GalCsv.ProcessGalaxyDump()
    End Sub

    Private Sub txt_GalaxyDumpFilename_DoubleClick(sender As Object, e As EventArgs) Handles txt_GalaxyDumpFilename.DoubleClick
        Dim ofd As New OpenFileDialog With {.Multiselect = False}
        If ofd.ShowDialog() = DialogResult.OK Then txt_GalaxyDumpFilename.Text = ofd.FileName
    End Sub

    Private Sub btn_XMLtoCsv_Click(sender As Object, e As EventArgs) Handles btn_XMLtoCsv.Click
        Dim X2C As New XMLtoFlatCsv
        Dim ofd As New OpenFileDialog With {.Multiselect = False}
        If ofd.ShowDialog() = DialogResult.OK Then X2C.ConvertXMLtoCSV(ofd.FileName)
    End Sub

    Private Sub lbl_aaGraphicNameImport_Click(sender As Object, e As EventArgs) Handles lbl_aaGraphicNameImport.Click

    End Sub






    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    '   Multithreading
    '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

#Region "EasyMultithreading"


    'Dim tasks As New List(Of Archestra2Xml)

    'Private Sub AddNewTaskThread(ByVal MethodToCall As String)
    '    'Construct Thread 1 (You must do this in the UI form)
    '    Try
    '        Dim ThisThreadWorker As Archestra2Xml = tasks.Where(Function(x) x.thread.ID = MethodToCall).FirstOrDefault
    '        If Not IsNothing(ThisThreadWorker) Then
    '            tasks.Remove(ThisThreadWorker)
    '        End If
    '        tasks.Add(New Archestra2Xml())
    '        tasks.Last.thread = New ThreadCallBack(Me, AddressOf tasks.Last.DoWork, AddressOf UpdateStatusBox)
    '        tasks.Last.thread.ID = MethodToCall
    '        tasks.Last.thread.Start()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.ToString)
    '    End Try
    'End Sub

    'Private Sub DisposeTaskThread(ByVal MethodToCall As String)
    '    Try
    '        'Dispose to abort the thread
    '        For Each t As Archestra2Xml In tasks
    '            If t.thread.ID = MethodToCall Then
    '                If t.thread IsNot Nothing Then t.thread.Dispose()
    '                tasks.Remove(t)
    '                Exit For
    '            End If
    '        Next
    '    Catch ex As Exception
    '        MessageBox.Show(ex.ToString)
    '    End Try
    'End Sub

    'Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.VisibleChanged
    '    'Dispose will Abort the threads if they are still running
    '    For Each t As Archestra2Xml In tasks
    '        If t.thread IsNot Nothing Then t.thread.Dispose()
    '    Next
    'End Sub


#End Region



End Class
