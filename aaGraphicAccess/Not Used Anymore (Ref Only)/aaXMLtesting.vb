﻿Imports System
Imports ArchestrA.GRAccess
Imports ArchestrA.Visualization.GraphicAccess
Imports System.Xml.Linq
Imports System.Linq
Imports System.Collections.Generic
Imports System.IO
Imports System.Windows.Forms
Imports System.Xml.Serialization

Public Class aaXMLtesting


    Public Function GetAllAnimationValues_CADEN(xSourceElem As XElement, xAttrName As String, xPrefix As String) As List(Of String)
        Try
            'First, get the animation elements
            Dim xAnimations As New List(Of XElement)
            xAnimations.AddRange(xSourceElem.Descendants.Where(Function(x) Not IsNothing(x.Attribute("Expression"))).ToList)

            'There is a lot going on here
            '1. If statement checks for either the minimum or maximum element.
            '2. If either of those are selected we have to go farther back into the parent elements to get what we want
            Dim IsThisWhatYouAreLookingFor = xAnimations.Select(Function(x) If((x.Name.LocalName.ToString.ToLowerInvariant = "maximum") Or (x.Name.ToString.ToLowerInvariant = "minimum"), xSourceElem.Attribute(xAttrName).Value.ToString & " | Type: " & x.Parent.Parent.Parent.Parent.Name.ToString & "." & x.Parent.Parent.Parent.Name.ToString & "." & x.Parent.Parent.Name.ToString & "." & x.Parent.Name.ToString & "." & x.Name.ToString & " | Exp:" & x.Attribute("Expression").Value.ToString, xSourceElem.Attribute(xAttrName).Value.ToString & " | Type: " & x.Parent.Parent.Name.ToString & "." & x.Parent.Name.ToString & "." & x.Name.ToString & " | Exp:" & x.Attribute("Expression").Value.ToString)).ToList


            'This looks more complicated than it needs to be, and we may hit new animations in the future that don't work.  I just need anything that is under an element named "Animations".

            'Then, get the animations as strings
            Dim aaAnimationValues As New List(Of String)
            If Not IsNothing(xAnimations) Then
                'Get all the animation expressions as list of string in the format "aaObject|aaAnimation"
                aaAnimationValues.AddRange(xAnimations.Select(Function(x) {xSourceElem.Attribute(xAttrName).Value.ToString & " | Type: " & x.Attribute("Expression").Parent.Name.ToString & " | Exp:" & x.Attribute("Expression").Value.ToString}.FirstOrDefault).ToList)
            Else

            End If

            'Return the list of animations as string
            Return aaAnimationValues
        Catch ex As Exception
            MessageBox.Show(ExResults(ex))
            Return Nothing
        End Try
    End Function

    Public Function ParseXML_OLD(aaGraphicName As String, xmlfoldername As String, xmlfilename As String) As String
        Dim StatusResults = "ParseXML"
        Try
            Dim aaXML As New aaXMLhelper
            'Set up the xml filename
            xmlfilename = xmlfoldername & aaGraphicName & ".xml"
            'Read the entire xml file
            Dim xmlDoc As XDocument = XDocument.Load(xmlfilename)

            'Get all root attributes as a dictionary
            aaXML.GetRootAttributesAsDictionary(xmlDoc)

            'Get the Last Used ElementID so that when you add new elements you can assign them a number
            Dim LastUsedId As Integer = xmlDoc.Root.Attribute("LastUsedId")

            'Get these as primary elements
            Dim base_GraphicElements As XElement = xmlDoc.Root.Element("GraphicElements")
            Dim base_CustomProperties As XElement = xmlDoc.Root.Element("CustomProperties")
            Dim base_NamedScripts As XElement = xmlDoc.Root.Element("NamedScripts")
            Dim base_PredefinedScripts As XElement = xmlDoc.Root.Element("PredefinedScripts")


            'Find the target element (Group) by the attribute called "Name"
            Dim xAttrName = "Name"
            Dim xElemName = "Tank1"
            Dim xaaGroup As XElement = aaXML.GetFirstElementByAttrName(base_GraphicElements, xAttrName, xElemName)

            'Duplicate a symbol
            xAttrName = "Name"
            xElemName = "TankLevel"
            Dim xNewElemName = "TankLevel2"
            'aaXML.DuplicateSymbol(xaaGroup, xAttrName, xElemName, xNewElemName)

            'Get Lists of all available elements in specific areas
            Dim listofdescendants = base_GraphicElements.Descendants.ToList
            Dim listofelements = base_GraphicElements.Elements.ToList
            Dim listofattributes = base_GraphicElements.Attributes.ToList


            'Get a list of all custom properties
            Dim aaItemCP_List As List(Of CustomProperty) = aaXML.GetAllCustomProperties(xaaGroup.Element("CustomProperties"))
            Dim base_CP_List As List(Of CustomProperty) = aaXML.GetAllCustomProperties(xmlDoc.Root.Element("CustomProperties"))

            'Save as a new xmlDoc for importing
            xmlfilename = xmlfoldername & aaGraphicName & ".xml"
            xmlDoc.Save(xmlfilename)

            Return StatusResults

        Catch ex As Exception
            StatusResults = ExResults(ex)
            Return StatusResults
        End Try
    End Function





End Class

