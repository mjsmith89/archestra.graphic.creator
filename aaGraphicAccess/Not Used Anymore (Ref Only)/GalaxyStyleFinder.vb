﻿Imports System
Imports ArchestrA.GRAccess
Imports ArchestrA.Visualization.GraphicAccess
Imports System.Xml.Linq
Imports System.Linq
Imports System.Collections.Generic
Imports System.IO


Class GalaxytyleFinder
    <STAThread>
    Private Shared Sub Main(ByVal args As String())
        Try

            Dim gal As String = args(0)  'Galaxy name
            Dim user As String = args(1)  'username
            Dim pass As String = args(2)  'password
            Dim stylesFile As String = args(3)  'filename of your styles file
            Dim graphic As String = args(4)  'the graphic name you want to check
            Dim grAccess As GRAccessApp = New GRAccessAppClass()
            Dim gals As IGalaxies = grAccess.QueryGalaxies("localhost")

            If gals Is Nothing OrElse grAccess.CommandResult.Successful = False Then
                Console.WriteLine(grAccess.CommandResult.CustomMessage + grAccess.CommandResult.Text)
                Return
            End If


            Dim galaxy As IGalaxy = gals(gal)
            Dim cmd As ICommandResult

            galaxy.Login(user, pass)
            cmd = galaxy.CommandResult

            If Not cmd.Successful Then
                Console.WriteLine("Login to galaxy Failed :" & cmd.Text & " : " + cmd.CustomMessage)
                Return
            End If

            Dim ff As GraphicAccess = New GraphicAccess()
            Dim graphicFile = Path.GetTempFileName()

            ff.ExportGraphicToXml(galaxy, graphic, graphicFile)
            cmd = ff.CommandResult

            If Not cmd.Successful Then
                Console.WriteLine(String.Format("Error exporting {0}.  {1}", graphic, cmd.CustomMessage))
                Return
            End If

            galaxy.Logout()


            Dim xmlGraphic As XElement = XElement.Load(graphicFile)
            Dim ids As List(Of String) = New List(Of String)()

            Dim nodes = xmlGraphic.Descendants().Attributes("ElementStyleID").[Select](Function(x) x.Value).Distinct()
            ids.AddRange(nodes)
            nodes = xmlGraphic.Descendants().Elements("TrueElementStyle").[Select](Function(x) x.Value).Distinct()
            ids.AddRange(nodes)
            nodes = xmlGraphic.Descendants().Elements("FalseElementStyle").[Select](Function(x) x.Value).Distinct()
            ids.AddRange(nodes)
            nodes = xmlGraphic.Descendants().Elements("ElementStyle").Where(Function(x) x.HasElements = False).[Select](Function(x) x.Value).Distinct()
            ids.AddRange(nodes)

            Dim xmlStyles As XElement = XElement.Load(stylesFile)

            For Each id As String In ids.Distinct()
                Console.WriteLine(xmlStyles.Elements("ES").Where(Function(x) x.Attribute("Id").Value = id).[Select](Function(x) x.Attribute("Name").Value).FirstOrDefault())
            Next

        Catch ex As Exception
            Console.WriteLine(ExResults(ex))
        End Try
    End Sub
End Class


