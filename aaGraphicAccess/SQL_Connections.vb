﻿Imports System
Imports System.IO
Imports System.Data
Imports System.Text
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Collections.Concurrent

Module SQL_Connections

    Public sqlComputerName As String = "(local)"
    Public sqlDB As String = ""
    Public UseWindowsAuthentication As Boolean = True
    Public saUsername As String = "sa"
    Public saPassword As String = ""
    Public sqlConnectionString As String = ""



    Public Function Configure_sqlConnectionString(ByVal GalaxyName As String, Optional ByVal sqlComputerName As String = "", Optional ByVal sqlInstance As String = "") As Boolean
        Dim OldPrompt As String = "Enter the instance name" & vbCrLf & vbCrLf & "• Leave <blank> to use the default instance (MSSQLSERVER)" & vbCrLf & "• Enter SQLEXPRESS if you are using SQL Server Express" & vbCrLf & "• Otherwise, enter the custom instance name that was used during installation"

        Dim Success As Boolean = False
        Dim Tries As Integer = 0

        'Setup variables
        If sqlComputerName = "" Then sqlComputerName = "(local)"
        If sqlInstance = "" Then sqlInstance = "" 'Blank for MSSQLSERVER, SQLEXPRESS

        Dim sqlDataSource As String = ""
        If sqlInstance = "" Or sqlInstance = "MSSQLSERVER" Then
            sqlDataSource = sqlComputerName
        Else
            sqlDataSource = sqlComputerName & "\" & sqlInstance
        End If

TryAgain:
        Try

            'Construct the connection string so we can list the databases in SQL Server
            Dim connectstringBuilder As New SqlClient.SqlConnectionStringBuilder
            With connectstringBuilder
                .Clear()
                .ApplicationName = ".NET SqlClient Data Provider" 'default is '.NET SqlClient Data Provider'
                .DataSource = sqlDataSource
                If GalaxyName = "" Then
                    .InitialCatalog = "master" 'initial database. Set to master to list databases below
                Else
                    .InitialCatalog = GalaxyName
                End If

                If UseWindowsAuthentication Then
                    .IntegratedSecurity = True
                Else
                    .IntegratedSecurity = False
                    .UserID = saUsername
                    .Password = saPassword
                End If
            End With

            'create the connection string
            sqlConnectionString = connectstringBuilder.ConnectionString

        Catch ex As Exception
            MessageBox.Show(ExResults(ex))
        End Try

        'Test connection
        Success = TestSQLconnection(sqlConnectionString)

        If Not Success Then
            Tries = Tries + 1
            If UseWindowsAuthentication Then
                UseWindowsAuthentication = False
            Else
                UseWindowsAuthentication = True
            End If
            If Tries < 2 Then
                GoTo TryAgain
            Else
                MessageBox.Show("Both Windows authentication and SQL user authentication failed to connect.  Check your passwords and try again.  Other items to check include: " & vbCrLf &
                    "> If trying the use the 'sa' user, then that account must be enabled.  It is disabled by default on install." & vbCrLf &
                    "> Verify that the SQL server security is configured for Mixed Mode Authentication." & vbCrLf &
                    "> Verify that the SQL server is configured to accept remote connections." & vbCrLf &
                    "> Verify that SQL connection ports are not blocked on either the local or remote firewall." & vbCrLf &
                    "> Veriy that you can ping the IP address of the computer hosting the SQL server.")
            End If
        End If

        Return Success

    End Function

    Public Function SQL_Query_Return_Results_as_Dictionary(ByVal ConnectionString As String, ByVal QueryString As String) As Dictionary(Of String, String)

        Using connection As New SqlConnection(ConnectionString)

            Try
                Dim command As SqlCommand = New SqlCommand(QueryString, connection)
                Dim mySQLerrormsg As String = ""
                Dim mySQLsuccess As Boolean = VerifySQLConnection(connection, mySQLerrormsg)

                'Open SQL Connection if no error
                If mySQLsuccess = False Then
                    MessageBox.Show(mySQLerrormsg & vbCrLf & "This application only supports SQL Server Windows Authentication for security purposes.  If your Windows account does not have SQL server access, then contact your SQL administrator or add this Windows account to the trusted users.")
                    GoTo ExitEarly
                Else
                    connection.Open()
                End If

                Dim Column01 As String = ""
                Dim Column02 As String = ""
                Dim ReaderCount As Integer = 0
                Dim reader As SqlDataReader = command.ExecuteReader()
                'Retrieve the rows in the SQL table and add to a dictionary
                Dim SQLresults As New Dictionary(Of String, String)

                If reader.HasRows And reader.FieldCount > 0 Then
                    Dim FirstPass As Boolean = True
                    Do While reader.Read()
                        'Read the first column
                        Try
                            Column01 = reader.GetValue(0).ToString
                        Catch ex As Exception
                            'Ignore
                        End Try
                        'Read the second column
                        Try
                            Column02 = reader.GetValue(1).ToString
                        Catch ex As Exception
                            Column02 = "Column doesn't exist"
                        End Try
                        'Ignore duplicates
                        Try
                            SQLresults.Add(Column01, Column02)
                        Catch ex As Exception
                        End Try
                        'Count the rows in the SQL table 
                        ReaderCount = ReaderCount + 1
                    Loop

                Else
                    SQLresults.Add("SQL returned no results", "SQL returned no results")
                End If
                reader.Close()

                Return SQLresults

            Catch ex As Exception
                MessageBox.Show(ExResults(ex))
                GoTo ExitEarly
            End Try
ExitEarly:
        End Using
        Return Nothing

    End Function

    Public Class SQLResult
        Public Property Column1 As String
        Public Property Column2 As String
        Public Property Column3 As String
        Public Property Column4 As String
        Public Property Column5 As String
    End Class

    Public Function SQL_Query_Return_Rows(ByVal ConnectionString As String, ByVal QueryString As String, Optional ByVal ColumnCount As Integer = 2) As List(Of SQLResult)

        Using connection As New SqlConnection(ConnectionString)
            Try
                Dim command As SqlCommand = New SqlCommand(QueryString, connection)
                Dim mySQLerrormsg As String = ""
                Dim mySQLsuccess As Boolean = VerifySQLConnection(connection, mySQLerrormsg)

                'Open SQL Connection if no error
                If mySQLsuccess = False Then
                    MessageBox.Show(mySQLerrormsg & vbCrLf & "This application only supports SQL Server Windows Authentication for security purposes.  If your Windows account does not have SQL server access, then contact your SQL administrator or add this Windows account to the trusted users.")
                    GoTo ExitEarly
                Else
                    connection.Open()
                End If

                Dim SQLrow As New SQLResult
                Dim AllRows As New List(Of SQLResult)
                Dim ReaderCount As Integer = 0
                Dim reader As SqlDataReader = command.ExecuteReader()
                'Retrieve the rows in the SQL table and add to the list
                If reader.HasRows And reader.FieldCount > 0 Then
                    Dim FirstPass As Boolean = True
                    Do While reader.Read()
                        'Read the first column
                        Try
                            SQLrow.Column1 = reader.GetValue(0).ToString
                        Catch ex As Exception
                            SQLrow.Column1 = ex.Message
                        End Try
                        'Read the second column
                        If ColumnCount > 1 Then
                            Try
                                SQLrow.Column2 = reader.GetValue(1).ToString
                            Catch ex As Exception
                                SQLrow.Column2 = ex.Message
                            End Try
                        End If
                        'Read the third column
                        If ColumnCount > 2 Then
                            Try
                                SQLrow.Column3 = reader.GetValue(2).ToString
                            Catch ex As Exception
                                SQLrow.Column3 = ex.Message
                            End Try
                        End If
                        'Read the fourth column
                        If ColumnCount > 3 Then
                            Try
                                SQLrow.Column4 = reader.GetValue(3).ToString
                            Catch ex As Exception
                                SQLrow.Column4 = ex.Message
                            End Try
                        End If
                        'Read the fifth column
                        If ColumnCount > 4 Then
                            Try
                                SQLrow.Column5 = reader.GetValue(4).ToString
                            Catch ex As Exception
                                SQLrow.Column5 = ex.Message
                            End Try
                        End If
                        AllRows.Add(SQLrow)
                        'Count the rows in the SQL table 
                        ReaderCount = ReaderCount + 1
                    Loop

                Else
                    AllRows.Add(New SQLResult With {.Column1 = "SQL returned no results"})
                End If
                reader.Close()

                Return AllRows

            Catch ex As Exception
                MessageBox.Show(ExResults(ex))
                GoTo ExitEarly
            End Try
ExitEarly:
        End Using
        Return Nothing

    End Function

    Public Sub GetAvailablesqlDB()

        '    Dim databaseNames As New List(Of String)

        '    'list the databases that exist in SQL Server
        '    Try
        '        Dim cmdText As String = ("select * from master.dbo.sysdatabases")

        '        Using sqlConnection As SqlClient.SqlConnection = New SqlClient.SqlConnection( s.UserSettings.sqlConnectionString)

        '            'open connection to SQL Server
        '            sqlConnection.Open()

        '            'query for databases
        '            Using sqlCmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(cmdText, sqlConnection)

        '                Using reader As SqlClient.SqlDataReader = sqlCmd.ExecuteReader
        '                    While reader.Read
        '                        Dim oneDB As String = reader(0)
        '                        Select Case oneDB
        '                            Case "master", "tempdb", "model", "msdb", "Runtime", "Holding", "A2ALMDB", "WWALMDB", "WWALMDB2"
        '                                'Exclude
        '                            Case Else
        '                                databaseNames.Add(oneDB)
        '                        End Select
        '                    End While
        '                End Using

        '            End Using

        '            sqlConnection.Close()

        '        End Using

        '        'Clear dictionary
        '        If sqlDB_Dict.Count > 0 Then sqlDB_Dict.Clear()
        '        For i As Integer = 0 To databaseNames.Count - 1
        '            'Add Databases to dictionary
        '            If sqlDB_Dict.TryAdd(databaseNames(i), databaseNames(i)) = False Then
        '                ErrorOccurred = True
        '                MainWindow.Status = ("Can't add " & databaseNames(i) & " to selection box.")
        '            Else
        '                'Update status
        '                MainWindow.Status = databaseNames(i) & " was found."
        '            End If
        '        Next

        '    Catch ex As Exception
        '        CustomErrMsg = "Error while trying to collect databases."
        '        MainWindow.Status = ReportExceptionInfo(ex, CustomErrMsg)
        '    End Try

    End Sub

    Public Function TestSQLconnection(ByVal connstring As String) As Boolean

        Using sqlConnection As SqlClient.SqlConnection = New SqlClient.SqlConnection(connstring)
            Try
                sqlConnection.Open()
                Return True
            Catch ex As SqlException
                MessageBox.Show(("SQL Connection Failed" & vbCrLf & ExResults(ex)))
                Return False
            End Try

        End Using

    End Function

    'Return error message if the connection isn't successful
    Public Function VerifySQLConnection(ByVal connection As SqlConnection, Optional ByRef StatusText As String = "") As Boolean
        Try
            connection.Open()
            If connection.State Then
                StatusText = "Connection succeeded to " & connection.DataSource & " " & connection.Database & ", SQL version " & connection.ServerVersion
                Return True
            Else
                StatusText = "Connection failed to " & connection.DataSource & " " & connection.Database & ", SQL version " & connection.ServerVersion
                Return False
            End If
        Catch myerror As SqlException
            Return False
            StatusText = "Connection failed as <" & myerror.Message & ">" & vbCrLf
        Catch ex As Exception
            MessageBox.Show(ExResults(ex))
            Return False
        Finally
            connection.Close()
        End Try
    End Function

    Public Sub Set_Connection_Manually(ByRef SqlConnString As String)
        Dim MyInput As String = SqlConnString
        MyInput = InputBox("Enter the connection string", "Manual connection string entry", SqlConnString)
        ' If user has clicked Cancel, set myValue to defaultValue 
        If MyInput Is "" Then
            MyInput = SqlConnString
        End If
    End Sub


End Module
