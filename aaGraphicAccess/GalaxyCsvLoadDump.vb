﻿Imports System
Imports ArchestrA.GRAccess
Imports ArchestrA.Visualization.GraphicAccess
Imports System.Xml.Linq
Imports System.Linq
Imports System.Collections.Generic
Imports System.IO
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports System.Reflection
Imports System.ComponentModel
Imports System.Text
Imports System.Runtime.CompilerServices

Public Class GalaxyCsvLoadDump

    Public Sub New()

    End Sub

    Public GalaxyDumpCsv As String = "C:\Files\GalaxyDump.csv"
    Public GalaxyInstances As New List(Of gInstance)

    Public Sub ProcessGalaxyDump()

        Dim CsvLines = IO.File.ReadAllLines(GalaxyDumpCsv)

        Dim gI As New gInstance
        Dim gTemplate As String = ""
        Dim HeaderLine As New List(Of String)
        Dim ValuesLine As New List(Of String)

        For Each line In CsvLines
            Select Case True
                Case line.StartsWith(";")
                    'Ignore
                Case line.StartsWith(":TEMPLATE=")
                    gTemplate = line.Split(",")(0)
                Case line.StartsWith(":Tagname")
                    HeaderLine = line.Split(",").ToList
                Case line.Trim = ""
                    'Ignore
                Case Else
                    'Tag Lines
                    ValuesLine = line.Split(",").ToList
                    gI = New gInstance
                    gI.Template = gTemplate
                    gI.Instance = ValuesLine(0)
                    For col = 0 To HeaderLine.Count - 1
                        Dim gA As New gInstance.gAttrProp
                        Dim Header = HeaderLine(col)
                        Select Case Header
                            Case ":Tagname"

                            Case "Area"
                                gI.Area = ValuesLine(col)
                            Case "SecurityGroup"

                            Case "Container"

                            Case "ContainedName"

                            Case "FriendlyName"

                            Case "ShortDesc"

                            Case "ExecutionRelativeOrder"

                            Case "ExecutionRelatedObject"

                            Case "UDAs" 'This is the same no matter which Features are selected
                                gI._xUDAs = XElement.Parse(ValuesLine(col).Trim("""").Replace("""""", """"))
                            Case "Extensions" 'This shows the Features that are selected (there is no entry for features unchecked)
                                gI._xExtensions = XElement.Parse(ValuesLine(col).Trim("""").Replace("""""", """"))
                            Case "CmdData" 'There is an entry here for Boolean, but not sure if it is needed
                                gI._xCmdData = XElement.Parse(ValuesLine(col).Trim("""").Replace("""""", """").Replace(" \n", ""))
                            Case "UserAttrData"

                            Case Else 'These are the Attributes and Attr.Properties
                                If Header.Contains(".") Then
                                    gA.Attr = Header.Split(".")(0)
                                    gA.Prop = Header.Split(".")(1)
                                Else
                                    gA.Attr = Header
                                    gA.Prop = "_InitialValue"
                                End If
                                gA.Value = ValuesLine(col)

                        End Select
                        If gA.Attr IsNot Nothing Then gI._AttrProps.Add(gA)
                    Next
                    'Extract the important UDAInfo from xml
                    Dim UDA_Names = gI._xUDAs.Elements.Select(Function(x) x.Attribute("Name").Value).ToList
                    Dim UDA_DTs = gI._xUDAs.Elements.Select(Function(x) x.Attribute("DataType").Value).ToList
                    For i = 0 To UDA_Names.Count - 1
                        Dim u As New gInstance.gAttribute
                        u.Name = UDA_Names(i)
                        u.DataType = UDA_DTs(i)
                        gI.Attributes.Add(u)
                    Next
                    'Extract the important ExtentionInfo from xml to the matching UDA
                    Dim ExtNames = gI._xExtensions.Element("AttributeExtension").Elements.Select(Function(x) x.Attribute("Name").Value).ToList
                    Dim ExtTypes = gI._xExtensions.Element("AttributeExtension").Elements.Select(Function(x) x.Attribute("ExtensionType").Value).ToList
                    Dim ExtInherits = gI._xExtensions.Element("AttributeExtension").Elements.Select(Function(x) x.Attribute("InheritedFromTagName").Value).ToList
                    For i = 0 To ExtNames.Count - 1
                        Dim ext As New gInstance.gAttrExtension
                        ext.ExtensionType = ExtTypes(i)
                        ext.InheritedFromTagName = ExtInherits(i)
                        For Each uda In gI.Attributes
                            If uda.Name = ExtNames(i) Then
                                uda.Extensions.Add(ext)
                            End If
                        Next
                    Next
                    'Add this Instance to the list
                    GalaxyInstances.Add(gI)

            End Select
        Next

        Dim stopit = 1
    End Sub

    'Generic types in a list
    Private Sub ProcessListWithoutKnowingTheMemberType(Of T)(ByVal list As List(Of T))

    End Sub

    Public Class gInstance
        Public Property Template As String
        Public Property Instance As String
        Public Property Area As String
        Public Property Attributes As New List(Of gAttribute)

        Public Property _AttrProps As New List(Of gAttrProp)
        Public Property _xUDAs As XElement
        Public Property _xExtensions As XElement
        Public Property _xCmdData As XElement


        'UDAs from csv XML - This is the Attribute list (use this as the base for others)
        Public Class gAttribute
            'From csv xml
            Public Property Name As String
            Public Property DataType As String
            Public Property InheritedFromTagName As String
            'Don't need the others from the XML
            Public Property Extensions As New List(Of gAttrExtension)

        End Class

        'Extensions from csv XML - "Features" selected in the Instance
        Public Class gAttrExtension
            Public Property ExtensionType As String 'Alarm, History, etc
            Public Property InheritedFromTagName As String 'Inherited from Template
            Public Property Properties As New List(Of gAttrExtension)
        End Class
        'Attribute.Property that comes in a separate column for each property
        Public Class gAttrProp
            Public Property Attr As String
            Public Property Prop As String
            Public Property Value As String
        End Class

        'BMS created for easier use
        Public Class Extention
            Public Property eBase As gAttrProp
            Public Property eAlarm As gAttrProp
            Public Property eBadvaluealarm As gAttrProp
            Public Property eBoolean As gAttrProp 'Statistics
            Public Property eAnalog As gAttrProp 'Statistics
            Public Property eHistory As gAttrProp
            Public Property eInput As gAttrProp
            Public Property eInputoutput As gAttrProp
            Public Property eLogdatachangeevent As gAttrProp
        End Class
    End Class


End Class
