﻿Imports System
Imports ArchestrA.GRAccess
Imports ArchestrA.Visualization.GraphicAccess
Imports System.Xml.Linq
Imports System.Linq
Imports System.Collections.Generic
Imports System.IO
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports System.Reflection
Imports System.ComponentModel
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Runtime.CompilerServices
Imports System.Diagnostics
Imports System.Threading

Public Class InTouchTag_to_GalaxyAttribute

    Public CsvOpenFilename As String = "C:\Files\Temp\TagConverterInput.csv"
    Public CsvSaveFilename As String = "C:\Files\Temp\TagConverterOutput.csv"

    Public ReportObjectsList As New List(Of ReportObject)

    'Define columns
    Private Property cDisplayName = 0 : Private Property cObjName = 3 : Private Property cObjType = 2 : Private Property cAnimType = 6
    Private Property cTag1Name = 25 : Private Property cTag2Name = 26 : Private Property cTag3Name = 27
    Private Property cTag4Name = 28 : Private Property cTag5Name = 29 : Private Property cTag6Name = 30
    Private Property cTag1Type = 32 : Private Property cTag1Desc = 33 : Private Property cTag2Type = 34 : Private Property cTag2Desc = 35
    Private Property cTag3Type = 36 : Private Property cTag3Desc = 37 : Private Property cTag4Type = 38 : Private Property cTag4Desc = 39
    Private Property cTag5Type = 40 : Private Property cTag5Desc = 41 : Private Property cTag6Type = 42 : Private Property cTag6Desc = 43

    Public Sub ConvertCsv()

        Dim csvRows = IO.File.ReadAllLines(CsvOpenFilename).ToList

        Dim ErrorRow As Integer = 0
        For Each row In csvRows
            ErrorRow = ErrorRow + 1
            Try
                'Ignore the header
                If csvRows.IndexOf(row) > 0 Then
                    Dim columns = row.Split(",")
                    Dim item = New ReportObject
                    item.DisplayName = columns(cDisplayName)
                    item.ObjectName = columns(cObjName)
                    item.ObjectType = columns(cObjType)
                    Dim tag = New ReportObject.Tag
                    tag.ITtagname = columns(cTag1Name)

                    'Modify all of these to blanks so they sort at the bottom
                    If item.ObjectName = "<N/A>" Then item.ObjectName = "z <OutsideGroup>"

                    If tag.ITtagname <> "" Then
                            'Does the object already exist in the list?
                            Dim ObjectInList = ReportObjectsList.Where(Function(x) x.ObjectName = item.ObjectName).FirstOrDefault
                            If Not IsNothing(ObjectInList) Then
                                'Pull out the existing Object from the list so you can make changes
                                item = ObjectInList
                            Else
                                'Add a new Object to the list
                                ReportObjectsList.Add(item)
                            End If 'Use linq to determine if x.ObjectName exists in the list.  Add if not, update if exists
                            'Update tag1
                            Call UpdateTagAnimation(tag.ITtagname, columns(cTag1Desc), columns(cTag1Type), columns(cAnimType), item)
                            'Update tag2
                            Call UpdateTagAnimation(columns(cTag2Name), columns(cTag2Desc), columns(cTag2Type), columns(cAnimType), item)
                            'Update tag3
                            Call UpdateTagAnimation(columns(cTag3Name), columns(cTag3Desc), columns(cTag3Type), columns(cAnimType), item)
                            'Update tag4
                            Call UpdateTagAnimation(columns(cTag4Name), columns(cTag4Desc), columns(cTag4Type), columns(cAnimType), item)
                            'Update tag5
                            Call UpdateTagAnimation(columns(cTag5Name), columns(cTag5Desc), columns(cTag5Type), columns(cAnimType), item)
                            'Update tag6
                            Call UpdateTagAnimation(columns(cTag6Name), columns(cTag6Desc), columns(cTag6Type), columns(cAnimType), item)
                        Else
                            'Do nothing since there is no tag in this cell
                        End If
                    End If
            Catch ex As Exception
                MessageBox.Show("Error on row " & ErrorRow & vbCrLf & vbCrLf & ex.Message)
            End Try
        Next

        'Loop back through the list to determine attribute type
        Dim ErrorTag As String = ""
        Dim a As New AttributeNames
        'Dim a As New AttributeNames (MOVED outside the sub so it can be used in a function)
        For Each item In ReportObjectsList
            For Each tag In item.Tags
                ErrorTag = item.ObjectName & " | " & tag.ITtagname
                'Skip oddball items and do them manually
                Dim TypeIsValid As Boolean = tag.ITtype.ToLowerInvariant = "bool" Or tag.ITtype.ToLowerInvariant.StartsWith("analog")
                If Not TypeIsValid Then
                    tag.ObjAttr = "<MANUALFIX - Not Bool/Analog>"
                    Continue For
                End If

                '━━━━━━ FUNCTION AddAttribute ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                'Function that checks to see if the new attribute already exists for this object before adding it
                Dim AddAttribute As Func(Of String, String) =
                    Function(NewAttr As String)
                        If item.Tags.Any(Function(x) x.ObjAttr = NewAttr) Then
                            'For SPECIAL CASES, add a new Attr if the first one exists
                            Select Case NewAttr
                                'CASE: Val2 when Val exists
                                Case a.AI_Basic.Val
                                    If item.Tags.Any(Function(x) x.ObjAttr = a.AI_Basic.Val2) Then
                                        'Val2 attribute has already been assigned
                                        Return NewAttr & "_<DUPLICATE>"
                                    Else
                                        'add as Val2 since Val exists
                                        Return a.AI_Basic.Val2
                                    End If
                                Case Else
                                    'Attribute has already been assigned, so add text 
                                    'so that we know too many were attempted
                                    Return NewAttr & "_<DUPLICATE>"
                            End Select
                        Else
                            'Assign attribute as new
                            Return NewAttr
                        End If
                    End Function
                '━━━━━━ END FUNCTION ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

                Try
                    Select Case item.ObjectType
                        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                        Case "#" & ObjectTypes.AI_Basic.ToString & "#",
                             "#" & ObjectTypes.AI_Tank.ToString & "#",
                             "#" & ObjectTypes.AI_Sidebar.ToString & "#"
                            If tag.ITtype = "bool" Then
                                'Discrete Tag Type
                                If tag.IT_AnimTypes.Any(Function(x) x.StartsWith("TextChangeColor")) Then
                                    tag.ObjAttr = AddAttribute(a.AI_Basic.Visible)
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("Visibility")) Then
                                    tag.ObjAttr = AddAttribute(a.AI_Basic.Visible)
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("Disable.Disc")) Then
                                    tag.ObjAttr = AddAttribute(a.AI_Basic.Visible)
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("ActionScript")) Then
                                    tag.ObjAttr = "<MANUALFIX - Create Separate Alarm>"
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("Blink")) Then
                                    tag.ObjAttr = "<MANUALFIX - Create Separate Alarm>"
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("PushButton")) Then
                                    tag.ObjAttr = "<MANUALFIX - Create Separate Alarm>"
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.EndsWith("Analog")) Then
                                    tag.ObjAttr = "<ERROR - Discrete Tag / Analog Anim>"
                                Else
                                    tag.ObjAttr = "<ERROR - Unexpected Animation>"
                                End If
                            Else
                                'Analog Tag Type
                                If tag.IT_AnimTypes.Any(Function(x) x.StartsWith("ValueDisplay")) Then
                                    tag.ObjAttr = AddAttribute(a.AI_Basic.Val)
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.EndsWith("TruthTable")) Then
                                    tag.ObjAttr = AddAttribute(a.AI_Basic.Selection)
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("Visibility.Disc")) Then
                                    tag.ObjAttr = AddAttribute(a.AI_Basic.Selection)
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("UserInput")) Then
                                    tag.ObjAttr = "<ERROR - UserInput>"
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("TextChangeColor")) Then
                                    tag.ObjAttr = "<ERROR - Unexpected Animation>"
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("Vertical")) Then
                                    If tag.ITtagname.ToLowerInvariant.Trim = "temp" Then
                                        tag.ObjAttr = "<IGNORE>"
                                    Else
                                        tag.ObjAttr = AddAttribute(a.AI_Basic.Val)
                                    End If
                                Else
                                    tag.ObjAttr = "<ERROR - Unexpected Animation>"
                                End If
                            End If

                        Case "#" & ObjectTypes.RTC.ToString & "#"
                            'Check the description since they will all be ValueDisplay.Analog
                            If tag.ITdescription.ToLowerInvariant.Contains("second") Then
                                tag.ObjAttr = AddAttribute(a.RTC.Sec)
                            ElseIf tag.ITdescription.ToLowerInvariant.Contains("minute") Then
                                tag.ObjAttr = AddAttribute(a.RTC.Min)
                            ElseIf tag.ITdescription.ToLowerInvariant.Contains("hour") Then
                                tag.ObjAttr = AddAttribute(a.RTC.Hr)
                            Else
                                tag.ObjAttr = "<MANUALFIX - No Hr/Min/Sec>"
                            End If

                        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                        Case "#" & ObjectTypes.Discrete_Alarm.ToString & "#"
                            If tag.IT_AnimTypes.Any(Function(x) x.StartsWith("ActionScript")) Then
                                tag.ObjAttr = "<ERROR - Should be #SetpointAlarm_DI#>"
                            ElseIf tag.IT_AnimTypes.Any(Function(x) x = "PushButton.Discrete") Then
                                tag.ObjAttr = "<ERROR - Should be #SetpointAlarm_DI#>"
                            ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("UserInput")) Then
                                tag.ObjAttr = "<ERROR - Should be #SetpointAlarm_DI#>"
                            Else
                                tag.ObjAttr = AddAttribute(a.Discrete_Alarm.DI)
                            End If

                        Case "#" & ObjectTypes.Discrete_Status.ToString & "#"
                            If tag.ITtype = "bool" Then
                                'Discrete Tag Type
                                If tag.IT_AnimTypes.Any(Function(x) x.StartsWith("ActionScript")) Then
                                    tag.ObjAttr = "<MANUALFIX - Create Separate PB>"
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x = "PushButton.Discrete") Then
                                    tag.ObjAttr = "<MANUALFIX - Create Separate PB>"
                                Else
                                    tag.ObjAttr = AddAttribute(a.Discrete_Status.DI)
                                End If
                            Else
                                'Analog Tag Type
                                tag.ObjAttr = "<ERROR - Should be #AI#>"
                            End If

                        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                        Case "#" & ObjectTypes.Cntrl_ScadaHOA.ToString & "#"
                            If tag.ITtype = "bool" Then
                                'Discrete Tag Type
                                'MUST get from descriptions since the AnimType covers too many types
                                If tag.ITdescription.ToLowerInvariant.Contains(" fail") Then
                                    tag.ObjAttr = AddAttribute(a.Cntrl_ScadaHOA.Failure)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("scada") Then
                                    'Scada in description
                                    If tag.ITdescription.ToLowerInvariant.Contains("auto") Then
                                        tag.ObjAttr = AddAttribute(a.Cntrl_ScadaHOA.ScadaHOA_DiscreteAuto)
                                    ElseIf tag.ITdescription.ToLowerInvariant.Contains("manual") Then
                                        tag.ObjAttr = AddAttribute(a.Cntrl_ScadaHOA.ScadaHOA_DiscreteManual)
                                    ElseIf tag.ITdescription.ToLowerInvariant.Contains("hand") Then
                                        tag.ObjAttr = AddAttribute(a.Cntrl_ScadaHOA.ScadaHOA_DiscreteManual)
                                    Else
                                        tag.ObjAttr = AddAttribute(a.Cntrl_ScadaHOA.ScadaHOA_Analog)
                                    End If
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("local") Then
                                    tag.ObjAttr = AddAttribute(a.Cntrl_ScadaHOA.LocalAuto)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("ready") Then
                                    tag.ObjAttr = AddAttribute(a.Cntrl_ScadaHOA.LocalAuto)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains(" run") Then
                                    tag.ObjAttr = AddAttribute(a.Cntrl_ScadaHOA.Running)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("overload") Then
                                    tag.ObjAttr = AddAttribute(a.Cntrl_ScadaHOA.Failure)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("tripped") Then
                                    tag.ObjAttr = AddAttribute(a.Cntrl_ScadaHOA.Failure)
                                Else
                                    tag.ObjAttr = "<MANUALFIX - Can't determine type>"
                                End If
                            Else
                                'Analog Tag Type
                                If tag.IT_AnimTypes.Any(Function(x) x.StartsWith("Visibility.Disc")) Then
                                    tag.ObjAttr = AddAttribute(a.Cntrl_ScadaHOA.ScadaHOA_Analog)
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("ValueDisplay")) Then
                                    tag.ObjAttr = AddAttribute(a.Cntrl_ScadaHOA.ManualSpeedSetpoint)
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("UserInput")) Then
                                    tag.ObjAttr = AddAttribute(a.Cntrl_ScadaHOA.ManualSpeedSetpoint)
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("Horizontal")) Then
                                    tag.ObjAttr = "<IGNORE>"
                                Else
                                    tag.ObjAttr = "<MANUALFIX - Can't determine type>"
                                End If
                            End If

                        Case "#" & ObjectTypes.Cntrl_Switch2pos.ToString & "#"
                            If tag.ITtype = "bool" Then
                                'Discrete Tag Type
                                tag.ObjAttr = AddAttribute(a.Cntrl_Switch2pos.Position_Discrete)
                            Else
                                'Analog Tag Type
                                tag.ObjAttr = AddAttribute(a.Cntrl_Switch2pos.Position_Analog)
                            End If

                        Case "#" & ObjectTypes.Cntrl_Switch3pos.ToString & "#"
                            'Too many possibilities - Notify to fix manually
                            tag.ObjAttr = "<MANUALFIX - All 3posSwitch>"

                        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                        Case "#" & ObjectTypes.Multistate_Indicator.ToString & "#"
                            'Too many possibilities - Notify to fix manually
                            tag.ObjAttr = "<MANUALFIX - All MultiState>"

                        Case "#" & ObjectTypes.PushButton.ToString & "#"
                            If tag.ITtype = "bool" Then
                                'Discrete Tag Type
                                If tag.IT_AnimTypes.Any(Function(x) x.StartsWith("ActionScript")) Then
                                    tag.ObjAttr = AddAttribute(a.PushButton.PB)
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x = "PushButton.Discrete") Then
                                    tag.ObjAttr = AddAttribute(a.PushButton.PB)
                                Else
                                    tag.ObjAttr = AddAttribute(a.PushButton.FB_Discrete)
                                End If
                            Else
                                'Analog Tag Type
                                tag.ObjAttr = "<MANUALFIX - Analog Tag>"
                            End If

                        Case "#" & ObjectTypes.Toggle_Switch.ToString & "#"
                            If tag.ITtype = "bool" Then
                                'Discrete Tag Type
                                If tag.IT_AnimTypes.Any(Function(x) x.StartsWith("ActionScript")) Then
                                    tag.ObjAttr = AddAttribute(a.Toggle_Switch.Switch)
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x = "PushButton.Discrete") Then
                                    tag.ObjAttr = AddAttribute(a.Toggle_Switch.Switch)
                                Else
                                    tag.ObjAttr = AddAttribute(a.Toggle_Switch.FB_Discrete)
                                End If
                            Else
                                'Analog Tag Type
                                tag.ObjAttr = "<MANUALFIX - Analog>"
                            End If

                        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                        Case "#" & ObjectTypes.Setpoint.ToString & "#"
                            If tag.ITtype = "bool" Then
                                'Discrete Tag Type
                                tag.ObjAttr = AddAttribute(a.Setpoint.FB_Discrete)
                            Else
                                'Analog Tag Type
                                If tag.IT_AnimTypes.Any(Function(x) x.StartsWith("UserInput")) Then
                                    'User Input
                                    If tag.ITdescription.ToLowerInvariant.Contains("delay") Then
                                        tag.ObjAttr = AddAttribute(a.Setpoint.Delay)
                                    Else
                                        tag.ObjAttr = AddAttribute(a.Setpoint.Setpoint)
                                    End If
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("ValueDisplay")) Then
                                    'Value Display
                                    If tag.ITdescription.ToLowerInvariant.Contains("delay") Then
                                        tag.ObjAttr = AddAttribute(a.Setpoint.DelayACC)
                                    ElseIf tag.ITdescription.ToLowerInvariant.Contains("timer") Then
                                        tag.ObjAttr = AddAttribute(a.Setpoint.DelayACC)
                                    Else
                                        tag.ObjAttr = AddAttribute(a.Setpoint.FB_Analog)
                                    End If
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.EndsWith("Discrete")) Then
                                    tag.ObjAttr = "<IGNORE (Anlg Tag / Disc Anim)>"
                                Else
                                    tag.ObjAttr = "<ERROR - Unexpected Animation>"
                                End If
                            End If

                        Case "#" & ObjectTypes.SetpointAlarm.ToString & "#"
                            If tag.ITtype = "bool" Then
                                'Discrete Tag Type
                                If tag.IT_AnimTypes.Any(Function(x) x = "PushButton.Discrete") Then
                                    tag.ObjAttr = AddAttribute(a.SetpointAlarm.Enable)
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("ActionScript")) Then
                                    tag.ObjAttr = "<MANUALFIX - ActionScript>"
                                Else
                                    tag.ObjAttr = AddAttribute(a.SetpointAlarm.Alarm)
                                End If
                            Else
                                'Analog Tag Type
                                If tag.IT_AnimTypes.Any(Function(x) x.StartsWith("UserInput")) Then
                                    'User Input
                                    If tag.ITdescription.ToLowerInvariant.Contains("delay") Then
                                        tag.ObjAttr = AddAttribute(a.SetpointAlarm.Delay)
                                    ElseIf tag.ITdescription.ToLowerInvariant.Contains("reset") Then
                                        tag.ObjAttr = AddAttribute(a.SetpointAlarm.Setpoint_Reset)
                                    Else
                                        tag.ObjAttr = AddAttribute(a.SetpointAlarm.Setpoint)
                                    End If
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("ValueDisplay")) Then
                                    'Value Display
                                    If tag.ITdescription.ToLowerInvariant.Contains("delay") Then
                                        tag.ObjAttr = AddAttribute(a.SetpointAlarm.DelayACC)
                                    Else
                                        tag.ObjAttr = AddAttribute(a.SetpointAlarm.FB_Analog)
                                    End If
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.EndsWith("Discrete")) Then
                                    tag.ObjAttr = "<IGNORE (Anlg Tag / Disc Anim)>"
                                Else
                                    tag.ObjAttr = "<ERROR - Unexpected Animation>"
                                End If
                            End If

                        Case "#" & ObjectTypes.SetpointAlarm_DI.ToString & "#"
                            If tag.ITtype = "bool" Then
                                'Discrete Tag Type
                                If tag.IT_AnimTypes.Any(Function(x) x = "PushButton.Discrete") Then
                                    tag.ObjAttr = AddAttribute(a.SetpointAlarm_DI.Enable)
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("ActionScript")) Then
                                    tag.ObjAttr = "<MANUALFIX - ActionScript>"
                                Else
                                    tag.ObjAttr = AddAttribute(a.SetpointAlarm_DI.Alarm)
                                End If
                            Else
                                'Analog Tag Type
                                If tag.IT_AnimTypes.Any(Function(x) x.StartsWith("UserInput")) Then
                                    tag.ObjAttr = AddAttribute(a.SetpointAlarm_DI.Delay)
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("ValueDisplay")) Then
                                    tag.ObjAttr = AddAttribute(a.SetpointAlarm_DI.DelayACC)
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.EndsWith("Discrete")) Then
                                    tag.ObjAttr = "<IGNORE (Anlg Tag / Disc Anim)>"
                                Else
                                    tag.ObjAttr = "<ERROR - Unexpected Animation>"
                                End If
                            End If

                        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                        Case "#" & ObjectTypes.PID.ToString & "#"
                            If tag.ITtype = "bool" Then
                                'Discrete Tag Type
                                tag.ObjAttr = AddAttribute(a.PID.Auto)
                            Else
                                'Analog Tag Type
                                If tag.IT_AnimTypes.Any(Function(x) x.StartsWith("UserInput")) Then
                                    tag.ObjAttr = AddAttribute(a.PID.ManualCommand)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("setpoint") Then
                                    tag.ObjAttr = AddAttribute(a.PID.SP)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains(" sp") Then
                                    tag.ObjAttr = AddAttribute(a.PID.SP)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("output") Then
                                    tag.ObjAttr = AddAttribute(a.PID.CV)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains(" cv") Then
                                    tag.ObjAttr = AddAttribute(a.PID.CV)
                                Else
                                    'Assume it is a PV if not one of the others (THIS WILL DEFINITELY MAKE SOME OF THEM WRONG, so double-check these)
                                    tag.ObjAttr = AddAttribute(a.PID.PV)
                                End If
                            End If

                        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                        Case "#" & ObjectTypes.Motor_Standard.ToString & "#"
                            If tag.ITtype = "bool" Then
                                'Discrete Tag Type
                                If tag.ITdescription.ToLowerInvariant.Contains("running") Then
                                    tag.ObjAttr = AddAttribute(a.Motor_Standard.Run)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("fault") Then
                                    tag.ObjAttr = AddAttribute(a.Motor_Standard.LocalFault)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("alarm") Then
                                    tag.ObjAttr = AddAttribute(a.Motor_Standard.LocalFault)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("fail to start") Then
                                    tag.ObjAttr = AddAttribute(a.Motor_Standard.FailToStart)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("fail") Then
                                    tag.ObjAttr = AddAttribute(a.Motor_Standard.Failure)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("local") Then
                                    tag.ObjAttr = AddAttribute(a.Motor_Standard.LocalAuto)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("manual") Then
                                    tag.ObjAttr = AddAttribute(a.Motor_Standard.ScadaManual)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("auto") Then
                                    tag.ObjAttr = AddAttribute(a.Motor_Standard.ScadaAuto)
                                    'Check Tagnames if not found in description
                                ElseIf tag.ITtagname.ToLowerInvariant.Contains("_run") Then
                                    tag.ObjAttr = AddAttribute(a.Motor_Standard.Run)
                                ElseIf tag.ITtagname.ToLowerInvariant.Contains("_call") Then
                                    tag.ObjAttr = AddAttribute(a.Motor_Standard.RunCall)
                                ElseIf tag.ITtagname.ToLowerInvariant.Contains("fault") Then
                                    tag.ObjAttr = AddAttribute(a.Motor_Standard.LocalFault)
                                ElseIf tag.ITtagname.ToLowerInvariant.Contains("alarm") Then
                                    tag.ObjAttr = AddAttribute(a.Motor_Standard.LocalFault)
                                ElseIf tag.ITtagname.ToLowerInvariant.Contains("_trip") Then
                                    tag.ObjAttr = AddAttribute(a.Motor_Standard.LocalFault)
                                ElseIf tag.ITtagname.ToLowerInvariant.Contains("fail") Then
                                    tag.ObjAttr = AddAttribute(a.Motor_Standard.Failure)
                                ElseIf tag.ITtagname.ToLowerInvariant.Contains("_hand") Then
                                    tag.ObjAttr = AddAttribute(a.Motor_Standard.ScadaManual)
                                ElseIf tag.ITtagname.ToLowerInvariant.Contains("auto") Then
                                    tag.ObjAttr = AddAttribute(a.Motor_Standard.ScadaAuto)
                                Else
                                    tag.ObjAttr = "<MANUALFIX - Motor Discrete>"
                                End If
                            Else
                                'Analog Tag Type
                                If tag.IT_AnimTypes.Any(Function(x) x.StartsWith("ActionScript")) Then
                                    tag.ObjAttr = "<MANUALFIX - Attach to ScadaHOA>"
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("UserInput")) Then
                                    tag.ObjAttr = AddAttribute(a.Motor_Standard.SpeedCommand)
                                ElseIf tag.IT_AnimTypes.Any(Function(x) x.StartsWith("ValueDisplay")) Then
                                    If tag.ITdescription.ToLowerInvariant.Contains("sequence") Then
                                        tag.ObjAttr = AddAttribute(a.Motor_Standard.Sequence)
                                    ElseIf tag.ITdescription.ToLowerInvariant.Contains(" amps") Then
                                        tag.ObjAttr = AddAttribute(a.Motor_Standard.Amps)
                                    ElseIf tag.ITdescription.ToLowerInvariant.Contains(" output") Then
                                        tag.ObjAttr = AddAttribute(a.Motor_Standard.SpeedCommand)
                                    ElseIf tag.ITdescription.ToLowerInvariant.Contains("speed") Then
                                        tag.ObjAttr = AddAttribute(a.Motor_Standard.Speed)
                                    Else
                                        tag.ObjAttr = "<ERROR - Unexpected Anim>"
                                    End If
                                Else
                                    tag.ObjAttr = "<MANUALFIX - Motor Analog>"
                                End If
                            End If

                        Case "#" & ObjectTypes.Valve_Standard.ToString & "#"
                            If tag.ITtype = "bool" Then
                                'Discrete Tag Type
                                If tag.ITdescription.ToLowerInvariant.Contains("fail to op") Then
                                    tag.ObjAttr = AddAttribute(a.Valve_Standard.FailToOpen)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("fail to clo") Then
                                    tag.ObjAttr = AddAttribute(a.Valve_Standard.FailToClose)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("fault") Then
                                    tag.ObjAttr = AddAttribute(a.Valve_Standard.LocalFault)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("fail") Then
                                    tag.ObjAttr = AddAttribute(a.Valve_Standard.LocalFault)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("open") Then
                                    tag.ObjAttr = AddAttribute(a.Valve_Standard.Open)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("close") Then
                                    tag.ObjAttr = AddAttribute(a.Valve_Standard.Closed)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("call") Then
                                    tag.ObjAttr = AddAttribute(a.Valve_Standard.OpenCall)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("command") Then
                                    tag.ObjAttr = AddAttribute(a.Valve_Standard.OpenCall)
                                Else
                                    tag.ObjAttr = "<MANUALFIX - Valve Discrete>"
                                End If
                            Else
                                'Analog Tag Type
                                If tag.ITdescription.ToLowerInvariant.Contains("command") Then
                                    tag.ObjAttr = AddAttribute(a.Valve_Standard.PositionCommand)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("%") Then
                                    tag.ObjAttr = AddAttribute(a.Valve_Standard.PositionCommand)
                                ElseIf tag.ITdescription.ToLowerInvariant.Contains("position") Then
                                    tag.ObjAttr = AddAttribute(a.Valve_Standard.Position)
                                Else
                                    tag.ObjAttr = "<MANUALFIX - Valve Analog>"
                                End If
                            End If

                        Case "#" & ObjectTypes.Generator.ToString & "#"
                            If tag.ITtype = "bool" Then
                                'Discrete Tag Type
                                tag.ObjAttr = "<MANUALFIX - Generator Discrete>"
                            Else
                                'Analog Tag Type
                                tag.ObjAttr = "<MANUALFIX - Generator Analog>"
                            End If

                        Case "#" & ObjectTypes.ScumTrough.ToString & "#"
                            If tag.ITtagname.ToLowerInvariant.Contains("_showtrough") Then
                                tag.ObjAttr = AddAttribute(a.ScumTrough.ShowTrough)
                            ElseIf tag.ITtagname.ToLowerInvariant.Contains("_up") Then
                                tag.ObjAttr = AddAttribute(a.ScumTrough.Up)
                            ElseIf tag.ITtagname.ToLowerInvariant.Contains("_down") Then
                                tag.ObjAttr = AddAttribute(a.ScumTrough.Down)
                            ElseIf tag.ITtagname.ToLowerInvariant.Contains("_fail") Then
                                tag.ObjAttr = AddAttribute(a.ScumTrough.Fail)
                            ElseIf tag.ITtagname.ToLowerInvariant.Contains("_sp") Then
                                tag.ObjAttr = AddAttribute(a.ScumTrough.DelaySetpoint)
                            Else
                                tag.ObjAttr = "<ERROR - ScumTrough>"
                            End If


                        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                        Case "<NOT_FOUND>"
                            tag.ObjAttr = "<ERROR - Incorrect #ObjectType#>"
                        Case "<# Not Found>"
                            tag.ObjAttr = "<ERROR - Anim outside of #Object#>"

                        Case Else
                            tag.ObjAttr = "<ERROR - Unexpected #ObjectType#>"
                    End Select
                Catch ex As Exception
                    MessageBox.Show("Error on tag " & ErrorTag & vbCrLf & vbCrLf & ex.Message)
                End Try
            Next
        Next

        'Put all Intouch tagnames in one list to look for duplicates
        Dim ITtagnamesList As New List(Of String)
        For Each item In ReportObjectsList
            ITtagnamesList.AddRange(item.Tags.Select(Function(s) s.ITtagname).ToList)
        Next

        'Sort the columns to prepare for csv output 
        ReportObjectsList = ReportObjectsList.OrderBy(Function(x) x.DisplayName).ThenBy(Function(y) y.ObjectName).ToList
        For Each item In ReportObjectsList
            item.Tags = item.Tags.OrderBy(Function(x) x.ObjAttr).ToList
            'ALSO flag any duplicate tagnames so a decision can be made on which to use (or use both)
            For Each tag In item.Tags
                tag.ITtagnameOccurences = ITtagnamesList.Where(Function(s) s = tag.ITtagname).Count
            Next
        Next

        Call CSVOutput()

    End Sub

    ''' <summary>
    ''' Writes the ReportObjectsList to a csv
    ''' </summary>
    Private Sub CSVOutput()
        Try
            Dim CSVList As New List(Of String)
            Dim header As New StringBuilder
            'Select the tags list, then get the count of the animTypes within the tags list. After that we have an enumerable of the number of animTypes for each tag. Lastly, get the max value from that list.
            Dim MaxAnimTypes = ReportObjectsList.Select(Function(x) x.Tags.Select(Function(y) y.IT_AnimTypes.Count).FirstOrDefault).Max(Function(x) x)

            header.Append("DisplayName,ObjectName,ObjectType,ObjAttr,ObjectName.ObjAttr,Count,ITtagname,ITtype,ITdescription")
            For i As Integer = 0 To MaxAnimTypes - 1
                header.Append(",IT_AnimTypes(" & i & ")")
            Next
            CSVList.Add(header.ToString)

            For Each item In ReportObjectsList
                For Each tag In item.Tags
                    Dim csvRow As New StringBuilder
                    csvRow.Append(item.DisplayName)
                    csvRow.Append("," & item.ObjectName)
                    csvRow.Append("," & item.ObjectType)
                    csvRow.Append("," & tag.ObjAttr)
                    csvRow.Append("," & item.ObjectName & "." & tag.ObjAttr)
                    csvRow.Append("," & tag.ITtagnameOccurences.ToString)
                    csvRow.Append("," & tag.ITtagname)
                    csvRow.Append("," & tag.ITtype)
                    csvRow.Append("," & tag.ITdescription)
                    For Each animType In tag.IT_AnimTypes
                        csvRow.Append("," & animType)
                    Next
                    CSVList.Add(csvRow.ToString)
                Next
            Next

            IO.File.WriteAllLines(CsvSaveFilename, CSVList)
            Process.Start(CsvSaveFilename.Replace(".csv", ".xlsx"))
        Catch ex As Exception
            MessageBox.Show("Error creating csv" & vbCrLf & vbCrLf & ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Updates the ReportObject.Tags list passed into the sub.
    ''' </summary>
    ''' <param name="tagName"></param>
    ''' <param name="tagDesc"></param>
    ''' <param name="tagAnimType"></param>
    ''' <param name="item"></param>
    Private Sub UpdateTagAnimation(ByVal tagName As String, ByVal tagDesc As String, ByVal tagType As String, ByVal tagAnimType As String, ByRef item As ReportObject)
        'Tag is blank, move on and get over it.
        If tagName <> "" Then
            'Check If tag exists
            If Not IsNothing(item.Tags.Where(Function(x) x.ITtagname = tagName).FirstOrDefault) Then
                Dim tag = item.Tags.Where(Function(x) x.ITtagname = tagName).FirstOrDefault
                'Add the animation type if it doesn't exist
                If Not tag.IT_AnimTypes.Contains(tagAnimType) Then tag.IT_AnimTypes.Add(tagAnimType)
            Else
                'Tag doesn't exist, update values then add
                Dim tag = New ReportObject.Tag
                tag.ITtagname = tagName
                tag.ITdescription = tagDesc
                tag.ITtype = tagType
                tag.IT_AnimTypes.Add(tagAnimType)
                'Assign a new generic attribute for any that can't be auto assigned
                If Not item.ObjectName.Contains("N/A") Then tag.ObjAttr = "zzGenericAttribute<" & item.Tags.Count & ">"
                'Add the to the tags list
                item.Tags.Add(tag)
            End If
        Else
            'Do nothing
        End If
    End Sub

    Public Class ReportObject
        Public Property DisplayName As String
        Public Property ObjectName As String
        Public Property ObjectType As String
        Public Property Tags As New List(Of Tag)
        Public Class Tag
            Public Property ITtagname As String
            Public Property ITtype As String
            Public Property ITdescription As String
            Public Property IT_AnimTypes As New List(Of String)
            Public Property ObjAttr As String
            Public Property ITtagnameOccurences As Integer = 0
        End Class
    End Class

    'Set Attribute Names in a class in order to avoid typos
    Public Class AttributeNames
        Public Property AI_Basic As New ID_AI_Basic

        Public Property Setpoint As New ID_Setpoint
        Public Property SetpointAlarm As New ID_SetpointAlarm
        Public Property SetpointAlarm_DI As New ID_SetpointAlarm_DI

        Public Property Discrete_Status As New ID_Discrete_Status
        Public Property Discrete_Alarm As New ID_Discrete_Alarm

        Public Property Multistate_Indicator As New ID_Multistate_Indicator
        Public Property PushButton As New ID_PushButton
        Public Property Toggle_Switch As New ID_Toggle_Switch

        Public Property Motor_Standard As New ID_Motor_Standard
        Public Property Valve_Standard As New ID_Valve_Standard
        Public Property Generator As New ID_Generator
        Public Property ScumTrough As New ID_ScumTrough

        Public Property Cntrl_ScadaHOA As New ID_Cntrl_ScadaHOA
        Public Property Cntrl_Switch3pos As New ID_Cntrl_Switch3pos
        Public Property Cntrl_Switch2pos As New ID_Cntrl_Switch2pos

        Public Property RTC As New ID_RTC
        Public Property PID As New ID_PID

        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Public Class ID_AI_Basic 'ID_AI_Tank, ID_AI_Sidebar
            Public Property Val As String = "Val"
            Public Property Visible As String = "Visible"
            Public Property Val2 As String = "Val2"
            Public Property Selection As String = "Selection"
            Public Property AlarmDisable As String = "AlarmDisable"
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Public Class ID_Setpoint
            Public Property Setpoint As String = "Setpoint"
            Public Property FB_Discrete As String = "FB_Discrete"
            Public Property FB_Analog As String = "FB_Analog"
            Public Property Delay As String = "Delay"
            Public Property DelayACC As String = "DelayACC"
        End Class

        Public Class ID_SetpointAlarm
            Public Property Setpoint As String = "Setpoint"
            Public Property Setpoint_Reset As String = "Setpoint_Reset"
            Public Property FB_Analog As String = "FB_Analog"
            Public Property Delay As String = "Delay"
            Public Property DelayACC As String = "DelayACC"
            Public Property Enable As String = "Enable"
            Public Property Alarm As String = "Alarm"
        End Class

        Public Class ID_SetpointAlarm_DI
            Public Property DI As String = "DI"
            Public Property FB_Discrete As String = "FB_Discrete"
            Public Property Delay As String = "Delay"
            Public Property DelayACC As String = "DelayACC"
            Public Property Enable As String = "Enable"
            Public Property Alarm As String = "Alarm"
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Public Class ID_Discrete_Status
            Public Property DI As String = "DI"
        End Class

        Public Class ID_Discrete_Alarm
            Public Property DI As String = "DI"
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Public Class ID_Multistate_Indicator
            Public Property State As String = "State"
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Public Class ID_PushButton
            Public Property PB As String = "PB"
            Public Property FB_Analog As String = "FB_Analog"
            Public Property FB_Discrete As String = "FB_Discrete"
        End Class

        Public Class ID_Toggle_Switch
            Public Property Switch As String = "Switch"
            Public Property FB_Analog As String = "FB_Analog"
            Public Property FB_Discrete As String = "FB_Discrete"
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Public Class ID_Motor_Standard
            Public Property RunCall As String = "RunCall"
            Public Property Run As String = "Run"
            Public Property Amps As String = "Amps"
            Public Property FailToStart As String = "FailToStart"
            Public Property LocalFault As String = "LocalFault"
            Public Property Failure As String = "Failure"
            Public Property Speed As String = "Speed"
            Public Property SpeedCommand As String = "SpeedCommand"
            Public Property ScadaAuto As String = "ScadaAuto"
            Public Property ScadaManual As String = "ScadaManual"
            Public Property LocalAuto As String = "LocalAuto"
            Public Property Sequence As String = "Sequence"
            Public Property HP As String = "HP"
        End Class

        Public Class ID_Valve_Standard
            Public Property OpenCall As String = "OpenCall"
            Public Property Open As String = "Open"
            Public Property Closed As String = "Closed"
            Public Property FailToOpen As String = "FailToOpen"
            Public Property FailToClose As String = "FailToClose"
            Public Property LocalFault As String = "LocalFault"
            Public Property Failure As String = "Failure"
            Public Property Position As String = "Position"
            Public Property PositionCommand As String = "PositionCommand"
            Public Property ScadaAuto As String = "ScadaAuto"
            Public Property ScadaManual As String = "ScadaManual"
            Public Property LocalAuto As String = "LocalAuto"
            Public Property Sequence As String = "Sequence"
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Public Class ID_Cntrl_ScadaHOA
            Public Property ScadaHOA_Analog As String = "ScadaHOA_Analog"
            Public Property ScadaHOA_DiscreteManual As String = "ScadaHOA_DiscreteManual"
            Public Property ScadaHOA_DiscreteOff As String = "ScadaHOA_DiscreteOff"
            Public Property ScadaHOA_DiscreteAuto As String = "ScadaHOA_DiscreteAuto"
            Public Property ManualSpeedSetpoint As String = "ManualSpeedSetpoint"
            Public Property LocalAuto As String = "LocalAuto"
            Public Property Failure As String = "Failure"
            Public Property Running As String = "Running"
        End Class

        Public Class ID_Cntrl_Switch3pos
            Public Position_Analog As String = "Value_Analog"
            Public Position_DiscreteLeft As String = "Value_DiscreteLeft"
            Public Position_DiscreteMiddle As String = "Value_DiscreteMiddle"
            Public Position_DiscreteRight As String = "Value_DiscreteRight"
            Public UseAnalog As Boolean = False
        End Class

        Public Class ID_Cntrl_Switch2pos
            Public Position_Analog As String = "Value_Analog"
            Public Position_Discrete As String = "Value_Discrete"
            Public UseAnalog As Boolean = False
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Public Class ID_RTC
            Public Property Hr As String = "Hr"
            Public Property Min As String = "Min"
            Public Property Sec As String = "Sec"
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Public Class ID_PID
            Public Property SP As String = "SP"
            Public Property PV As String = "PV"
            Public Property CV As String = "CV"
            Public Property P As String = "P"
            Public Property I As String = "I"
            Public Property D As String = "D"
            Public Property Auto As String = "Auto"
            Public Property ManualCommand = "ManualCommand"
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Public Class ID_Generator
            Public Property future As String = "future"
        End Class
        Public Class ID_ScumTrough
            Public Property DelaySetpoint As String = "DelaySetpoint"
            Public Property ShowTrough As String = "ShowTrough"
            Public Property Up As String = "Up"
            Public Property Down As String = "Down"
            Public Property Fail As String = "Fail"
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    End Class

End Class




