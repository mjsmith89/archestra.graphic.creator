﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class aaGraphicAccess
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btn_GalaxyLogin = New System.Windows.Forms.Button()
        Me.txt_StatusError = New System.Windows.Forms.TextBox()
        Me.btn_aaExport = New System.Windows.Forms.Button()
        Me.btn_AnimFromXml = New System.Windows.Forms.Button()
        Me.btn_aaImport = New System.Windows.Forms.Button()
        Me.txt_aaGraphicName = New System.Windows.Forms.TextBox()
        Me.btn_Initiallize = New System.Windows.Forms.Button()
        Me.txt_Galaxy = New System.Windows.Forms.TextBox()
        Me.btn_GetCustPropandOptions = New System.Windows.Forms.Button()
        Me.txt_ToolsetName = New System.Windows.Forms.TextBox()
        Me.btn_aaExport_Toolset = New System.Windows.Forms.Button()
        Me.btn_aaImport_Directory = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_ToolsetFolderName = New System.Windows.Forms.TextBox()
        Me.btn_GetSymbols = New System.Windows.Forms.Button()
        Me.txt_aaGraphicTemplate = New System.Windows.Forms.TextBox()
        Me.btn_AnimFromXml_Directory = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btn_aaExport_Update = New System.Windows.Forms.Button()
        Me.btn_CreateGraphicFromSymbolReference = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker2 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker3 = New System.ComponentModel.BackgroundWorker()
        Me.btn_CreateGraphicfromObjectNames = New System.Windows.Forms.Button()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cbo_aaGraphicTemplate = New System.Windows.Forms.ComboBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lbl_ImportingItem = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_TemplatesDir = New System.Windows.Forms.TextBox()
        Me.btn_OpenXML = New System.Windows.Forms.Button()
        Me.lbl_XMLgraphic = New System.Windows.Forms.Label()
        Me.txt_ExportDir = New System.Windows.Forms.TextBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.lbl_aaGraphicNameImport = New System.Windows.Forms.Label()
        Me.txt_ImportDir = New System.Windows.Forms.TextBox()
        Me.chk_ExAdvanced = New System.Windows.Forms.CheckBox()
        Me.btn_Cancel_BGW = New System.Windows.Forms.Button()
        Me.btn_CustPropsAndOptionsFromXml_Directory = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btn_aaGraphicRead_Import = New System.Windows.Forms.Button()
        Me.btn_OpenCSVXML = New System.Windows.Forms.Button()
        Me.lbl_CustPropsCSVFilename = New System.Windows.Forms.Label()
        Me.tb_CustPropsCSVDir = New System.Windows.Forms.TextBox()
        Me.cb_UpdateExistingGraphics = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lbl_aaAnimationsCsv = New System.Windows.Forms.Label()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.btn_ProcessAnim = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.chk_T4sidebar = New System.Windows.Forms.CheckBox()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.BackgroundWorker4 = New System.ComponentModel.BackgroundWorker()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.btn_CreateGraphicfromObjectNames_FTViewSE = New System.Windows.Forms.Button()
        Me.btn_AnimFromXml_FTViewSE = New System.Windows.Forms.Button()
        Me.btn_AnimFromXml_Directory_FTViewSE = New System.Windows.Forms.Button()
        Me.txt_aaGraphicName_FTViewSE = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_ExportDir_FTViewSE = New System.Windows.Forms.TextBox()
        Me.lbl_GalaxyLoginRequired = New System.Windows.Forms.Label()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.tb_GroupsCSVDir = New System.Windows.Forms.TextBox()
        Me.txt_ExportDir_Group = New System.Windows.Forms.TextBox()
        Me.btn_OpenGroupCSVXML = New System.Windows.Forms.Button()
        Me.lbl_GroupsCSVFileName = New System.Windows.Forms.Label()
        Me.btn_UpdateGroupsFromCSV = New System.Windows.Forms.Button()
        Me.btn_GroupsFromXML = New System.Windows.Forms.Button()
        Me.btn_GroupFromXML_Directory = New System.Windows.Forms.Button()
        Me.txt_aaGraphicName_Group = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btn_GetDASnodes = New System.Windows.Forms.Button()
        Me.btn_GetAApkgInstances = New System.Windows.Forms.Button()
        Me.GroupBox12 = New System.Windows.Forms.GroupBox()
        Me.txt_GalaxyDumpFilename = New System.Windows.Forms.TextBox()
        Me.btn_GalaxyDumpConvert = New System.Windows.Forms.Button()
        Me.GroupBox13 = New System.Windows.Forms.GroupBox()
        Me.btn_XMLtoCsv = New System.Windows.Forms.Button()
        Me.btn_Quick = New System.Windows.Forms.Button()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        Me.SuspendLayout()
        '
        'btn_GalaxyLogin
        '
        Me.btn_GalaxyLogin.Location = New System.Drawing.Point(18, 10)
        Me.btn_GalaxyLogin.Name = "btn_GalaxyLogin"
        Me.btn_GalaxyLogin.Size = New System.Drawing.Size(94, 23)
        Me.btn_GalaxyLogin.TabIndex = 0
        Me.btn_GalaxyLogin.Text = "Galaxy Login"
        Me.btn_GalaxyLogin.UseVisualStyleBackColor = True
        '
        'txt_StatusError
        '
        Me.txt_StatusError.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_StatusError.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_StatusError.Location = New System.Drawing.Point(6, 405)
        Me.txt_StatusError.Margin = New System.Windows.Forms.Padding(10)
        Me.txt_StatusError.Multiline = True
        Me.txt_StatusError.Name = "txt_StatusError"
        Me.txt_StatusError.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txt_StatusError.Size = New System.Drawing.Size(1752, 518)
        Me.txt_StatusError.TabIndex = 2
        '
        'btn_aaExport
        '
        Me.btn_aaExport.Location = New System.Drawing.Point(6, 25)
        Me.btn_aaExport.Name = "btn_aaExport"
        Me.btn_aaExport.Size = New System.Drawing.Size(190, 23)
        Me.btn_aaExport.TabIndex = 3
        Me.btn_aaExport.Text = "100: Export IDE aaGraphic to XML"
        Me.btn_aaExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_aaExport.UseVisualStyleBackColor = True
        '
        'btn_AnimFromXml
        '
        Me.btn_AnimFromXml.Location = New System.Drawing.Point(7, 47)
        Me.btn_AnimFromXml.Name = "btn_AnimFromXml"
        Me.btn_AnimFromXml.Size = New System.Drawing.Size(334, 23)
        Me.btn_AnimFromXml.TabIndex = 4
        Me.btn_AnimFromXml.Text = "200: Pull animations from XML to aaObjects list (and create report)"
        Me.btn_AnimFromXml.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_AnimFromXml.UseVisualStyleBackColor = True
        '
        'btn_aaImport
        '
        Me.btn_aaImport.Location = New System.Drawing.Point(6, 25)
        Me.btn_aaImport.Name = "btn_aaImport"
        Me.btn_aaImport.Size = New System.Drawing.Size(190, 23)
        Me.btn_aaImport.TabIndex = 5
        Me.btn_aaImport.Text = "900: Import XML to IDE aaGraphic"
        Me.btn_aaImport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_aaImport.UseVisualStyleBackColor = True
        '
        'txt_aaGraphicName
        '
        Me.txt_aaGraphicName.Location = New System.Drawing.Point(6, 28)
        Me.txt_aaGraphicName.Name = "txt_aaGraphicName"
        Me.txt_aaGraphicName.Size = New System.Drawing.Size(154, 20)
        Me.txt_aaGraphicName.TabIndex = 6
        Me.txt_aaGraphicName.Text = "aaGraphicName"
        '
        'btn_Initiallize
        '
        Me.btn_Initiallize.Location = New System.Drawing.Point(6, 43)
        Me.btn_Initiallize.Name = "btn_Initiallize"
        Me.btn_Initiallize.Size = New System.Drawing.Size(81, 23)
        Me.btn_Initiallize.TabIndex = 8
        Me.btn_Initiallize.Text = "Re-initialize"
        Me.btn_Initiallize.UseVisualStyleBackColor = True
        '
        'txt_Galaxy
        '
        Me.txt_Galaxy.BackColor = System.Drawing.Color.Pink
        Me.txt_Galaxy.Location = New System.Drawing.Point(118, 12)
        Me.txt_Galaxy.Name = "txt_Galaxy"
        Me.txt_Galaxy.Size = New System.Drawing.Size(154, 20)
        Me.txt_Galaxy.TabIndex = 9
        Me.txt_Galaxy.Text = "Galaxy"
        '
        'btn_GetCustPropandOptions
        '
        Me.btn_GetCustPropandOptions.Location = New System.Drawing.Point(9, 47)
        Me.btn_GetCustPropandOptions.Name = "btn_GetCustPropandOptions"
        Me.btn_GetCustPropandOptions.Size = New System.Drawing.Size(316, 23)
        Me.btn_GetCustPropandOptions.TabIndex = 10
        Me.btn_GetCustPropandOptions.Text = "300: Pull CustProps/Options from XML to aaObjects (save csv)"
        Me.btn_GetCustPropandOptions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_GetCustPropandOptions.UseVisualStyleBackColor = True
        '
        'txt_ToolsetName
        '
        Me.txt_ToolsetName.Location = New System.Drawing.Point(6, 56)
        Me.txt_ToolsetName.Name = "txt_ToolsetName"
        Me.txt_ToolsetName.Size = New System.Drawing.Size(154, 20)
        Me.txt_ToolsetName.TabIndex = 12
        '
        'btn_aaExport_Toolset
        '
        Me.btn_aaExport_Toolset.Location = New System.Drawing.Point(33, 54)
        Me.btn_aaExport_Toolset.Name = "btn_aaExport_Toolset"
        Me.btn_aaExport_Toolset.Size = New System.Drawing.Size(163, 23)
        Me.btn_aaExport_Toolset.TabIndex = 11
        Me.btn_aaExport_Toolset.Text = "100B: Entire Toolset (Folder)"
        Me.btn_aaExport_Toolset.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_aaExport_Toolset.UseVisualStyleBackColor = True
        '
        'btn_aaImport_Directory
        '
        Me.btn_aaImport_Directory.Location = New System.Drawing.Point(31, 56)
        Me.btn_aaImport_Directory.Name = "btn_aaImport_Directory"
        Me.btn_aaImport_Directory.Size = New System.Drawing.Size(165, 23)
        Me.btn_aaImport_Directory.TabIndex = 10
        Me.btn_aaImport_Directory.Text = "900B: Entire Directory"
        Me.btn_aaImport_Directory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_aaImport_Directory.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 49)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 29)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "⮡  "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 50)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 29)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "⮡  "
        '
        'txt_ToolsetFolderName
        '
        Me.txt_ToolsetFolderName.Location = New System.Drawing.Point(6, 48)
        Me.txt_ToolsetFolderName.Name = "txt_ToolsetFolderName"
        Me.txt_ToolsetFolderName.Size = New System.Drawing.Size(171, 20)
        Me.txt_ToolsetFolderName.TabIndex = 15
        Me.txt_ToolsetFolderName.Text = "SEJPA_Legacy_Converted_After_527"
        '
        'btn_GetSymbols
        '
        Me.btn_GetSymbols.Location = New System.Drawing.Point(6, 19)
        Me.btn_GetSymbols.Name = "btn_GetSymbols"
        Me.btn_GetSymbols.Size = New System.Drawing.Size(171, 23)
        Me.btn_GetSymbols.TabIndex = 14
        Me.btn_GetSymbols.Text = "Get all aaGraphics in toolset folder"
        Me.btn_GetSymbols.UseVisualStyleBackColor = True
        '
        'txt_aaGraphicTemplate
        '
        Me.txt_aaGraphicTemplate.Location = New System.Drawing.Point(6, 168)
        Me.txt_aaGraphicTemplate.Name = "txt_aaGraphicTemplate"
        Me.txt_aaGraphicTemplate.Size = New System.Drawing.Size(154, 20)
        Me.txt_aaGraphicTemplate.TabIndex = 13
        Me.txt_aaGraphicTemplate.Text = "T2_Template_For_Importer"
        '
        'btn_AnimFromXml_Directory
        '
        Me.btn_AnimFromXml_Directory.Location = New System.Drawing.Point(28, 74)
        Me.btn_AnimFromXml_Directory.Name = "btn_AnimFromXml_Directory"
        Me.btn_AnimFromXml_Directory.Size = New System.Drawing.Size(313, 23)
        Me.btn_AnimFromXml_Directory.TabIndex = 6
        Me.btn_AnimFromXml_Directory.Text = "200B: Entire Directory"
        Me.btn_AnimFromXml_Directory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_AnimFromXml_Directory.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(4, 71)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 29)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "⮡  "
        '
        'btn_aaExport_Update
        '
        Me.btn_aaExport_Update.Location = New System.Drawing.Point(6, 212)
        Me.btn_aaExport_Update.Name = "btn_aaExport_Update"
        Me.btn_aaExport_Update.Size = New System.Drawing.Size(334, 23)
        Me.btn_aaExport_Update.TabIndex = 7
        Me.btn_aaExport_Update.Text = "All-In-One - 200B > 210 > 900B"
        Me.btn_aaExport_Update.UseVisualStyleBackColor = True
        '
        'btn_CreateGraphicFromSymbolReference
        '
        Me.btn_CreateGraphicFromSymbolReference.Location = New System.Drawing.Point(9, 184)
        Me.btn_CreateGraphicFromSymbolReference.Name = "btn_CreateGraphicFromSymbolReference"
        Me.btn_CreateGraphicFromSymbolReference.Size = New System.Drawing.Size(316, 23)
        Me.btn_CreateGraphicFromSymbolReference.TabIndex = 11
        Me.btn_CreateGraphicFromSymbolReference.Text = "400: Import csv and create new XML Graphics (by SymbolRef)"
        Me.btn_CreateGraphicFromSymbolReference.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_CreateGraphicFromSymbolReference.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.txt_ToolsetFolderName)
        Me.GroupBox4.Controls.Add(Me.btn_GetSymbols)
        Me.GroupBox4.Location = New System.Drawing.Point(1550, 188)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(202, 83)
        Me.GroupBox4.TabIndex = 14
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Galaxy SQL Query Testing"
        '
        'btn_CreateGraphicfromObjectNames
        '
        Me.btn_CreateGraphicfromObjectNames.BackColor = System.Drawing.Color.Transparent
        Me.btn_CreateGraphicfromObjectNames.Location = New System.Drawing.Point(6, 186)
        Me.btn_CreateGraphicfromObjectNames.Name = "btn_CreateGraphicfromObjectNames"
        Me.btn_CreateGraphicfromObjectNames.Size = New System.Drawing.Size(334, 23)
        Me.btn_CreateGraphicfromObjectNames.TabIndex = 25
        Me.btn_CreateGraphicfromObjectNames.Text = "210: Create new XML Graphics from aaObjects (by ObjName)"
        Me.btn_CreateGraphicfromObjectNames.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_CreateGraphicfromObjectNames.UseVisualStyleBackColor = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(6, 151)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(51, 13)
        Me.Label12.TabIndex = 27
        Me.Label12.Text = "Template"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(54, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(221, 13)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Create aaObjects list with Animations from xml"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(27, 30)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(255, 13)
        Me.Label14.TabIndex = 30
        Me.Label14.Text = "Create aaGraphics CustProps + Options csv from xml"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbo_aaGraphicTemplate)
        Me.GroupBox1.Controls.Add(Me.txt_aaGraphicName)
        Me.GroupBox1.Controls.Add(Me.txt_ToolsetName)
        Me.GroupBox1.Controls.Add(Me.txt_aaGraphicTemplate)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Location = New System.Drawing.Point(220, 41)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(170, 230)
        Me.GroupBox1.TabIndex = 31
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Archestra IDE Symbols"
        '
        'cbo_aaGraphicTemplate
        '
        Me.cbo_aaGraphicTemplate.FormattingEnabled = True
        Me.cbo_aaGraphicTemplate.Items.AddRange(New Object() {"T2_Template_For_Importer", "T4_Template_For_Importer"})
        Me.cbo_aaGraphicTemplate.Location = New System.Drawing.Point(6, 192)
        Me.cbo_aaGraphicTemplate.Name = "cbo_aaGraphicTemplate"
        Me.cbo_aaGraphicTemplate.Size = New System.Drawing.Size(154, 21)
        Me.cbo_aaGraphicTemplate.TabIndex = 56
        Me.cbo_aaGraphicTemplate.Text = "T2_Template_For_Importer"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btn_aaExport_Toolset)
        Me.GroupBox2.Controls.Add(Me.btn_aaExport)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 41)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(202, 141)
        Me.GroupBox2.TabIndex = 32
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Archestra IDE >> ThisApp"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lbl_ImportingItem)
        Me.GroupBox3.Controls.Add(Me.btn_aaImport_Directory)
        Me.GroupBox3.Controls.Add(Me.btn_aaImport)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Location = New System.Drawing.Point(1550, 41)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(202, 141)
        Me.GroupBox3.TabIndex = 33
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "ThisApp >> Archestra IDE"
        '
        'lbl_ImportingItem
        '
        Me.lbl_ImportingItem.AutoSize = True
        Me.lbl_ImportingItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ImportingItem.ForeColor = System.Drawing.Color.Red
        Me.lbl_ImportingItem.Location = New System.Drawing.Point(6, 91)
        Me.lbl_ImportingItem.Name = "lbl_ImportingItem"
        Me.lbl_ImportingItem.Size = New System.Drawing.Size(88, 16)
        Me.lbl_ImportingItem.TabIndex = 56
        Me.lbl_ImportingItem.Text = "Importing ..."
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label13)
        Me.GroupBox5.Controls.Add(Me.Label6)
        Me.GroupBox5.Controls.Add(Me.txt_TemplatesDir)
        Me.GroupBox5.Controls.Add(Me.btn_OpenXML)
        Me.GroupBox5.Controls.Add(Me.lbl_XMLgraphic)
        Me.GroupBox5.Controls.Add(Me.txt_ExportDir)
        Me.GroupBox5.Location = New System.Drawing.Point(396, 41)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(170, 230)
        Me.GroupBox5.TabIndex = 32
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "XML Files/Directories"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(137, 147)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(27, 20)
        Me.Label13.TabIndex = 38
        Me.Label13.Text = ">>"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 151)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(83, 13)
        Me.Label6.TabIndex = 28
        Me.Label6.Text = "Template Folder"
        '
        'txt_TemplatesDir
        '
        Me.txt_TemplatesDir.Location = New System.Drawing.Point(6, 168)
        Me.txt_TemplatesDir.Name = "txt_TemplatesDir"
        Me.txt_TemplatesDir.Size = New System.Drawing.Size(154, 20)
        Me.txt_TemplatesDir.TabIndex = 37
        '
        'btn_OpenXML
        '
        Me.btn_OpenXML.Font = New System.Drawing.Font("Segoe UI Symbol", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_OpenXML.Location = New System.Drawing.Point(133, 26)
        Me.btn_OpenXML.Margin = New System.Windows.Forms.Padding(0)
        Me.btn_OpenXML.Name = "btn_OpenXML"
        Me.btn_OpenXML.Size = New System.Drawing.Size(27, 22)
        Me.btn_OpenXML.TabIndex = 35
        Me.btn_OpenXML.Text = ""
        Me.btn_OpenXML.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_OpenXML.UseVisualStyleBackColor = True
        '
        'lbl_XMLgraphic
        '
        Me.lbl_XMLgraphic.AutoSize = True
        Me.lbl_XMLgraphic.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_XMLgraphic.Location = New System.Drawing.Point(6, 30)
        Me.lbl_XMLgraphic.Name = "lbl_XMLgraphic"
        Me.lbl_XMLgraphic.Size = New System.Drawing.Size(74, 13)
        Me.lbl_XMLgraphic.TabIndex = 34
        Me.lbl_XMLgraphic.Text = "aaGraphic.xml"
        '
        'txt_ExportDir
        '
        Me.txt_ExportDir.Location = New System.Drawing.Point(6, 56)
        Me.txt_ExportDir.Name = "txt_ExportDir"
        Me.txt_ExportDir.Size = New System.Drawing.Size(154, 20)
        Me.txt_ExportDir.TabIndex = 12
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.lbl_aaGraphicNameImport)
        Me.GroupBox6.Controls.Add(Me.txt_ImportDir)
        Me.GroupBox6.Location = New System.Drawing.Point(1374, 41)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(170, 230)
        Me.GroupBox6.TabIndex = 36
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "XML Files/Directories"
        '
        'lbl_aaGraphicNameImport
        '
        Me.lbl_aaGraphicNameImport.AutoSize = True
        Me.lbl_aaGraphicNameImport.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_aaGraphicNameImport.Location = New System.Drawing.Point(6, 32)
        Me.lbl_aaGraphicNameImport.Name = "lbl_aaGraphicNameImport"
        Me.lbl_aaGraphicNameImport.Size = New System.Drawing.Size(116, 13)
        Me.lbl_aaGraphicNameImport.TabIndex = 38
        Me.lbl_aaGraphicNameImport.Text = "NEW_aaGraphicName"
        '
        'txt_ImportDir
        '
        Me.txt_ImportDir.Location = New System.Drawing.Point(6, 56)
        Me.txt_ImportDir.Name = "txt_ImportDir"
        Me.txt_ImportDir.Size = New System.Drawing.Size(154, 20)
        Me.txt_ImportDir.TabIndex = 12
        '
        'chk_ExAdvanced
        '
        Me.chk_ExAdvanced.AutoSize = True
        Me.chk_ExAdvanced.Location = New System.Drawing.Point(7, 20)
        Me.chk_ExAdvanced.Name = "chk_ExAdvanced"
        Me.chk_ExAdvanced.Size = New System.Drawing.Size(187, 17)
        Me.chk_ExAdvanced.TabIndex = 37
        Me.chk_ExAdvanced.Text = "Show advanced Exception details"
        Me.chk_ExAdvanced.UseVisualStyleBackColor = True
        '
        'btn_Cancel_BGW
        '
        Me.btn_Cancel_BGW.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Cancel_BGW.ForeColor = System.Drawing.Color.Red
        Me.btn_Cancel_BGW.Location = New System.Drawing.Point(248, 369)
        Me.btn_Cancel_BGW.Name = "btn_Cancel_BGW"
        Me.btn_Cancel_BGW.Size = New System.Drawing.Size(132, 23)
        Me.btn_Cancel_BGW.TabIndex = 39
        Me.btn_Cancel_BGW.Text = "CANCEL THREAD"
        Me.btn_Cancel_BGW.UseVisualStyleBackColor = True
        '
        'btn_CustPropsAndOptionsFromXml_Directory
        '
        Me.btn_CustPropsAndOptionsFromXml_Directory.Location = New System.Drawing.Point(27, 74)
        Me.btn_CustPropsAndOptionsFromXml_Directory.Name = "btn_CustPropsAndOptionsFromXml_Directory"
        Me.btn_CustPropsAndOptionsFromXml_Directory.Size = New System.Drawing.Size(298, 23)
        Me.btn_CustPropsAndOptionsFromXml_Directory.TabIndex = 42
        Me.btn_CustPropsAndOptionsFromXml_Directory.Text = "300B: Entire Directory"
        Me.btn_CustPropsAndOptionsFromXml_Directory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_CustPropsAndOptionsFromXml_Directory.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(4, 71)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 29)
        Me.Label4.TabIndex = 43
        Me.Label4.Text = "⮡  "
        '
        'btn_aaGraphicRead_Import
        '
        Me.btn_aaGraphicRead_Import.Cursor = System.Windows.Forms.Cursors.Default
        Me.btn_aaGraphicRead_Import.Location = New System.Drawing.Point(9, 212)
        Me.btn_aaGraphicRead_Import.Name = "btn_aaGraphicRead_Import"
        Me.btn_aaGraphicRead_Import.Size = New System.Drawing.Size(316, 23)
        Me.btn_aaGraphicRead_Import.TabIndex = 44
        Me.btn_aaGraphicRead_Import.Text = "All-in-One - 400 > 900B"
        Me.btn_aaGraphicRead_Import.UseVisualStyleBackColor = True
        '
        'btn_OpenCSVXML
        '
        Me.btn_OpenCSVXML.Font = New System.Drawing.Font("Segoe UI Symbol", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_OpenCSVXML.Location = New System.Drawing.Point(298, 115)
        Me.btn_OpenCSVXML.Margin = New System.Windows.Forms.Padding(0)
        Me.btn_OpenCSVXML.Name = "btn_OpenCSVXML"
        Me.btn_OpenCSVXML.Size = New System.Drawing.Size(27, 22)
        Me.btn_OpenCSVXML.TabIndex = 40
        Me.btn_OpenCSVXML.Text = ""
        Me.btn_OpenCSVXML.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_OpenCSVXML.UseVisualStyleBackColor = True
        '
        'lbl_CustPropsCSVFilename
        '
        Me.lbl_CustPropsCSVFilename.AutoSize = True
        Me.lbl_CustPropsCSVFilename.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_CustPropsCSVFilename.Location = New System.Drawing.Point(8, 123)
        Me.lbl_CustPropsCSVFilename.Name = "lbl_CustPropsCSVFilename"
        Me.lbl_CustPropsCSVFilename.Size = New System.Drawing.Size(156, 13)
        Me.lbl_CustPropsCSVFilename.TabIndex = 39
        Me.lbl_CustPropsCSVFilename.Text = "aaCustomPropsAndOptions.csv"
        '
        'tb_CustPropsCSVDir
        '
        Me.tb_CustPropsCSVDir.Location = New System.Drawing.Point(9, 140)
        Me.tb_CustPropsCSVDir.Name = "tb_CustPropsCSVDir"
        Me.tb_CustPropsCSVDir.Size = New System.Drawing.Size(316, 20)
        Me.tb_CustPropsCSVDir.TabIndex = 38
        '
        'cb_UpdateExistingGraphics
        '
        Me.cb_UpdateExistingGraphics.FormattingEnabled = True
        Me.cb_UpdateExistingGraphics.Items.AddRange(New Object() {"Update Existing", "Add NEW_", "Add T2_", "Add T4_"})
        Me.cb_UpdateExistingGraphics.Location = New System.Drawing.Point(927, 208)
        Me.cb_UpdateExistingGraphics.Name = "cb_UpdateExistingGraphics"
        Me.cb_UpdateExistingGraphics.Size = New System.Drawing.Size(101, 21)
        Me.cb_UpdateExistingGraphics.TabIndex = 45
        Me.cb_UpdateExistingGraphics.Text = "Add NEW_"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(923, 192)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(105, 13)
        Me.Label9.TabIndex = 46
        Me.Label9.Text = "<< New aa Name >>"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(27, 168)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(179, 13)
        Me.Label10.TabIndex = 47
        Me.Label10.Text = "Create new aaGraphics from the csv"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(54, 169)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(236, 13)
        Me.Label11.TabIndex = 48
        Me.Label11.Text = "Create aaGraphics xml file for import by ObjName"
        '
        'lbl_aaAnimationsCsv
        '
        Me.lbl_aaAnimationsCsv.AutoSize = True
        Me.lbl_aaAnimationsCsv.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_aaAnimationsCsv.Location = New System.Drawing.Point(11, 136)
        Me.lbl_aaAnimationsCsv.Name = "lbl_aaAnimationsCsv"
        Me.lbl_aaAnimationsCsv.Size = New System.Drawing.Size(297, 13)
        Me.lbl_aaAnimationsCsv.TabIndex = 49
        Me.lbl_aaAnimationsCsv.Text = "aaAnimations.csv (TODO link + add dir box like >>)"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.btn_ProcessAnim)
        Me.GroupBox7.Controls.Add(Me.Label17)
        Me.GroupBox7.Controls.Add(Me.Label15)
        Me.GroupBox7.Controls.Add(Me.Label16)
        Me.GroupBox7.Controls.Add(Me.lbl_aaAnimationsCsv)
        Me.GroupBox7.Controls.Add(Me.btn_AnimFromXml)
        Me.GroupBox7.Controls.Add(Me.Label11)
        Me.GroupBox7.Controls.Add(Me.btn_aaExport_Update)
        Me.GroupBox7.Controls.Add(Me.btn_AnimFromXml_Directory)
        Me.GroupBox7.Controls.Add(Me.btn_CreateGraphicfromObjectNames)
        Me.GroupBox7.Controls.Add(Me.Label1)
        Me.GroupBox7.Controls.Add(Me.Label5)
        Me.GroupBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox7.Location = New System.Drawing.Point(572, 22)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(347, 249)
        Me.GroupBox7.TabIndex = 50
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Convert Legacy Intouch to Modern"
        '
        'btn_ProcessAnim
        '
        Me.btn_ProcessAnim.Location = New System.Drawing.Point(198, 102)
        Me.btn_ProcessAnim.Name = "btn_ProcessAnim"
        Me.btn_ProcessAnim.Size = New System.Drawing.Size(143, 23)
        Me.btn_ProcessAnim.TabIndex = 56
        Me.btn_ProcessAnim.Text = "205: Process Anim Report"
        Me.btn_ProcessAnim.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_ProcessAnim.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Red
        Me.Label17.Location = New System.Drawing.Point(74, 77)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(196, 16)
        Me.Label17.TabIndex = 55
        Me.Label17.Text = "DO NOT USE UNTIL FIXED"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Red
        Me.Label15.Location = New System.Drawing.Point(75, 51)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(196, 16)
        Me.Label15.TabIndex = 54
        Me.Label15.Text = "DO NOT USE UNTIL FIXED"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Red
        Me.Label16.Location = New System.Drawing.Point(62, 189)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(196, 16)
        Me.Label16.TabIndex = 52
        Me.Label16.Text = "DO NOT USE UNTIL FIXED"
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.chk_T4sidebar)
        Me.GroupBox8.Controls.Add(Me.Label10)
        Me.GroupBox8.Controls.Add(Me.Label14)
        Me.GroupBox8.Controls.Add(Me.btn_GetCustPropandOptions)
        Me.GroupBox8.Controls.Add(Me.btn_CreateGraphicFromSymbolReference)
        Me.GroupBox8.Controls.Add(Me.btn_OpenCSVXML)
        Me.GroupBox8.Controls.Add(Me.lbl_CustPropsCSVFilename)
        Me.GroupBox8.Controls.Add(Me.btn_CustPropsAndOptionsFromXml_Directory)
        Me.GroupBox8.Controls.Add(Me.btn_aaGraphicRead_Import)
        Me.GroupBox8.Controls.Add(Me.tb_CustPropsCSVDir)
        Me.GroupBox8.Controls.Add(Me.Label4)
        Me.GroupBox8.Location = New System.Drawing.Point(1034, 22)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(332, 249)
        Me.GroupBox8.TabIndex = 51
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "aaGraphic Import/Export to/from csv"
        '
        'chk_T4sidebar
        '
        Me.chk_T4sidebar.AutoSize = True
        Me.chk_T4sidebar.Checked = True
        Me.chk_T4sidebar.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chk_T4sidebar.Location = New System.Drawing.Point(11, 102)
        Me.chk_T4sidebar.Name = "chk_T4sidebar"
        Me.chk_T4sidebar.Size = New System.Drawing.Size(174, 17)
        Me.chk_T4sidebar.TabIndex = 48
        Me.chk_T4sidebar.Text = "Export csv in T4 Sidebar format"
        Me.chk_T4sidebar.UseVisualStyleBackColor = True
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.chk_ExAdvanced)
        Me.GroupBox9.Controls.Add(Me.btn_Initiallize)
        Me.GroupBox9.Location = New System.Drawing.Point(12, 188)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(200, 83)
        Me.GroupBox9.TabIndex = 52
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Setup and Config"
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.btn_CreateGraphicfromObjectNames_FTViewSE)
        Me.GroupBox10.Controls.Add(Me.btn_AnimFromXml_FTViewSE)
        Me.GroupBox10.Controls.Add(Me.btn_AnimFromXml_Directory_FTViewSE)
        Me.GroupBox10.Controls.Add(Me.txt_aaGraphicName_FTViewSE)
        Me.GroupBox10.Controls.Add(Me.Label7)
        Me.GroupBox10.Controls.Add(Me.txt_ExportDir_FTViewSE)
        Me.GroupBox10.Location = New System.Drawing.Point(396, 277)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(523, 124)
        Me.GroupBox10.TabIndex = 54
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "FactoryTalk View SE"
        '
        'btn_CreateGraphicfromObjectNames_FTViewSE
        '
        Me.btn_CreateGraphicfromObjectNames_FTViewSE.Location = New System.Drawing.Point(182, 92)
        Me.btn_CreateGraphicfromObjectNames_FTViewSE.Name = "btn_CreateGraphicfromObjectNames_FTViewSE"
        Me.btn_CreateGraphicfromObjectNames_FTViewSE.Size = New System.Drawing.Size(334, 23)
        Me.btn_CreateGraphicfromObjectNames_FTViewSE.TabIndex = 53
        Me.btn_CreateGraphicfromObjectNames_FTViewSE.Text = "1210: Create new XML Graphics from aaObjects (by ObjName)"
        Me.btn_CreateGraphicfromObjectNames_FTViewSE.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_CreateGraphicfromObjectNames_FTViewSE.UseVisualStyleBackColor = True
        '
        'btn_AnimFromXml_FTViewSE
        '
        Me.btn_AnimFromXml_FTViewSE.Location = New System.Drawing.Point(182, 19)
        Me.btn_AnimFromXml_FTViewSE.Name = "btn_AnimFromXml_FTViewSE"
        Me.btn_AnimFromXml_FTViewSE.Size = New System.Drawing.Size(334, 23)
        Me.btn_AnimFromXml_FTViewSE.TabIndex = 50
        Me.btn_AnimFromXml_FTViewSE.Text = "1200: Pull animations from XML to aaObjects list (and create report)"
        Me.btn_AnimFromXml_FTViewSE.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_AnimFromXml_FTViewSE.UseVisualStyleBackColor = True
        '
        'btn_AnimFromXml_Directory_FTViewSE
        '
        Me.btn_AnimFromXml_Directory_FTViewSE.Location = New System.Drawing.Point(203, 46)
        Me.btn_AnimFromXml_Directory_FTViewSE.Name = "btn_AnimFromXml_Directory_FTViewSE"
        Me.btn_AnimFromXml_Directory_FTViewSE.Size = New System.Drawing.Size(313, 23)
        Me.btn_AnimFromXml_Directory_FTViewSE.TabIndex = 51
        Me.btn_AnimFromXml_Directory_FTViewSE.Text = "1200B: Entire Directory"
        Me.btn_AnimFromXml_Directory_FTViewSE.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_AnimFromXml_Directory_FTViewSE.UseVisualStyleBackColor = True
        '
        'txt_aaGraphicName_FTViewSE
        '
        Me.txt_aaGraphicName_FTViewSE.Location = New System.Drawing.Point(5, 19)
        Me.txt_aaGraphicName_FTViewSE.Name = "txt_aaGraphicName_FTViewSE"
        Me.txt_aaGraphicName_FTViewSE.Size = New System.Drawing.Size(154, 20)
        Me.txt_aaGraphicName_FTViewSE.TabIndex = 39
        Me.txt_aaGraphicName_FTViewSE.Text = "Wells Hatchery Globals"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(179, 43)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(35, 29)
        Me.Label7.TabIndex = 52
        Me.Label7.Text = "⮡  "
        '
        'txt_ExportDir_FTViewSE
        '
        Me.txt_ExportDir_FTViewSE.Location = New System.Drawing.Point(5, 47)
        Me.txt_ExportDir_FTViewSE.Name = "txt_ExportDir_FTViewSE"
        Me.txt_ExportDir_FTViewSE.Size = New System.Drawing.Size(154, 20)
        Me.txt_ExportDir_FTViewSE.TabIndex = 40
        Me.txt_ExportDir_FTViewSE.Text = "D:\GraphicExports"
        '
        'lbl_GalaxyLoginRequired
        '
        Me.lbl_GalaxyLoginRequired.AutoSize = True
        Me.lbl_GalaxyLoginRequired.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_GalaxyLoginRequired.ForeColor = System.Drawing.Color.Red
        Me.lbl_GalaxyLoginRequired.Location = New System.Drawing.Point(282, 15)
        Me.lbl_GalaxyLoginRequired.Name = "lbl_GalaxyLoginRequired"
        Me.lbl_GalaxyLoginRequired.Size = New System.Drawing.Size(198, 16)
        Me.lbl_GalaxyLoginRequired.TabIndex = 55
        Me.lbl_GalaxyLoginRequired.Text = "GALAXY LOGIN REQUIRED"
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.tb_GroupsCSVDir)
        Me.GroupBox11.Controls.Add(Me.txt_ExportDir_Group)
        Me.GroupBox11.Controls.Add(Me.btn_OpenGroupCSVXML)
        Me.GroupBox11.Controls.Add(Me.lbl_GroupsCSVFileName)
        Me.GroupBox11.Controls.Add(Me.btn_UpdateGroupsFromCSV)
        Me.GroupBox11.Controls.Add(Me.btn_GroupsFromXML)
        Me.GroupBox11.Controls.Add(Me.btn_GroupFromXML_Directory)
        Me.GroupBox11.Controls.Add(Me.txt_aaGraphicName_Group)
        Me.GroupBox11.Controls.Add(Me.Label8)
        Me.GroupBox11.Location = New System.Drawing.Point(927, 277)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(439, 124)
        Me.GroupBox11.TabIndex = 55
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "aaGroups"
        '
        'tb_GroupsCSVDir
        '
        Me.tb_GroupsCSVDir.Location = New System.Drawing.Point(116, 72)
        Me.tb_GroupsCSVDir.Name = "tb_GroupsCSVDir"
        Me.tb_GroupsCSVDir.Size = New System.Drawing.Size(286, 20)
        Me.tb_GroupsCSVDir.TabIndex = 54
        '
        'txt_ExportDir_Group
        '
        Me.txt_ExportDir_Group.Location = New System.Drawing.Point(6, 47)
        Me.txt_ExportDir_Group.Name = "txt_ExportDir_Group"
        Me.txt_ExportDir_Group.Size = New System.Drawing.Size(99, 20)
        Me.txt_ExportDir_Group.TabIndex = 57
        Me.txt_ExportDir_Group.Text = "C:\Files\Temp\Export"
        '
        'btn_OpenGroupCSVXML
        '
        Me.btn_OpenGroupCSVXML.Font = New System.Drawing.Font("Segoe UI Symbol", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_OpenGroupCSVXML.Location = New System.Drawing.Point(405, 71)
        Me.btn_OpenGroupCSVXML.Margin = New System.Windows.Forms.Padding(0)
        Me.btn_OpenGroupCSVXML.Name = "btn_OpenGroupCSVXML"
        Me.btn_OpenGroupCSVXML.Size = New System.Drawing.Size(27, 22)
        Me.btn_OpenGroupCSVXML.TabIndex = 56
        Me.btn_OpenGroupCSVXML.Text = ""
        Me.btn_OpenGroupCSVXML.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_OpenGroupCSVXML.UseVisualStyleBackColor = True
        '
        'lbl_GroupsCSVFileName
        '
        Me.lbl_GroupsCSVFileName.AutoSize = True
        Me.lbl_GroupsCSVFileName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_GroupsCSVFileName.Location = New System.Drawing.Point(43, 76)
        Me.lbl_GroupsCSVFileName.Name = "lbl_GroupsCSVFileName"
        Me.lbl_GroupsCSVFileName.Size = New System.Drawing.Size(73, 13)
        Me.lbl_GroupsCSVFileName.TabIndex = 55
        Me.lbl_GroupsCSVFileName.Text = "aaGroups.csv"
        '
        'btn_UpdateGroupsFromCSV
        '
        Me.btn_UpdateGroupsFromCSV.Location = New System.Drawing.Point(116, 96)
        Me.btn_UpdateGroupsFromCSV.Name = "btn_UpdateGroupsFromCSV"
        Me.btn_UpdateGroupsFromCSV.Size = New System.Drawing.Size(316, 23)
        Me.btn_UpdateGroupsFromCSV.TabIndex = 53
        Me.btn_UpdateGroupsFromCSV.Text = "2010: Update groups in XML Graphics (From CSV)"
        Me.btn_UpdateGroupsFromCSV.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_UpdateGroupsFromCSV.UseVisualStyleBackColor = True
        '
        'btn_GroupsFromXML
        '
        Me.btn_GroupsFromXML.Location = New System.Drawing.Point(116, 19)
        Me.btn_GroupsFromXML.Name = "btn_GroupsFromXML"
        Me.btn_GroupsFromXML.Size = New System.Drawing.Size(316, 23)
        Me.btn_GroupsFromXML.TabIndex = 50
        Me.btn_GroupsFromXML.Text = "2000: Pull groups XML to aaGrups list (and create report)"
        Me.btn_GroupsFromXML.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_GroupsFromXML.UseVisualStyleBackColor = True
        '
        'btn_GroupFromXML_Directory
        '
        Me.btn_GroupFromXML_Directory.Location = New System.Drawing.Point(134, 45)
        Me.btn_GroupFromXML_Directory.Name = "btn_GroupFromXML_Directory"
        Me.btn_GroupFromXML_Directory.Size = New System.Drawing.Size(298, 23)
        Me.btn_GroupFromXML_Directory.TabIndex = 51
        Me.btn_GroupFromXML_Directory.Text = "2000B: Entire Directory"
        Me.btn_GroupFromXML_Directory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_GroupFromXML_Directory.UseVisualStyleBackColor = True
        '
        'txt_aaGraphicName_Group
        '
        Me.txt_aaGraphicName_Group.Location = New System.Drawing.Point(5, 19)
        Me.txt_aaGraphicName_Group.Name = "txt_aaGraphicName_Group"
        Me.txt_aaGraphicName_Group.Size = New System.Drawing.Size(96, 20)
        Me.txt_aaGraphicName_Group.TabIndex = 39
        Me.txt_aaGraphicName_Group.Text = "T4_Grouping_Example.xml"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(111, 43)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(35, 29)
        Me.Label8.TabIndex = 52
        Me.Label8.Text = "⮡  "
        '
        'btn_GetDASnodes
        '
        Me.btn_GetDASnodes.Location = New System.Drawing.Point(5, 52)
        Me.btn_GetDASnodes.Name = "btn_GetDASnodes"
        Me.btn_GetDASnodes.Size = New System.Drawing.Size(187, 23)
        Me.btn_GetDASnodes.TabIndex = 56
        Me.btn_GetDASnodes.Text = "Get OI/DAS nodes from AAcfg xml"
        Me.btn_GetDASnodes.UseVisualStyleBackColor = True
        '
        'btn_GetAApkgInstances
        '
        Me.btn_GetAApkgInstances.Location = New System.Drawing.Point(6, 25)
        Me.btn_GetAApkgInstances.Name = "btn_GetAApkgInstances"
        Me.btn_GetAApkgInstances.Size = New System.Drawing.Size(186, 23)
        Me.btn_GetAApkgInstances.TabIndex = 57
        Me.btn_GetAApkgInstances.Text = "Get items from aapkg Manifest.xml"
        Me.btn_GetAApkgInstances.UseVisualStyleBackColor = True
        '
        'GroupBox12
        '
        Me.GroupBox12.Controls.Add(Me.txt_GalaxyDumpFilename)
        Me.GroupBox12.Controls.Add(Me.btn_GalaxyDumpConvert)
        Me.GroupBox12.Location = New System.Drawing.Point(1550, 313)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(202, 83)
        Me.GroupBox12.TabIndex = 16
        Me.GroupBox12.TabStop = False
        Me.GroupBox12.Text = "Galaxy Dump/Load csv"
        '
        'txt_GalaxyDumpFilename
        '
        Me.txt_GalaxyDumpFilename.Location = New System.Drawing.Point(6, 48)
        Me.txt_GalaxyDumpFilename.Name = "txt_GalaxyDumpFilename"
        Me.txt_GalaxyDumpFilename.Size = New System.Drawing.Size(171, 20)
        Me.txt_GalaxyDumpFilename.TabIndex = 15
        Me.txt_GalaxyDumpFilename.Text = "C:\Files\GalaxyDump.csv"
        '
        'btn_GalaxyDumpConvert
        '
        Me.btn_GalaxyDumpConvert.Location = New System.Drawing.Point(6, 19)
        Me.btn_GalaxyDumpConvert.Name = "btn_GalaxyDumpConvert"
        Me.btn_GalaxyDumpConvert.Size = New System.Drawing.Size(171, 23)
        Me.btn_GalaxyDumpConvert.TabIndex = 14
        Me.btn_GalaxyDumpConvert.Text = "Convert dump to useable csv"
        Me.btn_GalaxyDumpConvert.UseVisualStyleBackColor = True
        '
        'GroupBox13
        '
        Me.GroupBox13.Controls.Add(Me.btn_XMLtoCsv)
        Me.GroupBox13.Controls.Add(Me.btn_GetAApkgInstances)
        Me.GroupBox13.Controls.Add(Me.btn_GetDASnodes)
        Me.GroupBox13.Location = New System.Drawing.Point(12, 277)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Size = New System.Drawing.Size(200, 119)
        Me.GroupBox13.TabIndex = 53
        Me.GroupBox13.TabStop = False
        Me.GroupBox13.Text = "WW misc Xml conversions"
        '
        'btn_XMLtoCsv
        '
        Me.btn_XMLtoCsv.Location = New System.Drawing.Point(5, 90)
        Me.btn_XMLtoCsv.Name = "btn_XMLtoCsv"
        Me.btn_XMLtoCsv.Size = New System.Drawing.Size(187, 23)
        Me.btn_XMLtoCsv.TabIndex = 58
        Me.btn_XMLtoCsv.Text = "Benchmark XML Visualizer"
        Me.btn_XMLtoCsv.UseVisualStyleBackColor = True
        '
        'btn_Quick
        '
        Me.btn_Quick.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_Quick.Location = New System.Drawing.Point(226, 302)
        Me.btn_Quick.Name = "btn_Quick"
        Me.btn_Quick.Size = New System.Drawing.Size(154, 50)
        Me.btn_Quick.TabIndex = 58
        Me.btn_Quick.Text = "Replace #Symbol# with Instance.#Symbol#"
        Me.btn_Quick.UseVisualStyleBackColor = True
        '
        'aaGraphicAccess
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btn_Quick)
        Me.Controls.Add(Me.GroupBox13)
        Me.Controls.Add(Me.GroupBox12)
        Me.Controls.Add(Me.GroupBox11)
        Me.Controls.Add(Me.lbl_GalaxyLoginRequired)
        Me.Controls.Add(Me.GroupBox10)
        Me.Controls.Add(Me.GroupBox9)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.cb_UpdateExistingGraphics)
        Me.Controls.Add(Me.btn_Cancel_BGW)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.btn_GalaxyLogin)
        Me.Controls.Add(Me.txt_Galaxy)
        Me.Controls.Add(Me.txt_StatusError)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.GroupBox8)
        Me.Name = "aaGraphicAccess"
        Me.Size = New System.Drawing.Size(1764, 928)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox11.PerformLayout()
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.GroupBox13.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_GalaxyLogin As Button
    Friend WithEvents txt_StatusError As TextBox
    Friend WithEvents btn_aaExport As Button
    Friend WithEvents btn_AnimFromXml As Button
    Friend WithEvents btn_aaImport As Button
    Friend WithEvents txt_aaGraphicName As TextBox
    Friend WithEvents btn_Initiallize As Button
    Friend WithEvents txt_Galaxy As TextBox
    Friend WithEvents btn_GetCustPropandOptions As Button
    Friend WithEvents btn_CreateGraphicFromSymbolReference As Button
    Friend WithEvents btn_AnimFromXml_Directory As Button
    Friend WithEvents btn_aaImport_Directory As Button
    Friend WithEvents btn_aaExport_Toolset As Button
    Friend WithEvents txt_ToolsetName As TextBox
    Friend WithEvents btn_aaExport_Update As Button
    Friend WithEvents txt_aaGraphicTemplate As TextBox
    Friend WithEvents btn_GetSymbols As Button
    Friend WithEvents txt_ToolsetFolderName As TextBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents BackgroundWorker2 As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorker3 As System.ComponentModel.BackgroundWorker
    Friend WithEvents btn_CreateGraphicfromObjectNames As Button
    Friend WithEvents Label12 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents txt_ExportDir As TextBox
    Friend WithEvents lbl_XMLgraphic As Label
    Friend WithEvents btn_OpenXML As Button
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents txt_ImportDir As TextBox
    Friend WithEvents txt_TemplatesDir As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents lbl_aaGraphicNameImport As Label
    Friend WithEvents chk_ExAdvanced As CheckBox
    Friend WithEvents btn_Cancel_BGW As Button
    Friend WithEvents btn_CustPropsAndOptionsFromXml_Directory As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents btn_aaGraphicRead_Import As Button
    Friend WithEvents btn_OpenCSVXML As Button
    Friend WithEvents lbl_CustPropsCSVFilename As Label
    Friend WithEvents tb_CustPropsCSVDir As TextBox
    Friend WithEvents cb_UpdateExistingGraphics As ComboBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents lbl_aaAnimationsCsv As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents GroupBox8 As GroupBox
    Friend WithEvents GroupBox9 As GroupBox
    Friend WithEvents BackgroundWorker4 As System.ComponentModel.BackgroundWorker
    Friend WithEvents GroupBox10 As GroupBox
    Friend WithEvents btn_AnimFromXml_FTViewSE As Button
    Friend WithEvents btn_AnimFromXml_Directory_FTViewSE As Button
    Friend WithEvents txt_aaGraphicName_FTViewSE As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txt_ExportDir_FTViewSE As TextBox
    Friend WithEvents btn_CreateGraphicfromObjectNames_FTViewSE As Button
    Friend WithEvents lbl_GalaxyLoginRequired As Label
    Friend WithEvents cbo_aaGraphicTemplate As ComboBox
    Friend WithEvents lbl_ImportingItem As Label
    Friend WithEvents GroupBox11 As GroupBox
    Friend WithEvents btn_UpdateGroupsFromCSV As Button
    Friend WithEvents btn_GroupsFromXML As Button
    Friend WithEvents btn_GroupFromXML_Directory As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents btn_OpenGroupCSVXML As Button
    Friend WithEvents lbl_GroupsCSVFileName As Label
    Friend WithEvents tb_GroupsCSVDir As TextBox
    Friend WithEvents txt_ExportDir_Group As TextBox
    Friend WithEvents txt_aaGraphicName_Group As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents btn_GetDASnodes As Button
    Friend WithEvents btn_GetAApkgInstances As Button
    Friend WithEvents chk_T4sidebar As CheckBox
    Friend WithEvents GroupBox12 As GroupBox
    Friend WithEvents txt_GalaxyDumpFilename As TextBox
    Friend WithEvents btn_GalaxyDumpConvert As Button
    Friend WithEvents GroupBox13 As GroupBox
    Friend WithEvents btn_XMLtoCsv As Button
    Friend WithEvents btn_ProcessAnim As Button
    Friend WithEvents btn_Quick As Button
End Class
