﻿Imports System
Imports System.IO
Imports System.Data
Imports System.Text
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Collections.Concurrent
Imports ArchestrA.GRAccess

Module WWSP_Alarms

    Public Function WWSP_Get_aaGraphicNamesInFolder(ByVal FolderName As String, ByVal GalaxyName As String) As List(Of String)
        Try
            'Form connection string
            Configure_sqlConnectionString(GalaxyName)
        Catch ex As Exception
            MessageBox.Show(ExResults(ex))
        End Try
        Try
            Dim QueryString As String = ""

            'Get folder ID from its name
            QueryString = "SELECT        dbo.folder.folder_id, dbo.folder.folder_name 
            FROM           dbo.folder
            WHERE        (dbo.folder.folder_name = '" & FolderName & "')"
            Dim FolderIDs = SQL_Query_Return_Results_as_Dictionary(sqlConnectionString, QueryString)

            'Get all aaGraphic symbol names underneath a specific folder name
            Dim FolderID = FolderIDs.FirstOrDefault.Key
            QueryString = "SELECT [name]
  FROM [" & GalaxyName & "].[dbo].[internal_gtb_symbols_hierarchy_view]
  WHERE folder_id = " & FolderID & " "
            Dim SymbolNames = SQL_Query_Return_Results_as_Dictionary(sqlConnectionString, QueryString)

            Dim ReturnList As New List(Of String)
            ReturnList = SymbolNames.Keys.ToList
            Return ReturnList

        Catch ex As Exception
            MessageBox.Show("Could not retrieve data from the Galaxy." & vbCrLf & ExResults(ex))
            Return Nothing
        End Try
    End Function

    Public Function WWSP_Get_aaGraphicNames(ByVal GalaxyName As String) As List(Of String)
        Try
            'Form connection string
            Configure_sqlConnectionString(GalaxyName)
        Catch ex As Exception
            MessageBox.Show(ExResults(ex))
        End Try
        Try
            Dim QueryString As String = ""

            'Get all aaGraphic symbol names underneath a specific folder name
            QueryString = "SELECT [name]
  FROM [" & GalaxyName & "].[dbo].[internal_gtb_symbols_hierarchy_view]"
            Dim SymbolNames = SQL_Query_Return_Results_as_Dictionary(sqlConnectionString, QueryString)

            Dim ReturnList As New List(Of String)
            ReturnList = SymbolNames.Keys.ToList
            Return ReturnList

        Catch ex As Exception
            MessageBox.Show("Could not retrieve data from the Galaxy." & vbCrLf & ExResults(ex))
            Return Nothing
        End Try
    End Function


    Public Function WWSP_GetMenuTree_FromIDE(ByVal FolderName As String, ByVal GalaxyName As String, Optional ReturnToolsetAndSymbol As Boolean = True) As List(Of String)
        Try
            'Form connection string
            sqlDB = GalaxyName
            Configure_sqlConnectionString(sqlDB)
        Catch ex As Exception
            MessageBox.Show(ExResults(ex))
        End Try
        Try
            Dim QueryString As String = ""
            'Get all child folders and their ID
            QueryString = "declare @ParentFolderID int

            SET @ParentFolderID  = (SELECT  dbo.folder.folder_id 
            							FROM  dbo.folder
            							WHERE (dbo.folder.folder_name = '" & FolderName & "'))

            SELECT [folder_id] ,[folder_name]
              FROM [" & GalaxyName & "].[dbo].[folder]
              WHERE [parent_folder_id]  = @ParentFolderID"
            Dim ChildFolders = SQL_Query_Return_Results_as_Dictionary(sqlConnectionString, QueryString)

            If ChildFolders.FirstOrDefault.Value = "SQL returned no results" Then
                'There are no folders underneath the primary folder, so  get the ones under the primary folder
                Return WWSP_Get_aaGraphicNamesInFolder(FolderName, GalaxyName)
            End If
            'Use the ID's above to get the aaGraphic symbol names underneath each folder
            Dim MenuTreeDictionary As New Dictionary(Of String, String)
            For Each key In ChildFolders.Keys
                QueryString = " Select [name]
              From [" & GalaxyName & "].[dbo].[internal_gtb_symbols_hierarchy_view]
              Where folder_id = " & key & ""
                Dim SymbolNames = SQL_Query_Return_Results_as_Dictionary(sqlConnectionString, QueryString)
                If ChildFolders.FirstOrDefault.Value = "SQL returned no results" Then
                    'There are no aaGraphic symbols in the folder so move to the next
                Else
                    For Each aaG In SymbolNames.Keys
                        MenuTreeDictionary.Add(aaG, ChildFolders(key))
                    Next
                End If
            Next


            Dim ReturnList As New List(Of String)
            For Each key In MenuTreeDictionary.Keys
                If ReturnToolsetAndSymbol Then
                    ReturnList.Add(MenuTreeDictionary(key) & "," & key)
                Else
                    ReturnList.Add(key)
                End If
            Next

            Return ReturnList

        Catch ex As Exception
            MessageBox.Show("Could not retrieve data from the Galaxy." & vbCrLf & ExResults(ex))
            Return Nothing
        End Try
    End Function


    Public Function GetAllaaGraphicNames(ByVal GalaxyName As String) As List(Of String)
        Try
            'Form connection string
            Configure_sqlConnectionString(GalaxyName)
        Catch ex As Exception
            MessageBox.Show(ExResults(ex))
        End Try
        Try
            Dim QueryString As String = ""

            'Get all aaGraphic symbol names underneath a specific folder name
            QueryString = "SELECT        g.tag_name, fgl.folder_id 
  FROM [" & GalaxyName & "].[dbo].[gobject] AS g INNER Join 
            dbo.template_definition AS td ON g.template_definition_id = td.template_definition_id And td.original_template_tagname IN ('$Symbol', '$ExternalContent') INNER JOIN 
            dbo.folder_gobject_link AS fgl ON g.gobject_id = fgl.gobject_id And g.namespace_id = 3"

            Dim SymbolNames = SQL_Query_Return_Rows(sqlConnectionString, QueryString, ColumnCount:=2)
            'Return only column2
            Return SymbolNames.Select(Function(x) x.Column1).ToList()

        Catch ex As Exception
            MessageBox.Show("Could not retrieve data from the Galaxy." & vbCrLf & ExResults(ex))
            Return Nothing
        End Try
    End Function







End Module