﻿Module _EnumDelete
    Private Class AttributeNamesDELETE
        Private AI_Basic As New ID_AI_Basic

        Private Setpoint As New ID_Setpoint
        Private SetpointAlarm As New ID_SetpointAlarm
        Private SetpointAlarm_DI As New ID_SetpointAlarm_DI

        Private Discrete_Status As New ID_Discrete_Status
        Private Discrete_Alarm As New ID_Discrete_Alarm

        Private Multistate_Indicator As New ID_Multistate_Indicator
        Private PushButton As New ID_PushButton
        Private Toggle_Switch As New ID_Toggle_Switch

        Private Motor_Standard As New ID_Motor_Standard

        Private Valve_Standard As New ID_Valve_Standard

        Private Cntrl_ScadaHOA As New ID_Cntrl_ScadaHOA
        Private Cntrl_Switch3pos As New ID_Cntrl_Switch3pos
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Public Class ID_AI_Basic 'ID_AI_Tank, ID_AI_Sidebar
            Public Val As String = "Val"
            Public Visible As String = "Visible"
            Public Val2 As String = "Val2"
            Public Selection As String = "Selection"
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Private Class ID_Setpoint
            Private Setpoint As String = "Setpoint"
            Private FB_Discrete As String = "FB_Discrete"
            Private FB_Analog As String = "FB_Analog"
            Private Delay As String = "Delay"
            Private DelayACC As String = "DelayACC"
        End Class

        Private Class ID_SetpointAlarm
            Private Setpoint As String = "Setpoint"
            Private Setpoint_Reset As String = "Setpoint_Reset"
            Private FB_Analog As String = "FB_Analog"
            Private Delay As String = "Delay"
            Private DelayACC As String = "DelayACC"
            Private Enable As String = "Enable"
            Private Alarm As String = "Alarm"
        End Class

        Private Class ID_SetpointAlarm_DI
            Private DI As String = "DI"
            Private FB_Discrete As String = "FB_Discrete"
            Private Delay As String = "Delay"
            Private DelayACC As String = "DelayACC"
            Private Enable As String = "Enable"
            Private Alarm As String = "Alarm"
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Private Class ID_Discrete_Status
            Private DI As String = "DI"
        End Class

        Private Class ID_Discrete_Alarm
            Private DI As String = "DI"
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Private Class ID_Multistate_Indicator
            Private State As String = "State"
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Private Class ID_PushButton
            Private PB As String = "PB"
            Private FB_Analog As String = "FB_Analog"
            Private FB_Discrete As String = "FB_Discrete"
        End Class

        Private Class ID_Toggle_Switch
            Private Switch As String = "Switch"
            Private FB_Analog As String = "FB_Analog"
            Private FB_Discrete As String = "FB_Discrete"
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Private Class ID_Motor_Standard
            Private RunCall As String = "RunCall"
            Private Run As String = "Run"
            Private Amps As String = "Amps"
            Private FailToStart As String = "FailToStart"
            Private LocalFault As String = "LocalFault"
            Private Failure As String = "Failure"
            Private Speed As String = "Speed"
            Private SpeedCommand As String = "SpeedCommand"
            Private ScadaAuto As String = "ScadaAuto"
            Private ScadaManual As String = "ScadaManual"
            Private LocalAuto As String = "LocalAuto"
            Private Sequence As String = "Sequence"
            Private HP As String = "HP"
        End Class

        Private Class ID_Valve_Standard
            Private OpenCall As String = "OpenCall"
            Private Open As String = "Open"
            Private Closed As String = "Closed"
            Private FailToOpen As String = "FailToOpen"
            Private FailToClose As String = "FailToClose"
            Private LocalFault As String = "LocalFault"
            Private Failure As String = "Failure"
            Private Position As String = "Position"
            Private PositionCommand As String = "PositionCommand"
            Private ScadaAuto As String = "ScadaAuto"
            Private ScadaManual As String = "ScadaManual"
            Private LocalAuto As String = "LocalAuto"
            Private Sequence As String = "Sequence"
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Private Class ID_Cntrl_ScadaHOA
            Private ScadaHOA_Analog As String = "ScadaHOA_Analog"
            Private ScadaHOA_DiscreteManual As String = "ScadaHOA_DiscreteManual"
            Private ScadaHOA_DiscreteOff As String = "ScadaHOA_DiscreteOff"
            Private ScadaHOA_DiscreteAuto As String = "ScadaHOA_DiscreteAuto"
            Private ManualSpeedSetpoint As String = "ManualSpeedSetpoint"
            Private LocalAuto As String = "LocalAuto"
            Private Failure As String = "Failure"
            Private Running As String = "Running"
        End Class

        Private Class ID_Cntrl_Switch3pos
            Private Position_Analog As String = "Value_Analog"
            Private Position_DiscreteLeft As String = "Value_DiscreteLeft"
            Private Position_DiscreteMiddle As String = "Value_DiscreteMiddle"
            Private Position_DiscreteRight As String = "Value_DiscreteRight"
            Private UseAnalog As Boolean = False
        End Class

        Private Class ID_Cntrl_Switch2pos
            Private Position_Analog As String = "Value_Analog"
            Private Position_Discrete As String = "Value_Discrete"
            Private UseAnalog As Boolean = False
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Private Class ID_RTC
            Private Hr As String = "Hr"
            Private Min As String = "Min"
            Private Sec As String = "Sec"
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Private Class ID_PID
            Private SP As String = "SP"
            Private PV As String = "PV"
            Private CV As String = "CV"
            Private P As String = "P"
            Private I As String = "I"
            Private D As String = "D"
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
        Private Class ID_Generator
            Private future As String = "future"
        End Class
        Private Class ID_ScumTrough
            Private DelaySetpoint As String = "DelaySetpoint"
            Private ShowTrough As String = "ShowTrough"
            Private Up As String = "Up"
            Private Down As String = "Down"
            Private Fail As String = "Fail"
        End Class
        '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    End Class
End Module
