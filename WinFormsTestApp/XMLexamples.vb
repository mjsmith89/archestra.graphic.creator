﻿Imports System
Imports System.Xml.Linq
Imports System.Linq
Imports System.Collections.Generic
Imports System.IO
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports System.Reflection

Module XMLexamples

    '(VB.NET) Find Direct Child with Specific Tag

    'Demonstrates how To find a direct child having a specific tag.

    'The input XML, available at http://www.chilkatsoft.com/data/fruit.xml, Is this

    '<root>
    '    <fruit color = "red" > apple</fruit>
    '    <fruit color = "green" > pear</fruit>
    '    <veg color = "orange" > carrot</veg>
    '    <meat animal = "cow" > beef</meat>
    '    <xyz>
    '    <fruit color = "blue" > blueberry</fruit>
    '        <veg color = "green" > broccoli</veg>
    '    </xyz>
    '    <fruit color = "purple" > grape</fruit>
    '    <cheese color = "yellow" > cheddar</cheese>
    '</root>

    Dim xml As New Chilkat.Xml

    ' The sample input XML is available at http://www.chilkatsoft.com/data/fruit.xml
    Dim success As Boolean = xml.LoadXmlFile("qa_data/xml/fruit.xml")
    If (success <> True) Then
    Debug.WriteLine(xml.LastErrorText)
    Exit Sub
    End If


    ' Find the direct child node having the tag "meat", and 
    ' return a new instance of the XML object referencing the
    ' child node, if found.
    Dim child As Chilkat.Xml = xml.FindChild("meat")
    If (xml.LastMethodSuccess = False) Then
    Debug.WriteLine("No direct child having the tag ""meat"" was found.")
Else
    Debug.WriteLine("Content = " & child.Content)

End If


' The same can be accomplished without creating a new 
' XML object instance.  Instead, the FindChild2 method updates
' the caller's internal reference to the found child, if successful.
success = xml.FindChild2("meat")
If (success = True) Then
    ' Success!  The xml object now references the found child.
    Debug.WriteLine("Content = " & xml.Content)
    ' Restore the reference back to the parent.
    success = xml.GetParent2()
Else
    Debug.WriteLine("No direct child having the tag ""meat"" was found.")
End If



    '(VB.NET) Methods for Getting Attributes

    'Demonstrates some methods For getting attribute name/values.

    'The input XML, available at http://www.chilkatsoft.com/data/car.xml, Is this

    '<root>
    '    <car color = "black" make="mercedes" model="C350" hp="302" engine="v6" type="sedan">Mercedes Benz C350</car>
    '</root>

    Dim xml As New Chilkat.Xml
    Dim carNode As Chilkat.Xml
    Dim numAttr As Integer
    Dim horsepower As Integer
    Dim i As Integer

    Dim success As Boolean
'  The sample input XML is available at http://www.chilkatsoft.com/data/car.xml
success = xml.LoadXmlFile("car.xml")
If (success <> True) Then
    Console.WriteLine(xml.LastErrorText)
    Exit Sub
    End If


'  Navigate to the "car" node, which is the 1st child:
carNode = xml.FirstChild()

'  Get the value of the "model" attribute:
Console.WriteLine("model = " & carNode.GetAttrValue("model"))

'  Get the value of the "hp" attribute as an integer:
horsepower = carNode.GetAttrValueInt("hp")
Console.WriteLine("horsepower = " & horsepower)

'  Iterate over the attributes and show the name/value of each:
numAttr = carNode.NumAttributes

i = 0
While i < numAttr
    Console.WriteLine(carNode.GetAttributeName(i) & ": " & carNode.GetAttributeValue(i))
    i = i + 1
End While


    '(VB.NET) GetChild* methods

    'Demonstrates several Of the GetChild* methods.

    'The input XML, available at http://www.chilkatsoft.com/data/get_child.xml, Is this

    '<root>
    '    <fruit color = "red" > apple</fruit>
    '    <fruit color = "green" > pear</fruit>
    '    <veg color = "orange" > carrot</veg>
    '    <meat animal = "cow" > beef</meat>
    '    <xyz>
    '    <fruit color = "blue" > blueberry</fruit>
    '        <veg color = "green" > broccoli</veg>
    '    </xyz>
    '    <fruit color = "purple" > grape</fruit>
    '    <cheese color = "yellow" > cheddar</cheese>
    '</root>

    'The output XML Is this

    '<abc b = "pear" c="orange" a="130">Test</abc>

    Dim xml As New Chilkat.Xml
    Dim child As Chilkat.Xml

    Dim success As Boolean
'  The sample input XML is available at http://www.chilkatsoft.com/data/get_child.xml
success = xml.LoadXmlFile("get_child.xml")
If (success <> True) Then
    Console.WriteLine(xml.LastErrorText)
    Exit Sub
    End If


'  The NumChildren property contains the number of direct
'  child nodes.  Note: The child nodes under "xyz" are NOT
'  direct children of "root".  Therefore, the "root" node has
'  7 direct children
Console.WriteLine("NumChildren = " & xml.NumChildren)

'  Iterate over the direct children by index. The first child
'  is at index 0.
Dim i As Integer
    For i = 0 To xml.NumChildren - 1
    '   access the tag and content directly by index:
    Console.WriteLine(i & ": " & xml.GetChildTagByIndex(i) & " : " & xml.GetChildContentByIndex(i))

Next
Console.WriteLine("-----")

'  Do the same as the above loop, but get the child node
'  and access the Tag and Content properties:
For i = 0 To xml.NumChildren - 1
    child = xml.GetChild(i)
    Console.WriteLine(i & ": " & child.Tag & " : " & child.Content)

Next
Console.WriteLine("-----")

'  Do the same as the above loop, but instead of creating
'  a new object instance for each child, call GetChild2 to
'  update the object's reference instead.
For i = 0 To xml.NumChildren - 1
    '  Navigate to the Nth child.
    success = xml.GetChild2(i)
    Console.WriteLine(i & ": " & xml.Tag & " : " & xml.Content)
    '  Navigate back up to the parent:
    success = xml.GetParent2()
Next
Console.WriteLine("-----")

'  Examine the result:
Console.WriteLine(xml.GetXml())



End Module
